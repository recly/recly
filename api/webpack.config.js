'use strict';

var path = require('path');
var fs = require('fs');

var nodeModules = {};
var buildPath = path.join(__dirname, 'server');

fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = mod;
  });

var config = {
  entry: './server/server.js',
  target: 'node',
  output: {
    libraryTarget: "commonjs2",
    path: buildPath,
    filename: 'server.bundle.js'
  },
  externals: nodeModules
}

module.exports = config;

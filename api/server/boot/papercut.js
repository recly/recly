module.exports = function enableAuthentication(server) {

  var path = require('path');
  var papercut = require('papercut');
  var pathStorage = path.resolve(__dirname, '../../server/storage');

  papercut.configure(function() {
    papercut.set('storage', 'file');
    papercut.set('directory', pathStorage);
    papercut.set('url', '/temp');
  });

}

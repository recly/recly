module.exports = function enableAuthentication(server) {

  var Promise = require('bluebird');
  var logic = require('../../common/logic/logic.js');

  logic.opAsync(server.models.role, 'find', {where: {name: 'root'}})
    .then(function(items) {
      if (items.length === 0) return logic.opAsync(server.models.role, 'create',
      {
        'status': 1,
        'name': 'root',
        'description': 'root user'
      });
      else if (items.length === 1) return items[0];
      else throw new Error('root role is set many times');
    })

    .catch(function(e) {
      console.log('e', e);
    })

    .then(function(rootRole) {
      return Promise.all([
        Promise.resolve(rootRole),
        Promise.resolve(logic.modules),
        logic.opAsync(server.models.acl, 'find', {})
      ]);
    })

    .catch(function(e) {
      console.log('e', e);
    })

    .then(function(res) {
      return parseAclsToSend(res[0], res[1], res[2], ['c', 'd']);
    })

    .catch(function(e) {
      console.log('e', e);
    })

    .then(function(acls) {
      //console.log('acls', acls);
    });

  function parseAclsToSend(role, modules, acls, operations) {

    var tmpAcls = [];
    var res = [];

    modules.forEach(function(module_) {
      operations.forEach(function(operation) {
        tmpAcls.push(formateAclToSend(module_, role.name, role.id, operation));
      });
    });

    tmpAcls.forEach(function(aclToSend) {
      var toPush = true;
      acls.forEach(function(acl) {

        if (acl.model === aclToSend.model &&
          acl.principalId === aclToSend.principalId &&
          acl.property === aclToSend.property) toPush = false;

      });

      if (toPush) return res.push(aclToSend);

    });

    return res.length > 0 ?
      logic.opAsync(server.models.acl, 'create', res) : [];

  }

  function formateAclToSend(moduleName, roleName, roleId, operation) {
    return {
      model: moduleName,
      permission: 'ALLOW',
      principalType: 'ROLE',
      principalId: roleName,
      roleId: roleId,
      accessType: operation === 'r' ? 'READ' : 'WRITE',
      property: getOperationName(operation)
    };
  }

  function getOperationName(operation) {
    return operation === 'u' ? 'prototype_updateAttributes' :
      operation === 'r' ? 'find' :
      operation === 'd' ? 'deleteById' : 'create';
  }

};

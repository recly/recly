module.exports = function(app) {

  var async = require('async');

  var Role = app.models.Role;
  var Acl = app.models.Acl;
  var User = app.models.User;

  Role.registerResolver('settingsUser', settingsUser);
  Role.registerResolver('specialUm', specialUm);
  Role.registerResolver('initialize', initialize);

  //--
  function specialUm(role, ctx, cb) {

    var modelName = 'unidadMedida';

    // if the target model is not project
    if (ctx.modelName !== modelName) return reject(cb);

    // do not allow anonymous users
    var userId = ctx.accessToken.userId;
    if (!userId) return reject(cb);

    User.findById(userId, {fields: ['roles'], include: 'roles'},
      function(err, instance) {

        if (err) return reject(cb);

        var loop = true;
        var i = 0;

        async.doWhilst(function(callback) {
            if (i > instance.roles.length) loop = false;
            Acl.findOne({where: {principalType: 'ROLE', principalId:
              instance.roles[i], model: 'agregado', property: 'create',
              accessType: 'WRITE'}}, function(err, acl) {
              if (err) callback(err);
              else if (acl === null) callback('this user has not access to create agregado');
              else {
                loop = false;
                callback(null);
              }
            })
            i += 1;
          },
          function() {return loop;},
          function(err) {
            if (err) reject(cb);
            else cb(null, true);
          });

    });


  }

  function settingsUser(role, ctx, cb) {

    function resolve(err) {
      if (err) return cb(err);
      else cb(null, false);
    }

    // the target model is not settings
    if (ctx.modelName !== 'settings') return resolve();

    // do not allow anonymous users
    var userId = ctx.accessToken.userId;
    if (!userId) return resolve();

    // check if userId is in team table for the given project id
    ctx.model.findById(userId, function(err, settings) {
      if (err || !settings) resolve(err);
      else cb(null, settings.length === 1);
    });

  }

  function initialize(role, ctx, cb) {
    app.models.user.find(function(err, users) {
      if (err) reject(cb);
      else if(users.length > 0) reject(cb);
      else cb(null, true);
    });
  }

  function reject(cb) {
    process.nextTick(function() {
      cb(null, false);
    });
  }

}

var Primus = require('primus');
var Client = require('strong-pubsub');
var Connection = require('strong-pubsub-connection-mqtt');
var Bridge = require('strong-pubsub-bridge');
var Adapter = require('strong-pubsub-mqtt');
var MOSQUITTO_PORT = process.env.MOSQUITTO_PORT || 1883;

module.exports = function(app) {

  var testClient = new Client({port: MOSQUITTO_PORT}, Adapter);

  app.on('started', function(server) {

    var primus = new Primus(server, {
      transformer: 'engine.io',
      parser: 'binary'
    });

    primus.on('connection', function(spark) {
      var client = new Client({port: MOSQUITTO_PORT}, Adapter);
      var bridge = new Bridge(new Connection(spark), client);
      bridge.connect();
    });

    primus.on('disconnection', function(spark) {
      console.log('visitant disconnected');
      //console.log(spark.address);
    });

  });

  setInterval(function() {
    testClient.publish('message', 'hello');
  }, 1000);
}

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = loopback();

app.start = function() {
  // start the web server
  var server = app.listen(function() {
    app.emit('started', server);
    console.log('Web server listening at: %s', app.get('url'));
  });
  return server;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;
  // start the server if `$ node server.js`
  //if (require.main === module) {
  app.start();
    /*
    app.io.on('connection', function(socket) {
      socket.on('initConnect', function(ipData) {
        console.log('guest user connected');
        console.log(ipData.ip);
      });
      socket.on('disconnect', function() {
        console.log('guest user disconnected');
      });
    });
    */
  //}
});
module.exports = app;

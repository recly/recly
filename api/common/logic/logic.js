'use strict';

var Promise = require('bluebird');
var path = require('path');
var fs = require('fs');
var util = require('util');
var path = require('path');
var async = require('async');
var papercut = require('papercut');
var extend = require('extend');

var pathStorage = path.resolve(__dirname, '../../server/storage');

function Logic() {

  Object.defineProperty(this, 'modules', {
    writable: false,
    value: [
      'acl',
      'agregado',
      'area',
      'asistencia',
      'camion',
      'cargo',
      'caja',
      'compra',
      'empresa',
      'flota',
      'inventario',
      'persona',
      'role',
      'remuneracion',
      'settings',
      'trabajador',
      'translate',
      'unidadMedida',
      'user',
      'venta'
    ]
  });
}

var logic = new Logic();

module.exports = logic;

Logic.prototype.opAsync = function(Model, op, options, options2) {

  options = options || {};

  return (op === 'findOrCreate') ?

    new Promise(function(resolve, reject) {
      Model[op](options, options2, function(err, items) {
        if (err) reject(err);
        else resolve(items);
      });
    }) :

    new Promise(function(resolve, reject) {
      Model[op](options, function(err, items) {
        if (err) reject(err);
        else resolve(items);
      });
    });

};

Logic.prototype.validate = function(Model, validationType, propertyOrModel,
  ops) {

  var tmpOps = {};

  if (
    validationType === 'p'
  ) Model.validatesPresenceOf(propertyOrModel, {
    message: this.formatMessage('API_CONSAMA.FAIL.PRESENCE', propertyOrModel),
    failType: 'validatesPresenceOf',
    property: propertyOrModel,
    model: propertyOrModel
  });

  else if (validationType === 'u') Model.validatesUniquenessOf(propertyOrModel,
    {message: this.formatMessage('API_CONSAMA.FAIL.UNIQUE', propertyOrModel)});

  else if (validationType === 'n') {
    var s = this.formatMessage('API_CONSAMA.FAIL.NUMERIC', propertyOrModel);
    tmpOps = {message: {int: s, number: s}};
    if (ops.int) tmpOps.int = true;
    Model.validatesNumericalityOf(propertyOrModel, tmpOps);
  }

  return this;

};

Logic.prototype.deleteById = function(ctx, next, setPhotoName) {
  //console.log(ctx.where);
  setPhotoName = setPhotoName || 'photo';

  var self = this;

  ctx.Model.findById(ctx.where.id, function(err, item) {
    if (err) return next(err);

    if (item[setPhotoName] !== 'default.jpg') self.deletePhoto(ctx.Model
      .pluralModelName, item[setPhotoName])
      .then(function(res) {next(null);})
      .catch(function(err) {next(err);});
    else next(null);
  });

};

Logic.prototype.createOrUpdate = function(ctx, next, Model, setPhotoName,
  propPhotoName) {

  setPhotoName = setPhotoName || 'name';
  propPhotoName = propPhotoName || 'photo';

  var self = this;

  var item = ctx.instance ? ctx.instance : ctx.data;

  var tempImg = item[propPhotoName] ?
    path.join(pathStorage, 'temp', item[propPhotoName]) : '';

  var toDelete = '';
  var agregadoPhotoName = '';

  //update only description or whatever less photo or name
  if (typeof setPhotoName === 'undefined') next(null);

  else if (item.id && item[propPhotoName] === item[setPhotoName] +
    path.extname(item.photo)) next();

  //rename photo to the new name
  else if (item.id && item[propPhotoName] !== item[setPhotoName] +
    path.extname(item[propPhotoName]) && !fs.existsSync(tempImg)) {

    if (item[propPhotoName] === 'default.jpg') {

      if (item[propPhotoName] === ctx.instance.photo) next(null);
      else {
        this.deletePhoto(Model.definition.settings.plural, ctx.instance.photo)
          .then(function(res) {
            next(null);
          }, function(err) {
            next(err);
          });
      }

    }else {

      renamePhoto(Model.definition.settings.plural, ctx.instance.photo,
        item[setPhotoName] + path.extname(item[propPhotoName]))
        .then(function(res) {
          item[propPhotoName] = res.result;
          next(null);
        }, function(err) {
          next(err);
        });

    }

  }else if (item.id && item[propPhotoName] !== item[setPhotoName] +
    path.extname(item[propPhotoName]) && fs.existsSync(tempImg)) {//update photo

    this.deletePhoto(Model.definition.settings.plural, item[propPhotoName])
      .then(function() {
        return self.processPhoto(item[setPhotoName],
          tempImg, self.setPhoto(Model));
      }, function(err) {
        next(err);
      })

      .then(function(res) {
        item[propPhotoName] = res.result;
        next(null);
      }, function(err) {
        next(err);
      });

  }else if (item[propPhotoName] && fs.existsSync(tempImg)) {//create
    self.processPhoto(item[setPhotoName], tempImg, self.setPhoto(Model))
      .then(function(res) {
        item[propPhotoName] = res.result;
        //console.log(item);
        next(null);
      })
      .catch(function(err) {
        next(err);
      });
  }else {
    item[propPhotoName] = 'default.jpg';
    next(null);
  }
};

Logic.prototype.getApp = function(Model) {

  return new Promise(function(resolve, reject) {
    Model.getApp(function(err, app) {
      if (err) reject(err);
      else resolve(app);
    });
  });

};

Logic.prototype.setPhoto = function(Model, photoTypes) {

  var containerName = Model.definition.settings.plural;

  photoTypes = util.isArray(photoTypes) ?
    photoTypes : [
      {name: 'standard', size: '40x40', process: 'crop'},
      {name: 'large', size: '56x56', process: 'crop'},
      {name: 'cover', size: '360x202', process: 'crop'}
    ];

  papercut.configure(function() {
    papercut.set('directory', path.join(pathStorage, containerName));
  });

  var ModulePhoto = papercut.Schema(function(schema) {

    for (var i = 0; i < photoTypes.length; i++)(function(photoType, index) {
      schema.version(photoType);
    })(photoTypes[i], i);

  });

  return new ModulePhoto();

};

Logic.prototype.saveModel = function(app, modelName, data, res) {
  return new Promise(function(resolve, reject) {
    app.models[modelName].create(data, function(err, item) {

      if (typeof res === 'undefined') res = {app: app};
      else res.app = app;

      if (err) reject({err: err, res: res});
      else {
        res[modelName] = item;
        resolve(res);
      }
    });
  });
};

Logic.prototype.processPhoto = function(photoName, tempImg, papercutInstance) {
  return new Promise(function(resolve, reject) {

    fs.exists(tempImg, function(exists) {

      if (!exists) return resolve({result: 'default.jpg'});

      photoName = parseSpacesInPhoto(photoName);
      var ext = path.extname(tempImg).toLowerCase();

      papercutInstance.process(photoName, tempImg, function(err, images) {
        //console.log(err, images);
        if (err) reject(err);
        else {
          fs.unlink(tempImg, function(err) {
            if (err) reject(err);
            else resolve({result: photoName + ext});
          });
        }
      });

    });

  });

};

Logic.prototype.deletePhoto = function(pluralModelName, photo, photoTypes) {

  return new Promise(function(resolve, reject) {

    photoTypes = util.isArray(photoTypes) ?
      photoTypes : ['standard', 'large', 'cover'];

    var details = [];
    var pathPhotos = path.join(pathStorage,  pluralModelName);
    var photoName = '';
    var ext = path.extname(photo);

    async.each(photoTypes, function(photoType, cb) {

      photoName = path.basename(photo, ext);
      var photoToDelete = path
        .join(pathPhotos, photoName + '-' + photoType + ext);

      fs.exists(photoToDelete, function(exists) {
        if (exists) fs.unlink(photoToDelete, function(err) {
          if (err) cb(err);
          else {
            details.push({msg: photoToDelete + ' has been removed'});
            cb(null);
          }
        });
        else {
          details.push({msg: photoToDelete + ' doesn\'t exists'});
          cb(null);
        }
      });
    }, function(err) {
      if (err) reject(err);
      else resolve({details: details});
    });

  });

};

Logic.prototype.deleteInstances = function(instances, instancesWDependencies) {
  return new Promise(function(resolve, reject) {
    async.each(instances, function(instance, callback) {
      instance.destroy();
    }, function(err) {
      if (err) reject(err);
      else resolve();
    });
  });
};

Logic.prototype.formatMessage = function(codeI18N, propertyOrModel) {
  return JSON.stringify({
    code: codeI18N,
    property: propertyOrModel,
    model: propertyOrModel
  });
};

//--
function renamePhoto(pluralModelName, oldPhoto, newPhoto, photoTypes) {

  return new Promise(function(resolve, reject) {

    photoTypes = util.isArray(photoTypes) ?
      photoTypes : ['standard', 'large', 'cover'];

    newPhoto = parseSpacesInPhoto(newPhoto);

    var res = [];
    var pathPhotos = path.join(pathStorage,  pluralModelName);

    var photoNameO = '';
    var photoNameN = '';
    var extO = path.extname(oldPhoto);
    var extN = path.extname(newPhoto);

    async.each(photoTypes, function(photoType, cb) {

      photoNameO = path.basename(oldPhoto, extO);
      photoNameN = path.basename(newPhoto, extN);

      var photoToRename = path
        .join(pathPhotos, photoNameO + '-' + photoType + extO);

      var photoRenamed = path
        .join(pathPhotos, photoNameN + '-' + photoType + extN);

      fs.exists(photoToRename, function(exists) {
        if (exists) fs.rename(photoToRename, photoRenamed, function(err) {
          if (err) cb(err);
          else {
            res.push({msg: photoToRename + ' has been renamed to ' +
              photoRenamed});
            cb(null);
          }
        });
        else {
          res.push({msg: photoToRename + ' doesn\'t exists'});
          cb(null);
        }
      });
    }, function(err) {
      if (err) reject(err);
      else resolve({result: newPhoto, details: res});
    });

  });

}

function parseSpacesInPhoto(photo) {
  return (/ /.test(photo)) ? photo.replace(/ /g, '-') : photo || '';
}

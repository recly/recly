module.exports = function(Settings) {

  var logic = require('../logic/logic.js');

  Settings.validatesUniquenessOf('userId', {
    message: 'only settings for one user'
  });

  var fs = require('fs');
  var path = require('path');
  var async = require('async');

  Settings.public = function(cb) {
    data = {
      langsAvaibles: ['en', 'es'],
      langFallback: 'en',
      preferredLanguage: 'en'
    };
    cb(null, data);
  };

  Settings.remoteMethod('public', {
    description: 'get public info',
    http: {verb: 'get'},
    returns: {
      arg: 'data',
      type: 'object',
      root: true
    }
  });

  Settings.modules = function(cb) {
    cb(null, logic.modules);
  };

  Settings.remoteMethod('modules', {
    description: 'get modules for consama app',
    http: {verb: 'get'},
    returns: {
      arg: 'data',
      type: 'array',
      root: true
    }
  });

  Settings.translate = function(part, lang, cb) {

    var pathI18n = path.resolve(__dirname, '../i18n');

    var aux = true;
    var i = 0;
    var auxPaths = [
      {
        path: part,
        details: {codeError: 'I18NNOTPART', msg: 'not found part ' + part}
      },
      {
        path: part + '/' + part + '-' + lang + '.json',
        details: {
          codeError: 'I18NNOTLANG',
          msg: 'not found lang(' + lang + ') for ' + part
        }
      }
    ]

    async.whilst(function() {return aux && i < 2},
      function(callback) {
        fs.exists(path.join(pathI18n, auxPaths[i].path), function(exists) {
          if (exists) {
            i += 1;
            callback(null);
          }else {
            callback(auxPaths[i].details);
            aux = false;
          }
        });
      }, function(details) {
        if (details) cb(null, details);
        else {
          var translate = fs.readFileSync(path.join(pathI18n,
            auxPaths[1].path), {encoding: 'utf-8'});
          cb(null, JSON.parse(translate));
        }
      });
  };

  Settings.remoteMethod('translate', {
    description: 'get translate for determined section',
    http: {verb: 'get'},
    accepts: [
      {arg: 'part', type: 'string', required: true},
      {arg: 'lang', type: 'string', required: true}
    ],
    returns: {
      arg: 'translate',
      type: 'object',
      root: true
    }
  });

};

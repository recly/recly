module.exports = function(Empresa) {

  var ThisModel = Empresa;

  var logic = require('../logic/logic.js');
  /*
  logic
    .validate(ThisModel, 'p', 'comercialName')
    .validate(ThisModel, 'p', 'socialName')
    .validate(ThisModel, 'p', 'ruc')

    .validate(ThisModel, 'u', 'comercialName')
    .validate(ThisModel, 'u', 'socialName')
    .validate(ThisModel, 'u', 'ruc')

    .validate(ThisModel, 'n', 'ruc', {int: true});
  */
  ThisModel.observe('before save', function(ctx, next) {

    var item = ctx.instance ? ctx.instance : ctx.data;

    item.creationDate = new Date();
    //1: enable 2:disable
    if (typeof item.status === 'undefined') item.status = 1;

    //if (item.ruc.toString().length !== 11) return next(new Error(logic
    //  .formatMessage('API_CONSAMA.FAIL.LENGTH', 'ruc')));

    if (item.id) return logic.createOrUpdate(ctx, next, ThisModel,
      'socialName');

    ThisModel.find({where: {main: true}}, function(errFind, empresas) {
      if (errFind) next(errFind);
      else if (empresas.length > 0 && item.main) {

        next(new Error(logic
          .formatMessage('API_CONSAMA.FAIL.MAIN_ALREADY_SETED',
          ThisModel.definition.name)));

      }else {
        if (empresas.length === 0) item.main = true;
        logic.createOrUpdate(ctx, next, ThisModel, 'comercialName');
      }
    });

  });

};

module.exports = function(Camion) {

  var ThisModel = Camion;

  var logic = require('../logic/logic.js');

  logic
    .validate(ThisModel, 'p', 'brand')
    .validate(ThisModel, 'p', 'model')
    .validate(ThisModel, 'p', 'licensePlate')
    .validate(ThisModel, 'u', 'licensePlate')
    .validate(ThisModel, 'u', 'aka');

  ThisModel.observe('before save', function(ctx, next) {

    var item = ctx.instance ? ctx.instance : ctx.data;

    //0: disabled 1: enable 2:inRoute
    if (typeof item.status === 'undefined') item.status = 1;

    logic.createOrUpdate(ctx, next, ThisModel, 'licensePlate', 'photo');

  });

};

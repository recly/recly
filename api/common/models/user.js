module.exports = function(User) {

  User.observe('before save', function(ctx, next) {

    var item = ctx.instance || ctx.data;

    item.status = 'created';
    item.created = Date.now();
    next(null);

  });
  /*
  User.afterRemote('findById', function(ctx, u, next) {

    if (u === null) User.find(function(err, users) {
      if (users.length === 0) {
        var errToSend = new Error('launch initialize');
        errToSend.code = 'INITIALIZE';
        next(errToSend);
      }else next();
    });
    else next();

  });
  */
  User.checkinitiaize = function(cb) {
    User.find(function(err, users) {
      if (err) cb(err);
      else if (users.length === 0) cb(null, true);
      else cb(null, false);
    });
  };

  User.remoteMethod('checkinitiaize', {
    description: 'check if the app has the initialize user',
    http: {verb: 'get'},
    returns: {
      arg: 'isInitialize',
      type: 'boolean',
      root: true
    }
  });

};

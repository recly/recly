module.exports = function(Trabajador) {

  var Promise = require("bluebird");
  var logic = require('../logic/logic.js');
  var path = require('path');
  var async = require('async');
  var ThisModel = Trabajador;
  /*
  ThisModel.observe('before save', function(ctx, next) {

    var data = ctx.instance ? ctx.instance : ctx.data;
    var res = {};
    ctx.reqData = data;
    //console.log('in observe ---', ctx.instance, ctx.data);

    logic.getApp(ThisModel)
      .then(function(app) {
        res.app = app;
        return logic.opAsync(res.app.models.persona,
          'upsert', data.personaData);
      })
      .catch(function(e) {
        next(e);
      })
      .then(function(results) {
        res.persona = results.persona;
        ctx.results = res;
        data = data.trabajador;
        next(null);
      })
      .catch(function(e) {
        if (res.persona) res.persona.destroy();
        next(e);
      })

  });

  ThisModel.observe('after save', function(ctx, next) {
    var data = ctx.instance ? ctx.instance : ctx.data;
    console.log(data, ctx.results, ctx.res, ctx.reqData);
  });
  */
  Trabajador.createNew = function(data, cb) {

    function getApp() {
      return new Promise(function(resolve, reject) {
        Trabajador.getApp(function(err, app) {
          if (err) reject(err);
          else resolve(app);
        });
      });

    }

    function savePersona(app) {
      return new Promise(function(resolve, reject) {

        //var pathStorage = path.resolve(__dirname, '../../server/storage');
        //var tempImg = path.join(pathStorage, 'temp', data.persona.photo);
        //var fullName = data.persona.firstName + '-' + data.persona.lastName;

        app.models.persona.upsert(data.persona, function(err, p) {
          if (err) reject(err);
          else resolve({app: app, persona: p});
        });

        /*logic.processPhoto(fullName, tempImg, logic.setPhoto(app.models.persona))
          .then(function(photoName) {
            data.persona.photo = photoName.result;
            app.models.persona.upsert(data.persona, function(err, p) {
              if (err) reject(err);
              else resolve({app: app, persona: p});
            });
          }, function(err) {
            reject(err);
          });
        */
      });
    }

    function saveUser(res) {
      return new Promise(function(resolve, reject) {
        res.app.models.user.upsert(data.user,
          function(err, user) {
          if (err) {
            res.persona.destroy();
            reject(err);
          }else {
            data.settings.userId = user.id;
            data.settings.langFallback = 'en'; //in future user pick this

            res.app.models.settings.upsert(data.settings,
              function(err, settings) {
              if (err) {
                user.destroy();
                reject(err);
              }else {
                res.user = user;
                res.settings = settings;
                resolve(saveTrabajador(res));
              }
            });
          }
        });
      });
    }

    function saveRoleMapping(res) {
      return new Promise(function(resolve, reject) {
        //console.log(res.rolesTrabajador);

        var upsertRM = {
          principalType: 'USER',
          principalId: res.user.id,
          roleId: res.rolesTrabajador[0].roleId
        }

        var w = {
          where: {
            and: [
              {principalType: 'USER'},
              {principalId: res.user.id}
            ]
          }
        }

        if (res.user) res.app.models.roleMapping.findOrCreate(w, upsertRM,
          function(err, roleMapping) {
          if (err) {
            res.persona.destroy;
            res.trabajador.destroy();
            if (res.user) {
              res.user.destroy();
              res.settings.destroy();
            }
            for (var i = 0; i < res.rolesTrabajador.length; i++) {
              res.rolesTrabajador[i].destroy();
            }
            reject(err);
          }else {
            res.roleMapping = roleMapping;
            resolve(res);
          }
        });
        else resolve(res);
      });
    }

    function saveTrabajador(res) {
      return new Promise(function(resolve, reject) {
        res.app.models.trabajador.upsert(data.trabajador,
          function(err, trabajador) {
          if (err) {
            res.persona.destroy();
            if (res.user) {
              res.user.destroy();
              res.settings.destroy();
            }
            reject(err);
          }else {
            res.trabajador = trabajador;
            if (res.user) trabajador.userId = res.user.id;
            trabajador.personaId = res.persona.id;
            trabajador.save();
            resolve(res);
          }
        });
      });
    }

    function saveRoles(res) {
      return new Promise(function(resolve, reject) {

        var roleTrs = [];

        async.each(data.oldRoles, function(oldRole, callbackInset) {

          res.app.models.roleTrabajador.find({where: {
            and: [
              {roleId: oldRole.id},
              {trabajadorId: res.trabajador.id}
            ]}},
            function(err, instances) {
            if (err) callbackInset(err);
            else {
              for (var i = 0; i < instances.length; i++) {
                instances[i].destroy();
              }
              callbackInset(null);
            }
          });

        }, function(err) {

          if (err) {
            res.persona.destroy;
            res.trabajador.destroy();
            if (res.user) {
              res.user.destroy();
              res.settings.destroy();
            }
            reject(err);
          }else {
            async.each(data.roles, function(roleId, callback) {

              var upsertRT = {
                trabajadorId: res.trabajador.id,
                roleId: roleId
              }

              res.app.models.roleTrabajador.create(upsertRT,
                function(errRT, roleTrabajador) {
                  if (errRT) callback(err);
                  else {
                    roleTrs.push(roleTrabajador);
                    callback(null);
                  }
                });

            }, function(err) {
              if (err) {
                res.persona.destroy;
                res.trabajador.destroy();
                if (res.user) {
                  res.user.destroy();
                  res.settings.destroy();
                }
                reject(err);
              }
              else {
                res.rolesTrabajador = roleTrs;
                if (res.user) resolve(saveRoleMapping(res));
                else resolve(res);
              }
            });
          }

        });

      });
    }

    function saveRemuneration(res) {
      return new Promise(function(resolve, reject) {
        res.app.models.remuneracion.upsert(data.remuneration,
          function(err, remuneration) {
          if (err) {
            res.rolesTrabajador.destroy();
            if (res.user) {
              res.user.destroy();
              res.roleMapping.destroy();
            }
            reject(err);
          }else {
            res.remuneration = remuneration;
            remuneration.trabajadorId = res.trabajador.id;
            remuneration.save();
            resolve(res);
          }
        });
      });
    }

    getApp()

      .then(function(app) {
        return savePersona(app);
      }, function(err) {
        return new Promise(function(resolve, r) {r(err)});
      })

      .then(function(res) {
        if (data.user) {
          data.user.email = res.persona.email;
          data.user.emailVerified = false;
          data.user.created = Date.now();
          data.user.status = 'pending';
          return saveUser(res);
        }else return saveTrabajador(res);
      }, function(err) {
        return new Promise(function(resolve, r) {r(err)});
      })

      .then(function(res) {
        return saveRoles(res);
      }, function(err) {
        return new Promise(function(resolve, r) {r(err)});
      })

      .then(function(res) {
        return saveRemuneration(res);
      }, function(err) {
        return new Promise(function(resolve, r) {r(err)});
      })

      .then(function(res) {
        delete res.app;
        cb(null, res);
      }, function(err) {
        cb(err);
      });

  };

  Trabajador.remoteMethod('createNew', {
    description: 'create new trabajador with all dependencies',
    http: {verb: 'post'},
    accepts: {
      arg: 'data', type: 'object', description: 'Model instance data',
      http: { source: 'body' }
    },
    returns: {
      arg: 'res',
      type: 'object',
      root: true
    }
  });

};

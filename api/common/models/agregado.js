module.exports = function(Agregado) {

  var ThisModel = Agregado;

  var logic = require('../logic/logic.js');

  logic
    .validate(ThisModel, 'p', 'name')

    .validate(ThisModel, 'u', 'name');

  ThisModel.observe('before save', function(ctx, next) {
    logic.createOrUpdate(ctx, next, ThisModel, 'name');
  });

  ThisModel.observe('before delete', function(ctx, next) {
    logic.deleteById(ctx, next);
  });

};

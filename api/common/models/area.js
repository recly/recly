module.exports = function(Area) {

  var ThisModel = Area;

  var logic = require('../logic/logic.js');

  logic
    .validate(ThisModel, 'p', 'name')
    .validate(ThisModel, 'p', 'empresaId')

    .validate(ThisModel, 'u', 'name');

};

module.exports = function(Persona) {

  var ThisModel = Persona;
  var logic = require('../logic/logic.js');

  logic
    .validate(ThisModel, 'p', 'firstName')
    .validate(ThisModel, 'p', 'lastName')
    .validate(ThisModel, 'p', 'sex')
    .validate(ThisModel, 'p', 'birthdayDate')
    .validate(ThisModel, 'p', 'identityDocument');

};

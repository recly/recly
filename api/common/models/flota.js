module.exports = function(Flota) {

  var ThisModel = Flota;

  var logic = require('../logic/logic.js');

  logic
    .validate(ThisModel, 'p', 'type')

    .validate(ThisModel, 'u', 'type');

  ThisModel.observe('after save', function(ctx, next) {
    logic.getApp(ThisModel)
      .then(function(app) {
        var db = app.dataSources.mongoDs;
        console.log(ctx.instance);
        //app.createModel(ctx.instance.type, {
        //  status: {type: Number}
        //});
        next();
      })
      .catch(function(e) {
        next(e);
      });
  });

};

module.exports = function(Cargo) {

  var ThisModel = Cargo;

  var logic = require('../logic/logic.js');

  logic
    .validate(ThisModel, 'p', 'areaId');

  ThisModel.observe('before save', function(ctx, next) {

    var item = ctx.instance ? ctx.instance : ctx.data;
    item.creationDate = new Date(Date.now());

    next(null);

  });

};

module.exports = function(Picture) {

  var fs = require('fs');
  var path = require('path');

  var storage = path.resolve(__dirname, '../../server/storage');

  Picture.afterRemote('upload', function(ctx, picture, next) {

    var pictureOriginal = picture.result.files.file[0];

    var ext = pictureOriginal.name
      .substring(pictureOriginal.name.lastIndexOf('.'));

    var originalPath = path.join(storage, 'temp', pictureOriginal.name);
    var nameFormalizedPath = path.join(storage, 'temp',
      (picture.result.fields.storage || Date.now().toString()) + ext);

    //nameFormalizedPath = nameFormalizedPath.toLowerCase();
    console.log(nameFormalizedPath);

    fs.rename(originalPath, nameFormalizedPath, function(err) {
      if (err) next(err);
      else {
        picture.result.files.file[0].name = picture.result.fields.storage + ext;
        next(null);
      }
    });

  });

  Picture.beforeRemote('download', function(ctx, picture, next) {
    //console.log(ctx.req.params, ctx.req.body);
    var params = ctx.req.params;
    if (params.container === 'agregados' &&
    !fs.existsSync(path.join(storage, 'agregados', params.file))) {
      //console.log(ctx.req, 'not exits', params.file);
      ctx.req.params = {file: 'default-standard.jpg'};
      //ctx.req.params.file = 'default-standard.jpg';
    }
    next();
  });

  Picture.afterRemote('download', function(ctx, picture, next) {
    //console.log(ctx.req.params, picture);
    next();
  });

};

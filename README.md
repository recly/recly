# Recly

eRecycling es una plataforma web que ha desarrollado un modelo para facilitar la gestión de residuos sólidos desde la fuente con “un solo click”, donde vecinos, mercados, oficinas, empresas, industrias y universidades pueden reportar que su material reciclable se encuentra disponible, al mismo tiempo que la asociación de recicladores más cercana es notificada para su recojo inmediato. Así, los recicladores no tendrán que exponer su integridad en rellenos sanitarios y la sociedad podrá reciclar desde la comodidad de su entorno.

## Equipo
- Jorge rios
- Omar Lozada  [@Skeiter9](http://twitter.com/skeiter9)
- Jose Coronel [@Jocar_5](http://twitter.com/jocar_5)

## Introducción

Trabajamos separando nuestra api y la interface, solo contamos con interfaces para web, dejando la oportunidad
de crear interfaces nativas para Android o IOS u otro sistema operativo. Para ahorra esfuerzos y lograr
llegar a los mayores nichos de usuarios en distintantas plataformas como las ya mencionandas, nuestros ficheros
javascript estan diseñados de manera que con ayuda de [ng-cordova](https://github.com/driftyco/ng-cordova) podriamos
obetener una app hibrida.

##Instalación

Tanto api como client-web funcionan independientemente, pero uno sin el otro no serían capaces de brindarnos
la experiencia de la app completa.

1. localizarnos en la carpeta __api__ y a continuación:
> npm install

2. una vez que tenemos todos las librerias instaladas podemos correr nuestra api
> npm start

3. Acontinuación podemos ingresar a la dirección __http://localhost:3000__ y conprobaremos que nuestra api está lista para trabajar

4. localizarnos en la carpeta __client-web__ y a continuación:
> npm install

5. una vez que tenemos todos las librerias instaladas podemos correr nuestra interface web
> npm start

6. Acontinuación podemos ingresar a la dirección __http://localhost:1984__ y podemos a empezar a disfrutar de __RECLY__.

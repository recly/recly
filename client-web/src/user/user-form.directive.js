'use strict';

require('angular-material');

module.exports = angular
  .module('appUsersForm', [
    require('../components/input-picture.directive').name,
    'ngMaterial'
  ])
  .directive('userForm', usersFormDirective);

function usersFormDirective(User, Role, layoutService, apiConsama) {

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/users-form.jade')(),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'userForm',
    link: link
  };

  function link(scope, element, attrs, ctrlForm) {

    init();

    ctrlForm.save = function(form) {
      layoutService.logic.formUpsert(User, form, ctrlForm);
    }

    //--
    function init() {

      ctrlForm.update = ctrlForm.item ? true : false;
      ctrlForm.form = ctrlForm.update ?
        angular.extend({}, ctrlForm.item) : {};
      ctrlForm.formAux = {};

      ctrlForm.cover =  apiConsama.getPhoto('personas',
        ctrlForm.update ? ctrlForm.item.photo : 'default.jpg',
        'cover');

      loadRoles();

    }

    function loadRoles() {
      Role.find({filter: {where: {status: 1}}}).$promise
        .then(function(roles) {
          ctrlForm.roles = roles;
        }, function(err) {
          console.warn(err);
        });
    }

  }

}
usersFormDirective.$inject = ['User', 'Role', 'layoutService', 'apiConsama'];

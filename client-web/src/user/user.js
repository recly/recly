'use strict';

module.exports = angular
  .module('appUsers', [
    require('./user-list.directive.js').name,
    require('./user-show.directive.js').name,
    require('./user-form.directive.js').name
  ]);

'use strict';

module.exports = angular
  .module('appUsersShow', [])
  .controller('UsersShowDirectiveController',
    UsersShowDirectiveController)
  .directive('usersShow', usersShow);

function UsersShowDirectiveController() {}
UsersShowDirectiveController.$inject = [];

function usersShow(apiConsama) {

  function link(scope, elem, attrs, user) {
    user.getPhoto = apiConsama.getPhoto('personas',
      user.item.persona.photo, 'cover');

  }

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/users-show'),
    bindToController: true,
    controller: 'UsersShowDirectiveController',
    controllerAs: 'user',
    link: link
  };

}
usersShow.$inject = ['apiConsama'];

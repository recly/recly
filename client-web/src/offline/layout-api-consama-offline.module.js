'use strict';

require('angular-translate');
require('angular-translate-loader-partial');

module.exports = angular
  .module('appApiConsamaOffline', [
    'pascalprecht.translate',
  ])
  .config(config)
  .controller('ApiConsamaOfflineController', ApiConsamaOfflineControllerFn);

function config($translateProvider) {

  var apiUrl = 'http://localhost:3000/api/v1';
  var ls = localStorage;

  $translateProvider
    .useLoader('$translatePartialLoader', {
      urlTemplate: (ls.getItem('ngStorage-apiConsamaOffline') === null) ?
        apiUrl + '/settings/translate?part={part}&lang={lang}' :
        '/i18n/{part}/{part}-{lang}.json'
    });
}
config.$inject = ['$translateProvider'];

function ApiConsamaOfflineControllerFn(layoutService) {

  var dashboard = this;
  layoutService.logic.appbarTitleChange('API consama Offline');

}

ApiConsamaOfflineControllerFn.$inject = ['layoutService'];

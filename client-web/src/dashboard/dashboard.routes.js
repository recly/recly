'use strict';

export function rules(urlRouterProvider) {
  urlRouterProvider.when('/panel-de-control', '/');
}

export function routes(stateProvider) {

  stateProvider

    .state('dashboard', {
      parent: 'layout',
      url: '/',
      resolve: {
        boot: ['layoutFactory', (lF) => lF.stateLoad('dashboard')]
      },
      views: {
        'content': {
          controllerAs: 'dashboard',
          templateProvider: ['layoutFactory', (lF) => lF.ui.viewTemplate],
          controllerProvider: ['layoutFactory', (lF) => lF.ui.controllerName]
        }
      }
    });
}

'use strict';

require('./styles/dashboard.scss');

import dashboardControllerFn from './dashboard.controller';

module.exports = angular
  .module('appDashboard', [
    //require('../components/masonry.directive').name,
    //require('../components/module-card.directive').name,
    require('../components/module-toolbar.directive').name
  ])
  .controller('dashboardController', dashboardControllerFn);

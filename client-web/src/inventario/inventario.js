'use strict';

module.exports = angular
  .module('inventario', [
    require('./inventario-list.directive.js').name
  ]);

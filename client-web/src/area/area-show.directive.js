'use strict';

export default angular
  .module('areaShow', [])
  .directive('areaShow', [areaShowDirectiveFn]);

function areaShowDirectiveFn() {

  return {
    scope: {
      area: '=item',
      options: '='
    },
    restrict: 'E',
    template: require('./templates/area-show.jade')(),
    link: link
  };

  function link(scope, elem, attrs) {
  }

}

'use strict';

import areaForm from './area-form.directive.js';
import areaList from './area-list.directive.js';
import areaShow from './area-show.directive.js';

export default angular
  .module('area', [
    areaShow.name,
    areaList.name,
    areaForm.name
  ]);

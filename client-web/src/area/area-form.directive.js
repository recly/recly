'use strict';

export default angular
  .module('areaForm', [])
  .directive('areaForm', ['layoutFactory', 'Area', (lF, A) => {

    return {
      scope: {
        item: '=',
        options: '='
      },
      restrict: 'E',
      template: require('./templates/area-form.jade')(),
      controller: angular.noop,
      controllerAs: 'mForm',
      bindToController: true,
      compile(tE) {

        tE.find('form').eq(0).attr('ng-class',
          'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');

        return (scope, element, attrs, ctrlForm) => {

          lF.getFormInitial(scope, element, ctrlForm, ['empresa'], 'area')

            .then((formData) => {
              ctrlForm.form = formData;
              ctrlForm.save = (form) => lF.formUpsert(A, form, ctrlForm);
            })

            .catch((e) => {
              console.warn(e);
            });

        };
      }
    };

  }]);

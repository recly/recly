'use strict';

module.exports = angular
  .module('appLogin', [
    require('./login-form.directive').name,
    require('./../utils/valid-form.factory').name
  ])
  .controller('loginController', require('./login.controller'));

'use strict';

module.exports = LoginController;

function LoginController(layoutService) {

  layoutService.logic.appbarTitleChange('login');

}
LoginController.$inject = ['layoutService'];

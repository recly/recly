'use strict';

export function routes(stateProvider) {

  stateProvider
    .state('login', {
      url: '/login',
      parent: 'layout',
      resolve: {
        boot: ['layoutService', (lS) => lS.logic.loadState((res) => {
          res.toLoad.push(lS.logic.getUrlFile('login'));
          lS.logic.loadTranslate('login');
          return res;
        })]
      },
      views: {
        'content@base': {
          template: require('./views/login.jade')(),
          controller: 'LoginController',
          controllerAs: 'login'
        }
      }
    });

}

'use strict';

module.exports = angular
  .module('appLoginForm', [
    require('../api/api').name,
    require('./../utils/capitalize.filter').name
  ])
  .directive('loginForm', loginForm)
  .controller('LoginFormController', LoginFormController)

function LoginFormController($mdToast, $state, $translate,
  yeValidForm, capitalizeFilter, $q, User, Settings) {

  var login = this;

  login.data = {};
  login.submit = submit;

  function submit(form) {

    var deferred = $q.defer();

    yeValidForm(form)

      .then(function(result) {
        return User.login(login.data).$promise;
      }, function(err) {
        return $q(function(resolve, r) {
          err.errorOne.message.then(function(errMessage) {
            r(errMessage)
          });
        })
      })

      .then(function(loginSucces) {
        return $q.when(loginSucces);
      }, function(err) {
        return (angular.isString(err)) ?
          $q(function(resolve, r) {r(err)}) :
          $q(function(resolve, r) {
            $translate(err.data.error.code).then(function(errMessage) {
              r(errMessage)
            });
          })
      })

      .then(function(loginSucces) {
        return ($state.current.name === 'dashboard') ?
          $state.reload('layout') : $state.go('dashboard');
      }, function(errMessage) {
        return $mdToast.showSimple(errMessage);
      })

      .then(function(data) {
        //console.log(data);
        deferred.resolve();
      }, function(err) {
        //console.warn(err);
        deferred.reject(err);
      })

    return deferred.promise;

  }

}

LoginFormController.$inject = ['$mdToast', '$state', '$translate',
  'yeValidForm', 'capitalizeFilter', '$q', 'User', 'Settings']

function loginForm() {

  return {
    scope: {},
    template: require('./templates/login-form'),
    controller: 'LoginFormController',
    controllerAs: 'login'
  }
}

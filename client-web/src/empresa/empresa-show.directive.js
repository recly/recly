'use strict';

module.exports = angular
  .module('empresaShow', [])
  .directive('empresaShow', empresaShowDirectiveFn);

function empresaShowDirectiveFn(apiConsama) {

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/empresa-show.jade')(),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'empresa',
    link: link
  };

  function link(scope, elem, attrs, empresa) {

    empresa.getPhoto = apiConsama.getPhoto('empresas',
      empresa.item.photo, 'cover');

  }

}
empresaShowDirectiveFn.$inject = ['apiConsama'];

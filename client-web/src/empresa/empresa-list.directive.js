'use strict';

module.exports = angular
  .module('empresaList', [])
  .directive('empresa', empresaListDirectiveFn);

function empresaListDirectiveFn(layoutService, Empresa, $q) {

  return {
    scope: {
      module: '='
    },
    restrict: 'E',
    template: require('./templates/empresa.jade')(),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'empresaList',
    link: link
  };

  function link(scope, elem, attrs, empresaList) {

    var moduleResource = Empresa;
    var moduleController = empresaList;
    var moduleName = 'empresa';

    moduleController.data = function() {

      var deferred = $q.defer();
      moduleResource.find({filter: {include: {sucursales: 'almacenes'}}})
        .$promise
        .then(function(data) {
          deferred.resolve({
            items: data,
            pattern: layoutService.logic.listPattern(moduleName)
          });
        }, function(err) {
          console.error(err);
          deferred.reject(err);
        });
      return deferred.promise;

    }

    moduleController.showItem = function(evt, item, items) {
      layoutService.logic.showItem(scope, moduleController, evt, item, items);
    };

    moduleController.deleteItem = function deleteItem(evt, item, items) {
      layoutService.logic.deleteItem(scope, moduleController, evt, item, items,
        exeDelete);
    }

    moduleController.updateItem = function(evt, item, items, fromSN) {
      layoutService.logic.updateItem(scope, moduleController, evt, item, items,
        fromSN);
    };

    moduleController.createItem = function(evt) {
      layoutService.logic.createItem(scope, moduleController, evt);
    };

    //--

    moduleController.toggleCard = layoutService
      .logic.toggleCard.bind(this, moduleController, scope);

    moduleController.toggleTypeListCard = layoutService
      .logic.toggleTypeListCard.bind(this, moduleController, scope);

    function exeDelete(item) {
      return moduleResource.destroyById({id: item.id}).$promise;
    }

  }

}
empresaListDirectiveFn.$inject = ['layoutService', 'Empresa', '$q'];

'use strict';

import empresaFormAM from './empresa-form.directive';

module.exports = angular
  .module('empresa', [
    require('./empresa-list.directive').name,
    require('./empresa-show.directive').name,
    empresaFormAM.name
  ]);

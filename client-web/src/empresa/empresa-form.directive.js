'use strict';

require('angular-material');

export default angular
  .module('appEmpresasForm', [
    require('../components/input-picture.directive').name,
    require('../components/input-picker.directive').name,
    require('../utils/valid-form.factory').name,
    'ngMaterial'
  ])
  .directive('empresaForm', ['apiConsama', 'Empresa', '$translate',
    'layoutFactory', empresaFormFn]);

function empresaFormFn(apiConsama, E, $tr, lF) {

  return {
    scope: {
      item: '=',
      options: '='
    },
    restrict: 'E',
    template: require('./templates/empresa-form')(),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'eF',
    link: link
  };

  function link(scope, element, attrs, eF) {

    var modulePluralName = 'empresas';
    var mR = E;

    init();

    function init() {

      $tr('MODEL.EMPRESA').then((mName) => {eF.modelName = mName;});

      eF.update = angular.isDefined(eF.item) ? true : false;

      eF.form = eF.update ? eF.item : {};
      eF.formAux = {};//getFormAux(eF.form);

      eF.cover = apiConsama.getPhoto(modulePluralName,
        eF.update ? eF.item.photo : 'default.jpg', 'cover');

      if (eF.update) eF.form.creationDate = new Date(eF.item.creationDate);

      eF.formAux.maxCreationDate = lF.getDate(0);

    }

    eF.save = (form) => lF.formUpsert(E, form, eF);

  }

}

//--
/*
function getFormAux(form) {
  let res = {};
  if (angular.isObject(form.geoDirection)) res
    .geoDirection = parseCoordinates(form.geoDirection);
  if (angular.isObject(form.geoFiscalAddress)) res
    .geoFiscalAddress = parseCoordinates(form.geoFiscalAddress);
  return res;
}

function parseCoordinates(coors) {
  return `${coors.lat}, ${coors.lng}`;
}
*/

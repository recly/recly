'use strict';

module.exports = angular
  .module('appUtilsPictureFill', [])
  .factory('yePictureFill', getPictureFill);

function getPictureFill() {
  return require('picturefill');
}

getPictureFill.$inject = [];

'use strict';

module.exports = angular
  .module('components.validForm', [])
  .factory('yeValidFormMessage', validFormMessage)
  .factory('yeValidForm', validForm);

function validForm(yeValidFormMessage, $translate, $q) {

  return (form) => {

    var deferred = $q.defer();

    var errors = {};
    var errorOne = {};
    var erroOneFill = false;

    if (form.$invalid) {

      angular.forEach(form.$error, (value, typeError) => {

        angular.forEach(value, (inputFail) => {

          var condition = 'condition'; //replace
          var input = inputFail.$name;

          if (!erroOneFill) {
            erroOneFill = true;
            errorOne = {
              error: typeError,
              value: inputFail.$viewValue,
              message: yeValidFormMessage(typeError, inputFail.$name,
                inputFail.$viewValue, condition)
            };
          }

          if (angular.isUndefined(this[input])) this[input] = [];

          this[input].push({
            error: typeError,
            value: inputFail.$viewValue ,
            message: yeValidFormMessage(typeError, input,
              inputFail.$viewValue, condition)
          });

        }, errors);

      });

      $translate('FORM.WRONG')
        .then(function(sentence) {
          deferred.reject({
            status: false,
            errors: errors,
            errorOne: errorOne,
            message: sentence
          });
        });

    }else $translate('FORM.CORRECT').then(function(sentence) {
      deferred.resolve({
        status: true,
        message: sentence
      });
    });

    return deferred.promise;

  };
}
validForm.$inject = ['yeValidFormMessage', '$translate', '$q'];

function validFormMessage($q, $translate) {

  return function(errorName, field, valueFail, condition) {
    var deferred = $q.defer();

    var msgID = 'FORM.FIELD_SUBMITED_ERROR.';

    switch (errorName) {
      case 'email': msgID += 'EMAIL'; break;
      case 'minlength': msgID += 'MIN_LENGTH'; break;
      case 'maxlength': msgID += 'MAX_LENGTH'; break;
      case 'number': msgID += 'NUMBER'; break;
      case 'pattern': msgID += 'PATTERN'; break;
      case 'required': msgID += 'REQUIRED'; break;
      case 'url': msgID += 'URL'; break;
      case 'date': msgID += 'DATE'; break;
      case 'time': msgID += 'TIME'; break;
      case 'week': msgID += 'WEEK'; break;
      case 'month': msgID += 'MONTH'; break;//loopback errors response
      case 'presence': msgID += 'PRESENCE'; break;
      case 'uniqueness': msgID += 'UNIQUENESS'; break;
      default: msgID += 'ERROR'; break;
    }

    $translate(field.toUpperCase())
      .then(function(fieldName) {
        return $translate(msgID, {field: fieldName.toUpperCase()});
      })
      .then(function(message) {
        deferred.resolve(message);
      });

    return deferred.promise;
  };

}
validFormMessage.$inject = ['$q', '$translate'];

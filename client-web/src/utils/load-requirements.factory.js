'use strict';

require('oclazyload');
require('angular-translate');
require('angular-translate-loader-partial');

module.exports = angular
  .module('utilsLoadRequirementsTS', [
    //'oc.lazyLoad',
    'pascalprecht.translate'
  ])
  .factory('yeLoadTranslateAndScript', yeLoadTranslateAndScriptFactoryFn);

function yeLoadTranslateAndScriptFactoryFn($translatePartialLoader,
  $ocLazyLoad, $q, $translate) {

  return function(modulePlural) {

    var deferred = $q.defer();

    var loadScripts = {};

    if ($translatePartialLoader.getRegisteredParts()
      .indexOf(modulePlural) === -1) {
      $translatePartialLoader.addPart(modulePlural);
      loadScripts.refreshTranslate = true;
    }
    if (!$ocLazyLoad.isLoaded('app' + modulePlural)) {
      loadScripts.loadScriptModel = true;
    }

    $q.all([
      loadScripts.loadScriptModel ? $ocLazyLoad.load('build/' +
        modulePlural + '.bundle.js') : $q.when(1)//,
      //loadScripts.refreshTranslate ? $translate.refresh() : $q.when(2)
    ]).then(function() {
      deferred.resolve();
    }, function(err) {
      deferred.reject(err);
    });

    return deferred.promise;

  }

}
yeLoadTranslateAndScriptFactoryFn.$inject = ['$translatePartialLoader',
  '$ocLazyLoad', '$q', '$translate'];

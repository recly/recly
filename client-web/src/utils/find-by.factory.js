'use strict';

module.exports = angular
  .module('appUtilsFindBy', [])
  .factory('yeFindBy', findBy);

function findBy() {

  return function findByField(collection, searchKey, searchValue) {
    var result;
    var searchValueIndex = -1;

    angular.forEach(collection, function(value, index) {
      if (value[searchKey] == searchValue) {
        result = value;
        searchValueIndex = index;
      };
    });

    return {item: result, index: searchValueIndex};
  };

}
findBy.$inject = [];

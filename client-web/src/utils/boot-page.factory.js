'use strict';

module.exports = angular
  .module('utilsBootPage', [])
  .factory('bootPage', bootPage);

function bootPage($window, $document, $translate, tmhDynamicLocale,
  $translatePartialLoader, getLanguage, $ocLazyLoad, $q, Settings,
  apiConsamaOffline, $http, User) {

  var deferred = $q.defer();

  var loggued = (localStorage.getItem('$LoopBack$accessTokenId') !== null);
  var tempPromise = (loggued) ? User.findOne({
      filter: {
        where: {id: localStorage.getItem('$LoopBack$currentUserId')},
        include: ['settings', {roles: 'acls'}]
      }
    }).$promise :
    Settings.public().$promise;

  $q.all([
    //$ocLazyLoad.load('build/app.css'),
    tempPromise//,
    //$http({method: 'get', url: 'http://freegeoip.net/json'})
  ])
    .then(function(results) {
      //apiConsamaSocket.emit('initConnect', results[2].data);

      var userLanguage = (loggued) ? results[1].settings.preferredLanguage :
        getLanguage(results[1].langsAvaibles);

      if (loggued) {
        layoutService.data.dataUser = results[1];
        layoutService.data.modules = results[1].settings.modules;
        layoutService.data.settings = results[1].settings;
        layoutService.data.languages.currentLang = results[1].settings
          .preferredLanguage;
        layoutService.data.loggued = true;
        $translate.fallbackLanguage(results[1].settings.langFallback);
        $translate.preferredLanguage(results[1].settings.preferredLanguage);
      }else {
        $translate.fallbackLanguage(results[1].langFallback);
        $translate.preferredLanguage(results[1].preferredLanguage);
      }
      tmhDynamicLocale.set(userLanguage);
      $translate.use(userLanguage);
      $translatePartialLoader.addPart('layout');

      layoutService.data.languages = {
        avaibles: (loggued) ? results[1].settings.langsAvaibles :
          results[1].langsAvaibles,
        currentLang: userLanguage
      }
      //remove spinner
      document.getElementById('spinner').remove();
      deferred.resolve();
    }, function(error) {
      if (error.status === 0 && error.statusText === '') {
        var userLanguage = getLanguage(apiConsamaOffline.langsAvaibles);

        tmhDynamicLocale.set(userLanguage);

        layoutService.data.languages = {
          avaibles: apiConsamaOffline.langsAvaibles,
          currentLang: userLanguage
        }

        $ocLazyLoad.load('build/api-consama-offline.bundle.js')
          .then(function(offlineResults) {
            $translate.fallbackLanguage(apiConsamaOffline.langFallback);
            $translate.preferredLanguage(apiConsamaOffline.preferredLanguage);
            $translate.use(userLanguage);
            $translatePartialLoader.addPart('layout');
            deferred.resolve();
          }, function(offlineErrs) {
            console.log(offlineErrs);
            deferred.reject(offlineErrs);
          });

      }else {
        deferred.reject(error);
      }
    })

  return deferred.promise;

}

bootPage.$inject = ['$window', '$document', '$translate', 'tmhDynamicLocale',
  '$translatePartialLoader', 'getLanguage', '$ocLazyLoad', '$q', 'Settings',
  'apiConsamaOffline', '$http', 'User'];

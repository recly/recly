'use srict';

module.exports = angular
  .module('platformEnviroment', [])
  .factory('yePlatform', ['$window', ($w) => {
    return {
      isTouchScreen: 'ontouchstart' in $w || 'onmsgesturechange' in $w
    };
  }]);
/*
let isAgentMobile = {
  Android: () => {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: () => {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: () => {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: () => {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: () => {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: () => {
    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() ||
      isMobile.Opera() || isMobile.Windows());
  }
};
*/

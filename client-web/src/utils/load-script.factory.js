'use strict';

module.exports = angular
  .module('utilLoadScript', [])
  .factory('yeLoadScript', loadScript);

function loadScript($timeout, $q, $document) {

  return function loadScriptProccess(url) {
    var deferred = $q.defer();

    var script = $document[0].createElement('script');
    var body = $document[0].body;
    var scriptsLoaded = body.getElementsByTagName('script');
    var lastScript = scriptsLoaded[scriptsLoaded.length - 1];
    var removed = false;

    script.type = 'text/javascript';
    if (script.readyState) { // IE
      script.onreadystatechange = function() {
        if (script.readyState === 'complete' ||
            script.readyState === 'loaded') {
          script.onreadystatechange = null;
          $timeout(
            function() {
              if (removed) return;
              removed = true;
              body.removeChild(script);
              //callback();
            }, 30, false);
        }
      };
    } else { // Others
      script.onload = function() {
        if (removed) return;
        removed = true;
        body.removeChild(script);
        //callback();
        deferred.resolve();
      };
      script.onerror = function() {
        if (removed) return;
        removed = true;
        body.removeChild(script);
        //errorCallback();
        deferred.reject();
      };
    }
    script.src = url;
    script.async = false;
    lastScript.parentNode.insertBefore(script, lastScript);
    //body.appendChild(script);
    return deferred.promise
  }

}
loadScript.$inject = ['$timeout', '$q', '$document'];

'use strict';

export default angular
  .module('yetranslateFilter', [])
  .filter('yetranslate', ['$translate', '$q', yeTranslateFilterFn]);

function yeTranslateFilterFn($t, $q) {

  let translateFilter = (sentence, deps) => {
    let toTranslate = [];

    angular.forEach(deps, (preDepValue, preDepName) => {
      toTranslate.push(preDepValue.toUpperCase());
    });

    console.log(sentence, deps, toTranslate);

    return $t.instant(toTranslate)
      .then((t) => {
        angular.forEach(deps, (preDepValue, preDepName) => {
          deps[preDepName] = t[preDepValue.toUpperCase()];
        });
        return $t.instant(sentence, deps);
      });
  };

  translateFilter.$stateful = true;

  return translateFilter;

}

'use srict';

module.exports = angular
  .module('yeUtilLanguage', [])
  .factory('getLanguage', getLanguage);

function getLanguage(apiConsamaOffline, $window) {
  var lang = $window.navigator.language || $window.navigator.languages[0];

  return function(languages) {
    var index = languages.indexOf(lang.toLowerCase());
    if (index !== -1) return lang;

    var indexAux = lang.indexOf('-');
    var auxLanguage = (indexAux !== -1) ?
      lang.substring(0, indexAux) :
      lang.substring(0, lang.length);

    var index = languages.indexOf(auxLanguage);
    if (index !== -1) return auxLanguage;

    return apiConsamaOffline.preferredLanguage;
  }
}
getLanguage.$inject = ['apiConsamaOffline', '$window'];

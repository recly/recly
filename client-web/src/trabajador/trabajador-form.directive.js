'use strict';

require('angular-material');

module.exports = angular
  .module('trabajadoresFormDirective', [
    require('../components/input-picker.directive').name,
    require('../components/input-picture.directive').name,
    //require('../utils/valid-form.factory').name,
    //'ngMaterial'
  ])
  .directive('trabajadorForm', ['apiConsama', 'Role', '$log',
    'yeValidForm', '$controller', '$mdToast', '$q', 'Trabajador', '$translate',
    'Afp', 'Snp', 'Translates', '$mdDialog', 'layoutService', 'layoutFactory',
    'Settings', 'Area', trabajadorFormDirectiveFn]);

function trabajadorFormDirectiveFn(apiConsama, R, $l, yeValidForm,
  $controller, $mdT, $q, T, $translate, Afp, Snp, Translates,
  $mdDialog, layoutService, lF, S, A) {

  return {
    scope: {
      item: '=',
      options: '='
    },
    controller: angular.noop,
    controllerAs: 'mForm',
    bindToController: true,
    template: require('./templates/trabajador-form.jade')(),
    compile(tE) {
      tE.find('form').eq(0).attr('ng-class',
        'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');
      return link;
    }
  };

  function link(s, elem, attrs, ctrl) {

    init()
      .then((result) => {
        ctrl.roles = result.roles;
        ctrl.languages = result.languages;
        ctrl.formAux = result.formAux;
        return lF.getFormInitial(s, elem, ctrl, ['empresa'], 'trabajador',
          {photo: 'persona.photo', container: 'personas'});
      })

      .then((formData) => {

        ctrl.form = formData;

        ctrl.loadAfps = () => Afp.find().$promise
          .then((afps) => ctrl.afps = afps)
          .catch((err) => $l.error(err));

        ctrl.loadCargos = () => A.find(
          {filter: {
            include: 'cargos',
            where: {'empresaId': ctrl.form.empresaId}
          }}).$promise
          .then((areas) => ctrl.areas = areas)
          .catch((err) => $l.error(err));

        ctrl.userLanguagesChange = (preferredLang) => {
          console.log(preferredLang);
          let band = false;

          angular.forEach(ctrl.formAux.langsAvaibles, (status, lang) => {
            if (status) {band = true; ctrl.auxLastAvaible = lang;}
          });
          //"at least the new user should has a language"
          if (!band) {
            ctrl.formAux.langsAvaibles[ctrl.auxLastAvaible] = true;
            preferredLang = preferredLang !== ctrl.auxLastAvaible ?
              ctrl.auxLastAvaible : preferredLang;
            $translate('USER_NEW.AT_LEAST_ONE_LANG')
              .then((warning) => $mdT.showSimple(warning));
          }
        };

        ctrl.payType = () => {
          if (ctrl.form.remuneration.payType === 'honorarios') {
            ctrl.form.remuneration.numPlanilla = null;
            ctrl.form.remuneration.afpId = null;
            ctrl.form.remuneration.snpId = null;
            ctrl.form.remuneration.aportationFamiliar = false;
          }
        };

        ctrl.jubilation = () => {
          if (ctrl.formAux.jubilationType === 'snp') {
            //ctrl.getSnpId();
            ctrl.form.remuneration.afpId = null;
          }else if (ctrl.formAux.jubilationType === 'afp') {
            ctrl.form.remuneration.snpId = null;
          }
        };

        ctrl.save = (form) => {

          ctrl.form.roles = rolesSelected(ctrl.formAux.roles);

          if (
            ctrl.formAux.roles.length === 0
          ) return $l.warn('TRABAJADOR.WITHOUT_ROLE');

          if (
            ctrl.formAux.auxAddUser &&
            ctrl.form.user.password !== ctrl.formAux.user.rePassword
          ) return $l.warn('TRABAJADOR.PASSWORDS_INCORRECT');

          //return lF.formUpsert(T, form, ctrl, 'createNew');
        };

      })

      .catch((err) => $l.warn(err));

    function init() {

      let res = {};

      res.formAux = {
        //jubilationType: userJubilationType(ctrl.item),
        roles: {},
        days: getDays(28),
        birthdayDateMin: lF.getDate(90, true),
        birthdayDateMax: lF.getDate(18, false),
        contratationDateMin: '2000-08-30',
        contratationDateMax: lF.getDate(0)
      };

      //ctrl.afps = [];
      //ctrl.languages = [];

      //ctrl.form = ctrl.update ? formateItemToUpdate(ctrl.item) : {};

      return $q.all([
        R.find().$promise,
        S.public().$promise
      ])
        .then((results) => {
          res.roles = results[0];
          res.languages = results[1].langsAvaibles;
          return res;
        })
        .catch((err) => err);

    }

    function userJubilationType(item) {
      if (
        angular.isUndefined(item) ||
        angular.isUndefined(item.remuneracion)
      ) return;
      return angular.isNumber(item.remuneracion.snpId) ? 'snp' : 'afp';
    }

    function getDays(limit) {
      let days = [];
      for (let i = 1; i <= limit; i++) (function(numberDay) {
        days.push(numberDay);
      })(i);
      return days;
    }

    function rolesSelected(roles) {
      var res = [];
      angular.forEach(roles, function(status, roleId) {
        if (status) res.push(roleId);
      });
      return res;
    }

    ctrl.getSnpId = () => {
      return Snp.findOne().$promise
        .then((snp) => {
          ctrl.form.remuneration.snpId = snp.id;
        })
        .catch((err) => {
          ctrl.form.remuneration.snpId = false;
        });
    };

  }

  function link2(scope, element, attrs, tF) {

    init();

    tF.addUserChange = () => loadLanguages();

    tF.userLanguagesChange = () => {

      let band = false;

      forEach(tF.formAux.langsAvaibles,
        function(status, lang) {
        if (status) {band = true; tF.auxLastAvaible = lang;}
      });
      //"at least the new user should has a language"
      if (!band) {
        tF.formAux.langsAvaibles[tF.auxLastAvaible] = true;
        $translate('USER_NEW.AT_LEAST_ONE_LANG').then(function(warning) {
          $mdToast.showSimple(warning);
        });
      }
    };

    tF.save = function save(form) {

      var deferred = $q.defer();

      form.$setDirty();

      yeValidForm(form)

        .then(function(result) {

          if (tF.formAux.roles.length === 0) return $q
            .when('TRABAJADOR.WITHOUT_ROLE');

          if (tF.formAux.auxAddUser && tF.form.user.password !== tF.formAux
            .user.rePassword) return $q.when('TRABAJADOR.PASSWORDS_INCORRECT');

          var typeForm = tF.update ? 'UPDATE' : 'NEW';

          if (tF.formAux.auxAddUser) {
            tF.form.settings.langsAvaibles = [];
            forEach(tF.formAux.langsAvaibles, function(status, lang) {
              if (status) tF.form.settings.langsAvaibles.push(lang);
            });
          }

          if (angular.isUndefined(tF.form.persona.photo)) tF.form.persona
            .photo = 'default.jpg';

          tF.form.roles = rolesSelected(tF.formAux.roles);

          var fullName = tF.form.persona.firstName + ' ' + tF.form.persona
            .lastName;

          return Trabajador.createNew(tF.form).$promise

            .then(function(trabajador) {
              console.log(trabajador);
              return layoutService.logic.resolveSidenav('right', false)
                .then(function() {
                  return $translate('FORM.' + typeForm + '_SUCCESS',
                    {itemName: fullName})
                })
                .then(function(sentence) {
                  return sentence;
                });

            }, function(err) {
              console.log(err);
              return $translate('FORM.' + typeForm + '_FAIL', {itemName:
                fullName})
                .then(function(msg) {return msg});

            })

        }, function(err) {
          return err.errorOne.message.then(function(errMessage) {
            return errMessage;
          });
        })

        .then(function(msg) {
          return $mdToast.showSimple(msg);
        })

        .then(function() {
          deferred.resolve();;
        });

      return deferred.promise;

    }

    //-
    function init() {

      tF.update = angular.isDefined(tF.item) ? true : false;

      loadRoles();

      tF.formAux = {
        jubilationType: userJubilationType(tF.item),
        langsAvaibles: userLangsAvaibles(tF.item),
        cover: getCover(tF.item),
        days: getDays(28),
        roles: userRoles(tF.item),
        birthdayDateMin: getAge(90, true),
        birthdayDateMax: getAge(18, false),
        contratationDateMin: '2000-08-30',
        contratationDateMax: getAge(0)
      };

      tF.roles = [];
      tF.afps = [];
      tF.languages = [];

      tF.form = tF.update ? formateItemToUpdate(tF.item) : {};

    }

    function userRoles(item) {
      var result = {};
      if (angular.isUndefined(item)) return result;
      for (var i = 0; i < item.roles.length; i++)(function(role) {
        result[role.id] = true;
      })(item.roles[i]);
      return result;
    }

    function userLangsAvaibles(item) {
      var result = {};
      if (angular.isUndefined(item)) return result;
      if (angular.isUndefined(item.user)) return result;
      for (var i = 0; i < item.user.settings.langsAvaibles
        .length; i++)(function(roleName) {
        result[roleName] = true;
      })(item.user.settings.langsAvaibles[i]);
      return result;
    }

    function userJubilationType(item) {
      if (angular.isUndefined(item)) return;
      return angular.isNumber(item.remuneracion.snpId) ? 'snp' : 'afp';
    }

    function formateItemToUpdate(item) {

      //console.log(item);
      var updateItem = {
        persona: angular.extend({}, item.persona),
        trabajador: {id: item.id},
        remuneration: angular.extend({}, item.remuneracion),
        oldRoles: angular.extend([], item.roles)
      };

      updateItem.persona.birthdayDate = new Date(item.persona.birthdayDate);
      updateItem.trabajador.contratationDate = new Date(item.contratationDate);
      updateItem.trabajador.payday = item.payday;

      if (item.user) {

        loadLanguages();
        tF.formAux.auxAddUser = true;

        updateItem.user = {
          id: item.user.id,
          username: item.user.username
        };
        updateItem.settings = {
          id: item.user.settings.id,
          preferredLanguage: item.user.settings.preferredLanguage
        };
      }

      //console.log(updateItem);
      return updateItem;
    }

    function rolesSelected(roles) {
      var res = [];
      angular.forEach(roles, function(status, roleId) {
        if (status) res.push(roleId);
      });
      return res;
    }

    function getCover(item) {
      return apiConsama.getPhoto('personas',
        angular.isDefined(item) ? item.persona.photo : 'default.jpg', 'cover');
    }

    function loadRoles() {
      Role.find().$promise
        .then(function(roles) {
          tF.roles = roles;
        }, function(err) {
          $l.error(err);
        });
    }

    function loadLanguages() {
      Translates.find().$promise
        .then(function(translates) {
          tF.languages = translates;
        }, function(err) {
          $l.error(err);
        });
    }

    function getDays(limit) {
      var days = [];
      for (var i = 0; i <= limit; i++) (function(numberDay) {
        days.push(numberDay);
      })(i);
      return days;
    }

  }
}

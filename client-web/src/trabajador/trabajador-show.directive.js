'use strict';

module.exports = angular
  .module('appTrabajadoresShow', [
  ])
  .directive('trabajadorShow', trabajadoresShowDirectiveFn);

function trabajadoresShowDirectiveFn(apiConsama) {

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/trabajador-show.jade')(),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'trabajadorShow',
    link: link
  };

  function link(scope, elem, attrs, trabajadorShow) {
    scope.trabajador = trabajadorShow.item;
    console.log(scope.trabajador);
  }

}
trabajadoresShowDirectiveFn.$inject = ['apiConsama'];

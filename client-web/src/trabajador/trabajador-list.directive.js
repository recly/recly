'use strict';

module.exports = angular
  .module('trabajadorList', [
    //require('../utils/find-by.factory').name
  ])
  .directive('trabajador', ['Trabajador', '$q', 'layoutFactory',
    trabajadorListDirectiveFn]);

function trabajadorListDirectiveFn(T, $q, lF) {

  return {
    scope: {
      module: '='
    },
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'trabajadorList',
    restrict: 'E',
    template: require('./templates/trabajador.jade')(),
    link: link
  };

  function link(scope, element, attrs, ctrl) {

    let moduleResource = T;
    let moduleController = ctrl;

  }
}

'use strict';

module.exports = angular
  .module('appTrabajadores', [
    require('./trabajador-list.directive').name,
    require('./trabajador-form.directive').name,
    require('./trabajador-show.directive').name
  ])

'use strict';

import routesAngularModule from './routes.js';
import oclazyloadAngularModule from 'oclazyload';

//ES6
((w, d) => {

  w.addEventListener('load', () => {

    let injector = angular.injector(['apiConsama']);
    let User = injector.get('User');
    let $q = injector.get('$q');
    let loggued = User.isAuthenticated() && User.getCurrentId();

    return angular.isObject(User.getCurrentId()) ? User.findById({
        id: User.getCurrentId(),
        filter: {
          include: [
            'settings',
            {trabajador: ['persona', {'roles': 'acls'}]}
          ]
        }
      }).$promise

        .then((user) => {
          setLS('app-consama-user', JSON.stringify(user));
          return bootstrapApp();
        })

        .catch((e) => {
          if (e.status === 0) {
            setLS('app-consama-apiOffline', 'true');
            return bootstrapApp();
          }else if (e.status === 404) {
            removeLS('app-consama-apiOffline');
            return bootstrapApp();
          }else {
            console.log(e);
            return e;
          }
        }) :

      User.checkinitiaize().$promise
        .then((isInitialize) => {
          if (isInitialize) {
            removeLS('app-consama-apiOffline');
            setLS('app-consama-user', 'INITIALIZE');
          }else {
            removeLS('app-consama-apiOffline');
            removeLS('app-consama-user');
          }
          return bootstrapApp();
        })
        .catch((e) => {
          if (e.status === 0) {
            setLS('app-consama-apiOffline', 'true');
            return bootstrapApp();
          }else console.warn(e);
        });

    function bootstrapApp() {
      return angular.bootstrap(d, [routesAngularModule.name], {strictDi: true});
    }

    function setLS(toSet, v = '') {
      localStorage.setItem(toSet, v);
    }

    function removeLS(toRemove) {
      if (
        localStorage.getItem(toRemove) !== null
      ) localStorage.removeItem(toRemove);
    }

  });

})(window, document);

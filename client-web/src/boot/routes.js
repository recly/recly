'use strict';

import uiRouterAngularModule from 'ui.router';
import layoutAngularModule from '../layout/layout.js';

import * as dashboardRoutes from '../dashboard/dashboard.routes';
import * as layoutRoutes from '../layout/layout.routes';
import * as loginRoutes from '../login/login.routes';

export default angular

  .module('app', [
    'oc.lazyLoad',
    uiRouterAngularModule,
    layoutAngularModule.name
  ])

  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    ($sP, $uRP, $lP) => {

      $lP.html5Mode({
        enabled: true,
        requiBase: true
      });

      dashboardRoutes.rules($uRP);
      dashboardRoutes.routes($sP);

      loginRoutes.routes($sP);

      //-layout routes should be at end
      layoutRoutes.rules($uRP);
      layoutRoutes.routes($sP);

    }]);

'use strict';

module.exports = angular
  .module('appAgregadosShow', [])
  .directive('agregadoShow', agregadosShow);

function agregadosShow(apiConsama, $rootScope, layoutService) {

  function link(scope, elem, attrs, agregadoCtrl) {

    agregadoCtrl.getPhoto = apiConsama.getPhoto('agregados',
      agregadoCtrl.item.photo, 'cover');

    /*
    $rootScope.$on('closeSidenavRight', function(e, isOpen) {
      if (!isOpen) {
        console.log('destroy <ahregado-show/>');
        scope.$destroy();
      }
    });*/

  }

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/agregado-show.jade'),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'agregado',
    link: link
  };

}
agregadosShow.$inject = ['apiConsama', '$rootScope', 'layoutService'];

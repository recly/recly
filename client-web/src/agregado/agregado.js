'use strict';

module.exports = angular
  .module('agregado', [
    require('./agregado-list.directive.js').name,
    require('./agregado-show.directive.js').name,
    require('./agregado-form.directive.js').name
  ]);

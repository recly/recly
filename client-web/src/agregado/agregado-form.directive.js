'use strict';

module.exports = angular
  .module('appAgregadosForm', [
    require('../utils/valid-form.factory').name,
    require('../utils/find-by.factory').name,
    require('../components/input-picture.directive').name
  ])
  .controller('UmDialogController', UmDialogControllerFn)
  .directive('agregadoForm', agregadosFormDirectiveFn);

function UmDialogControllerFn(translates) {

  var mdDialog = this;
  mdDialog.title = translates['ITEM.ADD'];
  mdDialog.save = translates['ACTIONS.SAVE'];
  mdDialog.cancel = translates['ACTIONS.cancel'];

}
UmDialogControllerFn.$inject = ['translates'];

function agregadosFormDirectiveFn($rootScope, apiConsama, yeValidForm, $mdToast,
  $controller, Agregado, $translate, $q, Empresa, UnidadMedida, $mdDialog,
  yeFindBy, $$rAF) {

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/agregado-form.jade'),
    controller: angular.noop,
    controllerAs: 'agregadoForm',
    bindToController: true,
    link: link
  }

  function link(scope, element, attrs, agregadoForm) {

    var layoutController = $controller('LayoutController');

    agregadoForm.form = angular.isDefined(agregadoForm.item) ?
      agregadoForm.item : {};

    agregadoForm.cover =  apiConsama.getPhoto('agregados',
      (angular.isDefined(agregadoForm.item)) ?
      agregadoForm.item.photo : 'default.jpg', 'cover');

    init();

    agregadoForm.save = function(form) {
      console.log(scope, agregadoForm);

      var deferred = $q.defer();

      if (angular.isDefined(agregadoForm.item)) form.$setDirty();

      yeValidForm(form)

        .then(function(result) {

          var typeForm = angular.isDefined(agregadoForm.item) ?
            'UPDATE' : 'NEW';

          Agregado[angular.isDefined(agregadoForm.item) ?
            'prototype$updateAttributes' :
            'create'](angular.isDefined(agregadoForm.item) ?
            {id: agregadoForm.item.id} : {}, agregadoForm.form).$promise
              .then(function(agregado) {
                return layoutController.sidenavClose('right')
              }, function(err) {
                return $q.when({error: true, detail: err});
              })
              .then(function(res) {
                var i18nCode = 'FORM.' + typeForm + '_' +
                  (angular.isDefined(res) ? 'FAIL' : 'SUCCESS');
                return $translate(i18nCode, {itemName: agregadoForm.form.name});
              })
              .then(function(sentence) {
                $mdToast.showSimple(sentence);
                deferred.resolve();
              });

        }, function(err) {
          err.errorOne.message.then(function(errMessage) {
            deferred.reject($mdToast.showSimple(errMessage));
          });
        });

      return deferred.promise;

    };

    agregadoForm.newUm = function() {
      return formatDialogUM();
    };

    agregadoForm.selectAlmacen = function(priceWrapper) {

      if (angular.isUndefined(priceWrapper.almacen)) return;

      for (var i = 0; i < agregadoForm.empresas.length; i++) {
        for (var j = 0; j < agregadoForm.empresas[i].sucursales.length; j++) {
          for (var k = 0; k < agregadoForm.empresas[i].sucursales[j].almacenes
            .length; k++) {
            yeFindBy(agregadoForm.empresas[i].sucursales[j].almacenes, 'id',
              priceWrapper.almacen.id).item.taken = true;
          }
        }
      }

      resolveUms()

        .then(function(ums) {
          agregadoForm.ums = ums;
          priceWrapper.prices = [{}];
          priceWrapper.ums = angular.extend([], ums);
        }, function(fail) {
          $translate('MODEL.PLURAL.UNIDADMEDIDA')

            .then(function(translation) {
              return $translate([fail.codeErr, 'ITEM.ADD'],
                {moduleName: translation});
            })

            .then(function(translations) {

              var toast = $mdToast.simple()
                .highlightAction(true)
                .theme('agregado');

              if (translations['ITEM.NONE']) toast
                .content(translations['ITEM.NONE'])
                .action(translations['ITEM.ADD']);
              else toast
                .content(translations['FAIL.REQUEST'])

              return $mdToast.show(toast)

            })
            .then(function(data) {
              return formatDialogUM();
            });
        });

    };

    agregadoForm.selectPriceUm = function(um) {
      if (angular.isDefined(um)) um.taken = true;
    };

    agregadoForm.allUmsTaken = function(ums) {
      var res = true;
      var i = 0;
      while (angular.isDefined(ums[i]) && res) {
        if (!ums[i].taken) res = false;
        i += 1;
      }
      return res;
    };

    agregadoForm.ui.allAlmacenesTaken = function() {
      var res = true;
      var i = 0;
      while (angular.isDefined(agregadoForm.empresas[i]) && res) {
        for (var j = 0; j < agregadoForm.empresas[i].sucursales.length; j++) {
          for (var k = 0; k < agregadoForm.empresas[i].sucursales[j].almacenes
            .length; k++) {
            if (!agregadoForm.empresas[i].sucursales[j].almacenes[k].taken) {
              res = false
            }
          }
        }
        i += 1;
      }
      return res;
    };

    agregadoForm.ui.chxPrices = function(v) {
      if (agregadoForm.ui.priceWrapper.items.length === 0) agregadoForm.ui
        .priceWrapper.items.push({});
      else if (agregadoForm.ui.priceWrapper.items.length > 0 &&
        agregadoForm.ui.priceWrapper.dirty) {
        $mdToast.showSimple('you have prices initialized');
        v = true;
      }
    };

    agregadoForm.ui.addAlmacen = function(priceWrappers) {
      if (agregadoForm.ui.allAlmacenesTaken) return;
      priceWrappers.push({});
    };

    agregadoForm.ui.addPrice = function(priceWrapper, price) {
      if (angular.isUndefined(price.um) || angular.isUndefined(price.amount)) {
        return;
      }
      if (agregadoForm.allUmsTaken(priceWrapper.ums)) return;
      priceWrapper.prices.push({});
    };

    agregadoForm.ui.removePrice = function(index, price, priceWrapper) {
      if (priceWrapper.prices.length === 1) return;
      if (angular.isDefined(price.um) && price.um.taken) {
        yeFindBy(priceWrapper.ums, 'id', price.um.id).item.taken = false;
      }
      priceWrapper.prices.splice(index, 1);
    };

    $rootScope.$on('newItem', function(e, model, item) {
      if (model === 'unidadMedida') {
        agregadoForm.ums.push(item);
        for (var i = 0; i < agregadoForm.ui.priceWrapper.items.length; i++) (
          function(priceWrapper) {
            priceWrapper.ums.push(item);
          })(agregadoForm.ui.priceWrapper.items[i], i);
      }
    });

    //--
    function init() {

      agregadoForm.ums = [];
      agregadoForm.ui = {
        priceWrapper: {
          show: false,
          dirty: false,
          items: []
        }
      };

      Empresa.find({filter: {include: {sucursales: 'almacenes'}}}).$promise
        .then(function(data) {
          agregadoForm.empresas = data;
        }, function(err) {
          console.error(err);
          //$mdToast.showSimple();
        });
    }

    function resolveUms() {
      var deferred = $q.defer();
      if (agregadoForm.ums.length > 0) deferred.resolve(agregadoForm.ums);
      else UnidadMedida.find().$promise
        .then(function(data) {
          if (data.length === 0) {
            deferred.reject({codeErr: 'ITEM.NONE'});
          }else {
            deferred.resolve(data);
          }
        }, function(err) {
          deferred.reject({codeErr: 'FAIL.REQUEST', err: err});
        });
      return deferred.promise;
    }

    function formatDialogUM(evt) {

      var configDialog = {
        controller: 'UmDialogController',
        bindToController: true,
        controllerAs: 'umDialog',
        resolve: {
          translates: umTranslates
        },
        clickOutsideToClose: true,
        template: '' +
          '<md-dialog mdx-dialog aria-label="{{umDialog.title}}" ' +
          'md-theme="unidadMedida" title="{{umDialog.title}}" ' +
          'ok="{{umDialog.save}}" cancel="{{umDialog.cancel}}">' +
          '<unidad-medida-form in-dialog="true"/>' +
          '</md-dialog>',
        parent: angular.element(document.body)
      };

      return $mdDialog.show(configDialog);

    }

    function umTranslates($q) {
      var deferred = $q.defer();
      $translate('MODEL.UNIDADMEDIDA')
        .then(function(t) {
          return $translate([
            'ITEM.ADD',
            'ACTIONS.CANCEL',
            'ACTIONS.SAVE'
            ], {moduleName: t});
        })
        .then(function(t) {
          deferred.resolve(t);
        });
      return deferred.promise;
    }
    umTranslates.$inject = ['$q'];

  }
}
agregadosFormDirectiveFn.$inject = ['$rootScope', 'apiConsama', 'yeValidForm',
  '$mdToast', '$controller', 'Agregado', '$translate', '$q', 'Empresa',
  'UnidadMedida', '$mdDialog', 'yeFindBy', '$$rAF'];

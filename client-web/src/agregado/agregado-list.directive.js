'use strict';

module.exports = angular
  .module('appAgregadosList', [
    require('../utils/find-by.factory').name
  ])
  .directive('agregado', agregadosListDirectiveFn);

function agregadosListDirectiveFn(layoutService, Agregado, $q, lF) {

  return {
    restrict: 'E',
    scope: {
      module: '='
    },
    template: require('./templates/agregado.jade')(),
    controller: angular.noop,
    controllerAs: 'agregadoList',
    bindToController: true,
    link: link
  }

  function link(scope, element, attrs, agregadoList) {

    var moduleResource = Agregado;
    var moduleController = agregadoList;
    var moduleName = 'agregado';

    moduleController.mAgregado = Agregado;
    console.log(moduleController.module);

    moduleController.showItem = function(evt, item, items) {
      lF.showItem(scope, moduleController, evt, item, items, true, 'name');
    };

    moduleController.deleteItem = function deleteItem(evt, item, items) {
      lF.deleteItem(scope, moduleController, evt, item, items, exeDelete,
        false, 'name');
    }

    moduleController.updateItem = function(evt, item, items, fromSN) {
      lF.updateItem(scope, moduleController, evt, item, items, fromSN = false,
        noMdxLi = false, itemName = 'name', sidenavSize = '');
    };

    moduleController.createItem = function(evt) {
      lF.createItem(scope, moduleController, evt, 'x1');
    };

    //--

    //moduleController.toggleCard = layoutService
    //  .logic.toggleCard.bind(this, moduleController, scope);

    //moduleController.toggleTypeListCard = layoutService
    //  .logic.toggleTypeListCard.bind(this, moduleController, scope);

    function exeDelete(item) {
      return moduleResource.destroyById({id: item.id}).$promise;
    }

  }

}
agregadosListDirectiveFn.$inject = ['layoutService', 'Agregado', '$q',
  'layoutFactory'];

'use strict';

module.exports = angular
  .module('unidadMedidas', [
    require('./unidad-medida-list.directive.js').name,
    require('./unidad-medida-show.directive.js').name
  ])
  .directive('unidadMedidaForm', unidadMedidaFormDirectiveFn);

function unidadMedidaFormDirectiveFn(UnidadMedida, $rootScope, $mdDialog,
  layoutService) {

  return {
    scope: {
      item: '='
    },
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'umForm',
    restrict: 'E',
    template: require('./templates/um-form.jade')(),
    link: link
  }

  function link(scope, element, attrs, umForm) {

    umForm.inDialog = angular.isDefined(attrs.inDialog) ? true : false;
    umForm.save = save;
    umForm.cancel = cancel;

    //--
    function cancel(form) {
      $mdDialog.cancel();
    }

    function save(form) {

      layoutService.logic.formUpsert(UnidadMedida, form, umForm)
        .then(function() {
          if (umForm.inDialog) $mdDialog.hide();
        }, function(err) {
          console.error(err);
        })

    }

  }

}
unidadMedidaFormDirectiveFn.$inject = ['UnidadMedida', '$rootScope',
  '$mdDialog', 'layoutService'];

'use strict';

module.exports = angular
  .module('umList', [
    require('../utils/find-by.factory').name
  ])
  .directive('unidadmedida', unidadMedidaListDirectiveFn);

function unidadMedidaListDirectiveFn(UnidadMedida, $q, layoutService) {

  return {
    scope: {
      module: '='
    },
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'umList',
    restrict: 'E',
    template: require('./templates/um.jade')(),
    link: link
  }

  function link(scope, element, attrs, umList) {

    var moduleResource = UnidadMedida;
    var moduleController = umList;
    var moduleName = 'unidadMedida';

    moduleController.data = function() {
      var deferred = $q.defer();
      moduleResource.find().$promise
        .then(function(data) {
          deferred.resolve({
            items: data,
            pattern: layoutService.logic.listPattern(moduleName)
          });
        }, function(err) {
          console.error(err);
          deferred.reject(err);
        });
      return deferred.promise;
    }

    moduleController.showItem = function(evt, item, items) {
      layoutService.logic.showItem(scope, moduleController, evt, item, items);
    };

    moduleController.deleteItem = function deleteItem(evt, item, items) {
      layoutService.logic.deleteItem(scope, moduleController, evt, item, items,
        exeDelete);
    }

    moduleController.updateItem = function(evt, item, items, fromSN) {
      layoutService.logic.updateItem(scope, moduleController, evt, item, items,
        fromSN);
    };

    moduleController.createItem = function(evt) {
      layoutService.logic.createItem(scope, moduleController, evt);
    };

    //--
    moduleController.toggleCard = layoutService
      .logic.toggleCard.bind(this, moduleController, scope);

    moduleController.toggleTypeListCard = layoutService
      .logic.toggleTypeListCard.bind(this, moduleController, scope);

    function exeDelete(item) {
      return moduleResource.destroyById({id: item.id}).$promise;
    }

  }
}
unidadMedidaListDirectiveFn.$inject = ['UnidadMedida', '$q', 'layoutService'];

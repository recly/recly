'use strict';

module.exports = angular
  .module('umShow', [])
  .directive('unidadmedidaShow', agregadosShow);

function agregadosShow(apiConsama, $rootScope, layoutService) {

  function link(scope, elem, attrs, agregadoCtrl) {
  }

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/um-show.jade'),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'umShow',
    link: link
  };

}
agregadosShow.$inject = ['apiConsama', '$rootScope', 'layoutService'];

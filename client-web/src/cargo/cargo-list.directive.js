'use strict';

export default angular
  .module('cargoList', [])
  .directive('cargoList', cargoListDirectiveFn);

function cargoListDirectiveFn() {

  return {
    scope: {
      area: '='
    },
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'cargoList',
    template: 'area: {{cargoList.area.id}}',
    link: link
  }

  function link(scope, element, attrs, cargoList) {
    console.log(cargoList.area);
  }

}
cargoListDirectiveFn.$inject = [];

'use strict';

import cargoListModule from './cargo-list.directive.js';
import cargoFormModule from './cargo-form.directive.js';

export default angular
  .module('cargo', [
    cargoListModule.name,
    cargoFormModule.name
  ]);

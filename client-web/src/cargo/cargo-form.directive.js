'use strict';

export default angular
  .module('cargoForm', [])
  .directive('cargoForm', ['layoutFactory', 'Cargo', '$log', (lF, C, $l) => {

    return {
      scope: {
        item: '=',
        options: '='
      },
      restrict: 'E',
      template: require('./templates/cargo-form.jade')(),
      controller: angular.noop,
      controllerAs: 'mForm',
      bindToController: true,
      compile(tE) {

        tE.find('form').eq(0).attr('ng-class',
          'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');

        return (scope, element, attrs, ctrlForm) => {

          lF.getFormInitial(scope, element, ctrlForm, ['area'], 'cargo')

            .then((formData) => {
              ctrlForm.form = formData;
              ctrlForm.save = (form) => {
                return lF.formUpsert(C, form, ctrlForm);
              };
            })

            .catch((e) => $l.warn(e));

        };
      }
    };

  }]);

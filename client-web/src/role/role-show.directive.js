'use strict';

module.exports = angular
  .module('roleShow', [])
  .directive('roleShow', roleShowDirectiveFn)
  .directive('aclShow', roleShowDirectiveFn);

function roleShowDirectiveFn() {

  return {
    scope: {
      item: '='
    },
    restrict: 'E',
    template: require('./templates/role-show.jade')(),
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'roleShow',
    link: link
  };

  function link(scope, elem, attrs, agregadoCtrl) {

  }

}
roleShowDirectiveFn.$inject = [];

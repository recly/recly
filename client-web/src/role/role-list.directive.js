'use strict';

module.exports = angular
  .module('roleListDirective', [
    require('../api/api.js').name,
    require('../utils/find-by.factory.js').name,
    //require('../components/list-item-multiple-actions.directive.js').name,
    require('../components/module-toolbar.directive.js').name,
    'ngMaterial'
  ])
  .directive('role', ['$$rAF', 'Acl', 'layoutFactory', 'yeFindBy', '$log',
    '$q', 'Role', 'Settings', '$mdToast', '$translate', '$timeout',
    rolesListDirectiveFn]);
/*
  .directive('acl', ['$$rAF', 'Acl', 'layoutFactory', 'yeFindBy',
    '$q', 'Role', 'Settings', '$mdToast', '$translate', '$timeout',
    rolesListDirectiveFn]);*/

function rolesListDirectiveFn($$rAF, Acl, lF, yeFindBy, $l,
  $q, Role, Settings, $mdToast, $translate, $timeout) {

  return {
    scope: {
      module: '=',
      modules: '='
    },
    restrict: 'E',
    template: require('./templates/roles-list.jade')(),
    controller: angular.noop,
    controllerAs: 'roleList',
    bindToController: true,
    link: link
  };

  function link(scope, element, attrs, ctrl) {

    loadRolesWAcls()
      .then((roles) => {

        ctrl.roles = roles;

        ctrl.moduleAcl = getModuleAcl();
        ctrl.moduleRole = getModuleRoles();
        ctrl.module = ctrl.moduleRole;

        ctrl.mRole = Role;
        ctrl.mAcl = Acl;

        ctrl.ready = true;

        ctrl.createItem = (evt) => lF.createItem(scope, ctrl, evt);

        ctrl.changeAcl = (selectRole, selectModule, operation, vCh) => {

          var deferred = $q.defer();

          var auxAcl = angular.isDefined(selectModule.crud[operation + 'Acl']) ?
            true : false;

          var upsertAcl = auxAcl ?
            angular.extend({}, selectModule.crud[operation + 'Acl']) :
            formateAclToSend(selectModule.name, selectRole.name,
              selectRole.id, operation);

          if (vCh) {

            $q.all([
              extraRequestAcl(selectRole, selectModule, operation),
              Acl.create(upsertAcl)
            ])
              .then(function(data) {

                return $translate('CRUD.' + operation.toUpperCase())
                  .then(function(operationName) {
                    return $translate('ROLE.ACL_ADD',
                      {operation: operationName, moduleName: selectModule.name,
                        roleNamePlural: selectRole.name});
                  });

              }, function(err) {
                console.error(err);
                deferred.reject();
              })

              .then(function(translation) {
                $mdToast.showSimple(translation);
                deferred.resolve();
              });

          } else {
            if (auxAcl) {

              if (operation === 'r' && (selectModule.crud.d ||
                selectModule.crud.d)) {
                selectModule.crud[operation] = true;
                return $mdToast.showSimple('cannot remove read ' +
                  'while u or d is enabled');
              }

              $q.all([
                Acl.deleteById({id: selectModule.crud[operation + 'Acl'].id})
                  .$promise
              ])
                .then(function(data) {
                  return $translate('ROLE.ACL_REMOVE',
                    {operation: operation, moduleName: selectModule.name,
                      roleNamePlural: selectRole.name});
                }, function(err) {
                  vCh = true;
                  console.error(err);
                })

                .then(function(translation) {
                  $mdToast.showSimple(translation);
                  deferred.resolve();
                });

            }else {
              console.log('not exist acl saved for operation ' + operation);
              deferred.resolve();
            }
          }
          return deferred.promise;
        };

      })
      .catch((e) => $l.warn(e));

    //lF.toggleMasonry(scope, 320, roleList, 'auxShowContent');
    //--
    function getModuleAcl() {
      return ctrl.module.name === 'acl' ? ctrl.module : getAclModuleXUser();
    }

    function getModuleRoles() {
      return ctrl.module.name === 'role' ? ctrl.module : getRoleModuleXUser();
    }

    function getAclModuleXUser() {
      var tmpModule = yeFindBy(ctrl.modules, 'name', 'acl');
      return tmpModule.index !== -1 ? tmpModule.item : {
        name: 'acl',
        pluralName: 'acls',
        crud: {c: false, r: false, u: false, d: false}
      };
    }

    function getRoleModuleXUser() {
      var tmpModule = yeFindBy(ctrl.modules, 'name', 'role');
      return tmpModule.index !== -1 ? tmpModule.item : {
        name: 'role',
        pluralName: 'roles',
        icon: lF.getIconModule('role')
      };
    }

    function loadRolesWAcls() {

      let aclModule = getModuleAcl();

      return $q.all([
        Role.find().$promise,
        aclModule.crud.r ? Settings.modules().$promise : $q.when([]),
        aclModule.crud.r ? Acl.find().$promise : $q.when([])
      ])

        .then((results) => {

          if (
            lF.data.initialize
          ) results[0].push(lF.data.dataUser.trabajador.roles[0]);

          let roles = lF.formateModulesByRoles(results[0],
            results[1], results[2]);

          return roles;
        });

    }

    function formateAclToSend(moduleName, roleName, roleId, operation) {
      return {
        model: moduleName,
        permission: 'ALLOW',
        principalType: 'ROLE',
        principalId: roleName,
        roleId: roleId,
        accessType: operation === 'r' ? 'READ' : 'WRITE',
        property: operation === 'u' ? 'prototype_updateAttributes' :
          operation === 'r' ? 'find' :
          operation === 'd' ? 'deleteById' : 'create'
      };
    }

    function extraRequestAcl(selectRole, selectModule, operation) {
      return ((operation === 'u' || operation === 'd') &&
        !selectModule.crud.r) ?
        Acl.create(formateAclToSend(selectModule.name, selectRole.name,
          selectRole.id, 'r')).$promise : $q.when();
    }

  }
}

'use strict';

export default angular
  .module('roleForm', [])
  .directive('roleForm', ['Role', 'layoutFactory', '$log',
    rolesFormDirectiveFn]);

function rolesFormDirectiveFn(R, lF, $l) {

  return {
    scope: {
      item: '=',
      options: '='
    },
    restrict: 'E',
    template: require('./templates/roles-form.jade')(),
    controller: angular.noop,
    controllerAs: 'mForm',
    bindToController: true,
    compile(tE) {
      tE.find('form').eq(0).attr('ng-class',
        'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');
      return link;
    }
  };

  function link(scope, element, attrs, ctrl) {

    return lF.getFormInitial(scope, element, ctrl, [], 'role')
      .then((formData) => {
        ctrl.form = formData;
        ctrl.save = (form) => {
          ctrl.form.status = !!ctrl.form.status;
          console.log(ctrl.form);
          return lF.formUpsert(R, form, ctrl);
        };
      })
      .catch((e) => $l.error(e));

  }

}

'use strict';

import roleForm from './role-form.directive.js';

export default angular
  .module('role', [
    require('./role-show.directive.js').name,
    require('./role-list.directive.js').name,
    roleForm.name
  ]);

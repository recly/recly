'use strict';

module.exports = angular
  .module('logs', [
    require('./log-list.directive.js').name
  ]);

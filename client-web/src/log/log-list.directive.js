'use strict';

module.exports = angular
  .module('cardLog', [])
  .directive('cardLog', cardModuleDirectiveFn);

function cardModuleDirectiveFn($rootScope, $translate, $mdDialog) {

  return {
    scope: {
      //module: '=',
      modules: '='
    },
    controller: angular.noop,
    controllerAs: 'cardLog',
    template: require('./templates/card.jade')(),
    restrict: 'E',
    link: link
  };

  function link(scope, element, attrs, ctrl) {

    ctrl.module = {
      name: 'log',
      icon: 'book-open'
    }

    ctrl.logs = [];

    ctrl.check = function(logS) {
      /*
      $mdDialog.show({

      })
        .then(function() {

        });
      */
    }

    var log = {};
    $rootScope.$on('newItem', function(e, model, item) {
      log.detail = angular.extend({}, item);
      log.description = 'new item of ' + model;
      ctrl.logs.push(angular.extend({}, log));
    });

    $rootScope.$on('deleteItem', function(e, model, id) {
      log.detail = angular.extend({}, item);
      log.description = 'item removed from ' + model;
      ctrl.logs.push(angular.extend({}, log));
    });

    $rootScope.$on('updateItem', function(e, model, item) {
      log.detail = angular.extend({}, item);
      log.description = 'item updated from ' + model;
      ctrl.logs.push(angular.extend({}, log));
    });

  }

}
cardModuleDirectiveFn.$inject = ['$rootScope', '$translate', '$mdDialog'];

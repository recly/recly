'use strict';

module.exports = angular
  .module('sucursal', [
    require('./sucursal-list.directive.js').name,
    require('./sucursal-show.directive.js').name,
    require('./sucursal-form.directive.js').name
  ]);

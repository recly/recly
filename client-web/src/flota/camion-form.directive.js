'use strict';

export default angular
  .module('camionForm', [])
  .directive('camionForm', ['layoutFactory', 'Camion', '$log', (lF, C, $l) => {

    return {
      scope: {
        item: '=',
        options: '='
      },
      restrict: 'E',
      template: require('./templates/camion-form.jade')(),
      controller: angular.noop,
      controllerAs: 'mForm',
      bindToController: true,
      compile(tE) {

        tE.find('form').eq(0).attr('ng-class',
          'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');

        return (scope, element, attrs, ctrlForm) => {

          lF.getFormInitial(scope, element, ctrlForm, ['flota'],
            'camion', {photo: 'photo'})

            .then((formData) => ctrlForm.form = formData)

            .catch((e) => $l.warn(e));

          ctrlForm.save = (form) => lF.formUpsert(C, form, ctrlForm);

        };
      }
    };

  }]);

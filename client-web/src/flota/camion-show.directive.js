'use strict';

export default angular
  .module('camionShow', [])
  .directive('camionShow', ['layoutFactory', 'Camion', '$log',
    'gmapUtilsFactory', camionShowFn]);

function camionShowFn(lF, C, $l, gmapUF) {

  return {
    scope: {
      item: '=',
      options: '='
    },
    restrict: 'E',
    template: require('./templates/camion-show.jade')(),
    controller: angular.noop,
    controllerAs: 'mShow',
    bindToController: true,
    link(scope, element, attrs, ctrl) {
      ctrl.getPhoto = lF.getConsamaPhoto('camiones', ctrl.item.photo);
      ctrl.getUbication = (evt) => {
        gmapUF.launchMap(evt, ctrl.item.ubication,
          'flota', true, ctrl.item.licensePlate);
      };
    }
  };

}

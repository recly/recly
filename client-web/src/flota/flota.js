'use strict';

import flotaForm from './flota-form.directive.js';
import flotaShow from './flota-show.directive.js';
import camionForm from './camion-form.directive.js';
import camionShow from './camion-show.directive.js';

export default angular
  .module('flota', [
    flotaForm.name,
    flotaShow.name,
    camionForm.name,
    camionShow.name
  ]);

'use strict';

export default angular
  .module('flotaShow', [])
  .directive('flotaShow', ['$q', 'layoutFactory', 'Flota', 'Camion', 'yeFindBy',
    foltaShowFn]);

function foltaShowFn($q, lF, F, C, yFB) {
  return {
    scope: {
      data: '=',
      moduleCamion: '=',
      modelCamion: '='
    },
    restrict: 'E',
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'flotaShow',
    template: require('./templates/flota-show.jade')(),
    link(scope, elem, attrs, ctrl) {

      init()
        .then((items) => {

          ctrl.items = items;

          ctrl.getType = (typeName, flota) => {
            let itemOriginal = yFB(ctrl.items.original, 'id', flota.id).item;
            if (
              angular.isDefined(itemOriginal) &&
              angular.isDefined(itemOriginal.camiones) &&
              angular.isArray(itemOriginal.camiones)
            ) {
              flota.items = itemOriginal.camiones;
              flota.model = ctrl.modelCamion;
              flota.module = ctrl.moduleCamion;
              flota.showItems = true;
            }else if (typeName === 'camion') ctrl.modelCamion.find().$promise
              .then((rows) => {
                flota.items = rows;
                flota.model = ctrl.modelCamion;
                flota.module = ctrl.moduleCamion;
                flota.showItems = true;
              });
          };

          ctrl.createFlotaType = (evt, flotaType, flota) => {
            if (flotaType === 'camion') {
              let moduleController = {
                module: angular.extend({}, ctrl.moduleCamion),
                options: {flota: flota}
              };
              lF.createItem(scope, moduleController, evt, 'x3');
            }
          };

        });

      function init() {
        return $q((resolve, reject) => {
          let items = lF.listFormate(
            ctrl.data, lF.listPattern('flota')
          );
          resolve(items);

        });
      }

    }
  };
}

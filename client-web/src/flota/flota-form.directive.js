'use strict';

export default angular
  .module('flotaForm', [])
  .directive('flotaForm', ['Flota', 'layoutFactory', '$mdDialog', '$translate',
    flotaFormFn]);

function flotaFormFn(F, lF, $mdD, $t) {
  return {
    scope: {
      item: '=',
      options: '=' //type => 0: camion, 1: maquinaria, 2: auto, 3: moto, 4: otro vehicle
    },
    restrict: 'E',
    template: require('./templates/flota-form.jade')(),
    controller: angular.noop,
    controllerAs: 'mForm',
    bindToController: true,
    compile(tE) {

      tE.find('form').eq(0).attr('ng-class',
        'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');

      return (scope, element, attrs, mForm) => {

        lF.getFormInitial(scope, element, mForm, ['empresa'], 'flota')

          .then((formData) => {

            mForm.form = formData;
            mForm.formAux = {
              type: {
                'camion': false,
                'maquinaria': false,
                'carro': false,
                'moto': false,
                'otraMovilidad': false
              }
            };

            angular.forEach(mForm.options.empresa.flotas, (flota) => {
              mForm.formAux.type[flota.type] = true;
            });

            mForm.changeType = (idType, chx) => {

              return F.find({filter: {where: {type: idType}}}).$promise
                .then((rows) => {
                  if (
                    rows.length === 0 && chx
                  ) return F.create({
                    type: idType, empresaId: mForm.options.empresa.id});
                  else if (
                    rows.length === 1 && !chx
                  ) return F.deleteById({id: rows[0].id});
                  else throw new Error(
                    `there are more than one ${idType} registered`
                  );
                });

            };

          })

          .catch((e) => {
            console.warn(e);
          });

      };
    }
  };

}

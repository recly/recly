'use strict';

module.exports = angular
  .module('remuneracion', [
    require('./remuneracion-list.directive.js').name
  ]);

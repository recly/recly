'use strict';

module.exports = angular
  .module('remuneracionList', [
    require('../utils/find-by.factory').name
  ])
  .directive('remuneracion', remuneracionListDirectiveFn);

function remuneracionListDirectiveFn(layoutService, Remuneracion, $q) {

  return {
    restrict: 'E',
    scope: {
      module: '='
    },
    template: require('./templates/remuneracion.jade')(),
    controller: angular.noop,
    controllerAs: 'remuneracionList',
    bindToController: true,
    link: link
  }

  function link(scope, element, attrs, remuneracionList) {

    var moduleResource = Remuneracion;
    var moduleController = remuneracionList;
    var moduleName = 'remuneracion';

    moduleController.data = function() {
      var deferred = $q.defer();
      moduleResource.find().$promise
        .then(function(data) {
          deferred.resolve({
            items: data,
            pattern: layoutService.logic.listPattern(moduleController
              .module.name)
          });
        }, function(err) {
          console.error(err);
          deferred.reject(err);
        });
      return deferred.promise;
    }

    moduleController.showItem = function(evt, item, items) {
      layoutService.logic.showItem(scope, moduleController, evt, item, items);
    };

    moduleController.deleteItem = function deleteItem(evt, item, items) {
      layoutService.logic.deleteItem(scope, moduleController, evt, item, items,
        exeDelete);
    }

    moduleController.updateItem = function(evt, item, items, fromSN) {
      layoutService.logic.updateItem(scope, moduleController, evt, item, items,
        fromSN);
    };

    moduleController.createItem = function(evt) {
      layoutService.logic.createItem(scope, moduleController, evt);
    };

    //--

    moduleController.toggleCard = layoutService
      .logic.toggleCard.bind(this, moduleController, scope);

    moduleController.toggleTypeListCard = layoutService
      .logic.toggleTypeListCard.bind(this, moduleController, scope);

    function exeDelete(item) {
      return moduleResource.destroyById({id: item.id}).$promise;
    }

  }

}
remuneracionListDirectiveFn.$inject = ['layoutService', 'Remuneracion', '$q'];

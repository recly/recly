'use strict';

module.exports = angular
  .module('componentsDatePicker', [
    require('../utils/platform-environment.factory').name,
    require('../utils/verge.factory').name,
    'ngMaterial'
  ])

  .controller('DatePickerController', ['$mdDialog', '$mdToast', '$translate',
    'dateFilter', '$q', '$window', '$$rAF', '$rootScope', 'yeVerge',
    DatePickerController])

  .directive('datePicker', ['yePlatform', '$mdDialog', 'layoutFactory',
    datePickerDirectiveFn]);

function DatePickerController($mdD, $mdToast, $tr, dateFilter, $q, $w, $$rAF,
  $rS, yeVerge) {

  let datePicker = this;
  let datePickerUI = datePicker.state;

  datePicker.ui = datePickerUI;

  datePickerUI.today = new Date(Date.now());

  datePickerUI.selectDate = angular.isDefined(datePicker.modelValue) ?
    new Date(datePicker.modelValue) : undefined;

  datePickerUI.minDate = formateMinMax(datePicker.minValue);

  datePickerUI.maxDate = formateMinMax(datePicker.maxValue);

  datePickerUI.calendar = angular.isDefined(datePickerUI.selectDate) ?

    new Date(datePickerUI.selectDate.getTime()) :
    datePickerUI.maxDate ?
    new Date(datePickerUI.maxDate.getTime()) :
    new Date(datePickerUI.today.getTime());

  $w.addEventListener('resize',
    $$rAF.throttle(initSize.bind(null, datePickerUI.canvas)));

  let w1 = $rS.$watch(() => datePickerUI.canvas, (viewT) => initSize(viewT));

  function formateMinMax(datePre) {
    if (angular.isUndefined(datePre) ||
      !/[0-9]{4}(-[0-9]{2}){2}/.test(datePre)) return false;
    var d = datePre.split('-');
    return new Date(d[0], d[1], d[2]);
  }

  datePicker.save = (value) => {
    w1();
    $mdD.hide(datePickerUI.selectDate);
  };

  datePicker.cancel = () => {
    w1();
    $mdD.cancel();
  };

  datePicker.auxMoreYear = (indicator) => {
    if (indicator > 0) {
      var yearLast = datePicker.auxYears[datePicker.auxYears.length - 1];
      for (var i = yearLast + 1; i <= yearLast + indicator; i++) {
        datePicker.auxYears.push(i);
      }
    }else {
      var yearFirst = datePicker.auxYears[0];
      for (var i = yearFirst - 1; i >= yearFirst + indicator; i--) {
        datePicker.auxYears.unshift(i);
      }
    }
  };

  datePicker.getWeekDays = () => {
    var weekDays = [];
    for (var i = 4; i < 11; i++) {
      var aux = new Date(2015, 0, i);
      weekDays.push(aux.getTime());
    }
    return weekDays;
  };

  datePicker.changeViewtype = (type) => {
    if (type === 'month') {

      datePicker.auxMonths = [];
      var i = 0;
      while (datePicker.auxMonths.length < 12) {
        var aux = new Date(datePickerUI.calendar.getFullYear(), i);
        datePicker.auxMonths.push({code: i, display: aux.getTime()});
        i += 1;
      }
      datePickerUI.canvas = 'month';
    }else if (type === 'year') {

      datePicker.auxYears = [];

      var minYear = datePickerUI.minDate ?
        datePickerUI.minDate.getFullYear() : false;

      var maxYear = datePickerUI.maxDate ?
        datePickerUI.maxDate.getFullYear() : false;

      if (minYear && maxYear) {
        for (var i = minYear; i <= maxYear; i++) {
          datePicker.auxYears.push(i);
        }
      }else if (minYear) {
        for (var i = minYear; i <= datePickerUI.calendar.getFullYear() + 6;
          i++) {
          datePicker.auxYears.push(i);
        }
      }else if (maxYear) {
        for (var i = datePickerUI.calendar.getFullYear() - 6; i <= maxYear;
          i++) {
          datePicker.auxYears.push(i);
        }
      }else {
        for (var i = datePickerUI.calendar.getFullYear() - 3;
          i <= datePickerUI.calendar.getFullYear() + 3; i++) {
          datePicker.auxYears.push(i);
        }
      }
      datePickerUI.canvas = 'year';
    }else if (type === 'calendar') {
      datePickerUI.canvas = 'calendar';
    }
  };

  datePicker.changeYear = (year, replace) => {

    var result = datePicker.validCalendar('year', year);

    if (result.status) {
      datePickerUI.calendar.setFullYear(year);
      datePickerUI.canvas = 'calendar';
    }else tToast(result.errorType);

  };

  datePicker.changeMonth = (month, replace) => {

    let finalMonth = angular.isDefined(replace) ?
      month : datePickerUI.calendar.getMonth() + month;

    let result = datePicker.validCalendar('month', finalMonth);

    if (result.status) {
      if (
        angular.isUndefined(datePickerUI.selectDate)
      ) datePickerUI.calendar.setDate(1);
      datePickerUI.calendar.setMonth(finalMonth);
      datePickerUI.canvas = 'calendar';
    }else tToast(result.errorType);
  };

  datePicker.validCalendar = (type, indicator) => {

    let calendar = datePickerUI.calendar;

    let auxCalendar = new Date(calendar.getFullYear(), calendar.getMonth(), 1);

    let typeFinal = type === 'year' ? 'FullYear' : 'Month';

    auxCalendar['set' + typeFinal](indicator);

    let band = (
      datePickerUI.maxDate &&
      auxCalendar.getTime() > datePickerUI.maxDate.getTime() &&
      auxCalendar.getMonth() !== datePickerUI.maxDate.getMonth()
    ) ? {status: false, errorType: 'max'} :
    (
      datePickerUI.minDate &&
      auxCalendar.getTime() < datePickerUI.minDate.getTime() &&
      auxCalendar.getMonth() !== datePickerUI.minDate.getMonth()
    ) ? {status: false, errorType: 'min'} : {status: true};

    return band;
  };

  datePicker.isVisible = (day) => {
    var calendar = datePickerUI.calendar;
    var tmpDatePre = new Date(calendar.getFullYear(), calendar.getMonth(), day);
    return tmpDatePre.getMonth() === calendar.getMonth();
  };

  datePicker.isDisabled = (day) => {
    var calendar = datePickerUI.calendar;

    var tmpDatePre = new Date(calendar.getFullYear(), calendar.getMonth(), day);
    var disabled = false;

    //console.log(tmpDatePre.getMonth(), datePickerUI.maxDate.getMonth());
    if (datePickerUI.maxDate &&
      tmpDatePre.getTime() > datePickerUI.maxDate.getTime()) disabled = true;

    if (datePickerUI.minDate &&
      tmpDatePre.getTime() < datePickerUI.minDate.getTime()) disabled = true;

    return disabled;

  };

  datePicker.checkMarginLeft = (day) => {
    if (day !== 1) return {};
    var calendar = datePickerUI.calendar;
    var factor = 14.285;
    var valML = (new Date(calendar.getFullYear(), calendar.getMonth())
      .getDay() *
      factor) + '%';
    return {'margin-left': valML};
  };

  datePicker.checkToday = (day) => {
    var calendar = datePickerUI.calendar;
    return datePickerUI.today.getDate() === day &&
      calendar.getMonth() === datePickerUI.today.getMonth() &&
      calendar.getFullYear() === datePickerUI.today.getFullYear();
  };

  datePicker.checkSelected = (day) => {
    if (angular.isUndefined(datePickerUI.selectDate)) return false;
    var selectDate = datePickerUI.selectDate;
    var calendar = datePickerUI.calendar;

    return selectDate.getDate() === day &&
      selectDate.getMonth() === calendar.getMonth() &&
      selectDate.getFullYear() === calendar.getFullYear();

  };

  datePicker.checkMonth = (month) => {
    if (angular.isUndefined(datePickerUI.selectDate)) return false;
    return datePickerUI.selectDate.getMonth() === month;
  };

  datePicker.checkYear = (year) => {
    if (angular.isUndefined(datePickerUI.selectDate)) return false;
    return datePickerUI.selectDate.getFullYear() === year;
  };

  datePicker.select = (day) => {
    datePickerUI.selectDate = new Date(datePickerUI.calendar.getFullYear(),
      datePickerUI.calendar.getMonth(), day);
  };

  datePicker.setNow = () => {
    datePickerUI.calendar = new Date(Date.now());
    datePickerUI.selectDate = new Date(Date.now());
    if (datePickerUI.canvas !== 'calendar') datePickerUI.canvas = 'calendar';
  };

  //--
  function tToast(errorType) {

    return $tr('CALENDAR.' + errorType.toUpperCase() + '_DATE', {
        date: dateFilter(datePickerUI[errorType + 'Date'])
      })
      .then((t) => $mdToast.showSimple(t));

  }

  function initSize(viewType) {
    //let hDialog = 0.8 * yeVerge.viewportH();
    //console.log(hDialog);
    datePickerUI.height = {
      height: (yeVerge.viewportH() * 0.4) + 'px'
    };

  }

}

function datePickerDirectiveFn(yePlatform, $mdD, lF) {

  return {
    scope: {
      modelValue: '=ngModel'
    },
    link: link
  };

  function link(s, elem, attrs) {

    elem.on(yePlatform.isTouchScreen ? 'touchstart' : 'click', openDateDialog);

    function openDateDialog(evt) {

      let minValue = attrs.min ? lF.getDate(attrs.min) : false;
      let maxValue = attrs.max ? lF.getDate(attrs.max) : false;

      return $mdD.show({
        targetEvent: evt,
        template: require('./templates/dialog-date')(),
        controller: 'DatePickerController',
        controllerAs: 'datePicker',
        bindToController: true,
        clickOutsideToClose: true,
        locals: {
          state: {canvas: 'calendar'},
          modelValue: s.modelValue,
          theme: attrs.theme,
          minValue: minValue,
          maxValue: maxValue
        }
      }).then((value) => s.modelValue = new Date(value));
    }

  }

}

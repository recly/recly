'use strict';

export default angular
  .module('yeForm', [])
  .directive('yeInput', [() => {
    return {
      restrict: 'E',
      transclude: true,
      compile(tE, tA) {

        tA.mForm = angular.isDefined(tA.mForm) ? tA.mForm : 'mForm';
        tA.label = angular.isDefined(tA.label) ? tA.label : '';
        tA.field = angular.isDefined(tA.field) ? tA.field : 'field';
        tA.name = angular.isDefined(tA.name) ? tA.name :
          tA.field.lastIndexOf('.') !== -1 ?
          tA.field.slice(tA.field.lastIndexOf('.') + 1) : tA.field;
        tA.theme = angular.isDefined(tA.theme) && tA.theme !== '' &&
          tA.theme !== 'theme' ? tA.theme : 'default';

        let formOrformAux = angular.isDefined(tA.aux) ? 'formAux' : 'form';

        let maxlengthAttr = angular.isDefined(tA.preMdMaxlength) ?
          'md-maxlength = \'' +
            (parseInt(tA.preMdMaxlength) ? tA.preMdMaxlength : 140) + '\'' :
          '';

        let mdInputContainer = angular.element(
          `<md-input-container
            ${angular.isDefined(tA.checkbox) ? 'checkbox' : ''}
            ${angular.isDefined(tA.switch) ? 'switch' : ''}
            ${tA.radio && tA.icon ? 'radio-icon' : ''}
          >
          </md-input-container>`
        );

        let label = angular.isDefined(tA.checkbox) ||
          angular.isDefined(tA.switch)  ||
          angular.isDefined(tA.radio) ?
          tA.label === '' ?
            '' :
            `{{'${tA.label}' |uppercase | translate | capitalize}}` :
          angular.element(
            `<label>
              {{'${tA.label}' |uppercase | translate | capitalize}}
            </label>`
          );

        let inputOTextarea = angular.isDefined(tA.radio) ? angular.element(
          `
            <md-radio-button
              ng-value="${tA.value}"
              name="${tA.name}"
              aria-label="radio for ${label}"
            > ${label}
            </md-radio-button>
          `
          ) : angular.isDefined(tA.checkbox) ? angular.element(
          `<md-checkbox
            ng-model="${tA.mForm}.${formOrformAux}.${tA.field}"
            name="${tA.name}"
            aria-label='chx for ${tA.field}'
          >
            ${label}
          </md-checkbox>`
        ) : angular.isDefined(tA.switch) ? angular.element(
          `<md-switch
            ng-model="${tA.mForm}.${formOrformAux}.${tA.field}"
            name="${tA.name}"
            aria-label='switch for ${tA.field}'>
            ${label}
          </md-switch>`
          ) : angular.isDefined(tA.geoposition) ? angular.element(
            `<input
              gmap-geolocation
              theme = '${tA.theme}'
              ng-model-geo="${tA.mForm}.form.${tA.field}"
              ng-model="${tA.mForm}.${formOrformAux}Aux.${tA.field}"
              name="${tA.name}"
              ${angular.isDefined(tA.required) ? 'required' : ''}
            />`
          ) : angular.isDefined(tA.textarea) ? angular.element(
          `<textarea
            ng-model="${tA.mForm}.${formOrformAux}.${tA.field}"
            name="${tA.name}"
            columns="1"
            ${maxlengthAttr}
            ng-model-options="{ updateOn: 'default', debounce: {'blur': 200} }"
          />`
        ) : angular.isDefined(tA.type) && tA.type === 'date' ? angular.element(
          `<input
            type='date'
            date-picker=''
            theme = '${tA.theme}'
            ${angular.isDefined(tA.min) && tA.min !== '' ?
              'min=\'' + tA.min + '\'' : ''}
            ${angular.isDefined(tA.max) && tA.max !== '' ?
              'max=\'' + tA.max + '\'' : ''}
            ng-model="${tA.mForm}.${formOrformAux}.${tA.field}"
            name="${tA.name}"
            ${angular.isDefined(tA.required) ? 'required' : ''}
          />`
          ) : angular.element(
          `<input
            type="${angular.isDefined(tA.type) ? tA.type : 'text'}"
            ng-model="${tA.mForm}.${formOrformAux}.${tA.field}"
            name="${tA.name}"
            ${angular.isDefined(tA.required) ? 'required' : ''}
            ${angular.isDefined(tA.maxlength) ?
              'maxlength="' + tA.maxlength + '"'  : ''}
            ${maxlengthAttr}
            ng-model-options="{ updateOn: 'default', debounce: {'blur': 200} }"
          />`
        );

        let messages = angular.element(
          `<div ng-messages="form.${tA.name}.$error"
             ng-show="form.${tA.name}.$dirty && form.${tA.name}.$invalid">
            </div>`
        );

        if (angular.isDefined(tA.required)) messages.append(angular.element(
          `<div ng-message="required">
            {{'FORM.FIELD_ERROR.REQUIRED' | translate | capitalize}}
          </div>`
        ));

        if (
          angular.isDefined(tA.type) &&
          tA.type === 'number'
        ) messages.append(angular.element(
          `<div ng-message="number">
            {{'FORM.FIELD_ERROR.IS_NUMBER' | translate | capitalize}}
          </div>`
        ));

        if (
          angular.isDefined(tA.type) &&
          tA.type === 'email'
        ) messages.append(angular.element(
          `<div ng-message="email">
            {{'FORM.FIELD_ERROR.IS_EMAIL' | translate | capitalize}}
          </div>`
        ));

        if (
          angular.isDefined(tA.preMdMaxlength)
        ) messages.append(angular.element(
          `<div ng-message="md-maxlength">
            {{'FORM.FIELD_ERROR.MAX_LENGTH'
              | translate: {max: ${tA.preMdMaxlength}} | capitalize}}
          </div>`
        ));

        if (
          angular.isDefined(tA.maxlength)
        ) messages.append(angular.element(
          `<div ng-message="maxlength">
            {{'FORM.FIELD_ERROR.MAX_LENGTH'
              | translate: {max: ${tA.maxlength}} | capitalize}}
          </div>`
        ));

        if (
          angular.isUndefined(tA.checkbox) &&
          angular.isUndefined(tA.radio) &&
          angular.isUndefined(tA.switch)
        ) mdInputContainer.append(label);

        mdInputContainer.append(inputOTextarea);

        if (
          angular.isUndefined(tA.checkbox) &&
          angular.isUndefined(tA.radio) &&
          angular.isUndefined(tA.switch)
        ) mdInputContainer.append(messages);

        if (
          angular.isDefined(tA.tooltip)
        ) mdInputContainer.append(angular.element(
          `<md-tooltip>
            {{"${tA.tooltip}" | translate | capitalize}}
          </md-tooltip>`
        ));

        tE.append(mdInputContainer);

      }
    };
  }])
  .directive('formActions', [() => {
    return {
      restrict: 'E',
      compile(tE, tA) {
        tA.mForm = angular.isDefined(tA.mForm) ? tA.mForm : 'mForm';
        let b = angular.element(
          `<md-button ng-click='${tA.mForm}.save(form)'
            class = "md-accent mt-8"
            aria-label="login">
            {{${tA.mForm}.update ?
              'UPDATE': 'REGISTER' | translate | capitalize}}
          </md-button>`
        );
        tE.append(b);

      }
    };
  }])

  .directive('form', [() => {
    return {
      restrict: 'E',
      compile(tE, tA) {
        //tE.attr('ng-class',
        //  `mForm.showSpinner ? 'ye-fade-form' : 'ye-appear'`);
        tE.after(angular.element(
          `<aside class='content-spinner'
            ng-class="!mForm.showSpinner ? 'ye-fade' : 'ye-appear'">
            <md-progress-circular
              md-mode="indeterminate"
              md-theme="${tA.mdTheme || 'default'}"/>
          </aside>`
        ));
        return angular.noop;
      }
    };
  }]);

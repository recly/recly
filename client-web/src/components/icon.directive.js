'use strict';

export default angular
  .module('mdxIcon', [])
  .directive('mdxIcon', [() => {

    return (scope, element, attrs) => {

      scope.$watch(() => attrs.mdxIcon, (icon, oldIcon) => {
        element[0].classList.remove('mdi-' + oldIcon);
        element[0].classList.add('mdi-' + icon);
      });

    };

  }]);

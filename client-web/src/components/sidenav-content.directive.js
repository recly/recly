'use strict';

module.exports = angular
  .module('componentsSidenavContent', [])
  .directive('fixOverlay', ['layoutFactory', '$mdSidenav', '$document',
    '$timeout', fixOverlayFn])
  .directive('mdxSidenavContent', ['$compile', '$timeout', '$animate',
    ($c, $t, $a) => {

      return {
        scope: {
          content: '=',
        },
        restrict: 'A',
        link: link
      };

      function link(scope, elem, attrs) {

        let uppateContent = scope.$watch('content', reload);

        function reload() {

          let content = $c(angular.element(scope.content.html || '<br/>'))(
            scope.content.scope || scope);

          //content.addClass('fx-fade-down fx-speed-300');
          //let toolbar = elem.parent().find('md-toolbar').eq(0);

          $t(() => {
            let prevContent = elem.find('*').eq(0);
            //prevContent.removeClass('fx-fade-down');
            //prevContent.addClass('fx-fade-right');
            $a.leave(prevContent)
              .then(() => $a.enter(content, elem));
          }, 0);

        }

        scope.$on('$destroy', function() {
          uppateContent();
        });

      }

    }]);

function fixOverlayFn(lF, $mdS, $d, $ti) {
  return {
    link(s, elem, attrs) {

      let t1;

      //s.$watch(() => $mdS('left').isOpen(), () => appearSidenav(tog, 'left'));
      s.$watch(() => $mdS('right').isOpen(),
        (toggle) => appearSidenav(toggle, 'right'));

      function appearSidenav(toggle, idS) {

        if (toggle) t1 = $ti(() => {
          let overlay = $d[0].body.querySelector('.md-sidenav-backdrop');
          angular.element(overlay).on('click', (e) => {
            lF.sidenavAction('right', false);
            //e.stopPropagation();
          });
        }, 0);

      }

      s.$on('$destroy', () => {
        if (t1) $ti.cancel(t1);
      });

    }
  };
}

'use strict';

module.exports = angular
  .module('yeModuleCard', [
    require('./list.directive').name
  ])
  .directive('moduleCard', ['$compile', cardModuleDirectiveFn]);

function cardModuleDirectiveFn($compile) {

  return {
    scope: {
      module: '=',
      modules: '='
    },
    restrict: 'E',
    priority: 10,
    link: link
  };

  function link(scope, element, attrs) {

    if (!scope.module.crud.r && !scope.module.crud.c) return;

    fixAclRoleModule(scope.modules);

    if (scope.module.noDisplay) {
      element[0].style.display = 'none';
      return;
    }

    var listModuleComponent = $compile(angular.element('<' +
      scope.module.name + ' module = \'module\'' +
      ' modules = \'modules\' class="d-b"/>'))(scope);

    element.append(listModuleComponent);

  }

}

function fixAclRoleModule(modules) {

  var aux = {count: 0, modulesIndex: []};

  for (var i = 0; i < modules.length; i++) (function(module_, index) {
    if (module_.name === 'acl' || module_.name === 'role') {
      aux.count += 1;
      aux.modulesIndex.push(index);
    }
  })(modules[i], i);

  if (aux.count >= 2) modules[aux.modulesIndex[0]].noDisplay = true;

  //return modules;
}

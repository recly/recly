'use strict';

//require('./styles/settings');

module.exports = angular
  .module('yeSettingsDirective', [
    require('../api/api').name
  ])
  .controller('YeSettingsController', YeSettingsController)
  .directive('yeSettings', yeSettings);

function YeSettingsController($log, Settings, $translate,
  tmhDynamicLocale, apiConsama, $controller, layoutService) {

  var yeSettings = this;

  yeSettings.dataUser = formateDataUser(layoutService.data.dataUser);

  yeSettings.changePreferredLanguage = function(key) {

    Settings.prototype$updateAttributes({
      id: layoutService.data.dataUser.settings.id},
      layoutService.data.dataUser.settings).$promise
      .then(function(results) {
        layoutService.data.dataUser.settings = results;
        layoutService.logic.changeLanguage(key);
      }, function(err) {
        console.error(err);
      })

  };

  function formateDataUser(data) {
    //console.log(layoutService.data);
    return {
      initialize: layoutService.data.initialize,
      photo: apiConsama.getPhoto('personas',
        data.trabajador.persona.photo, 'cover'),
      fullName: (data.trabajador.persona.firstName || '') + ' ' +
        (data.trabajador.persona.middleName || '') + ' ' +
        data.trabajador.persona.lastName + ' ' +
        (data.trabajador.persona.secondLastName || ''),
      username: data.username,
      'creation_date': data.created,
      birthdayDate: data.trabajador.persona.birthdayDate,
      settings: data.settings
    };
  }
}
YeSettingsController.$inject = ['$log', 'Settings', '$translate',
  'tmhDynamicLocale', 'apiConsama', '$controller', 'layoutService'];

function yeSettings() {

  return {
    scope: {},
    restrict: 'E',
    template: require('./templates/settings'),
    bindToController: true,
    controller: 'YeSettingsController',
    controllerAs: 'yeSettings',
    link: link
  };

  function link(scope, elem, attrs, yeSettings) {}

}
yeSettings.$inject = [];

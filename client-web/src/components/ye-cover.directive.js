'use strict';

export default angular
  .module('yeCover', [])
  .directive('yeCover', ['$window', '$http', '$$rAF', '$animate', '$timeout',
    ($w, $h, $$rAF, $a, $t) => {

      return {
        scope: {
          spinner: '=',
          mdTheme: '@',
          actionFn: '&',
          actionIcon: '@'
        },
        require: '^^?mdSidenav',
        restrict: 'E',
        template: `
          <div class='content-spinner'
            ng-class='spinnerStop ? "ye-fade" : "ye-appear"'>
            <md-progress-circular md-mode="indeterminate"/>
          </div>
          <div class='cover_image' ng-style='photoImage'
            ng-class='!spinnerStop ? "ye-fade" : "ye-appear"'></div>
          <md-button ng-if='actionIcon' ng-click='actionFn({evt: $event})'
            class='md-fab md-accent' aria-label='action {{actionIcon}}'>
            <md-icon md-font-icon="mdi mdi-{{actionIcon}}"/>
          </md-button>
        `,
        compile(tE, tA) {

          return {
            pre(s, elem) {
              elem.attr('md-theme', s.mdTheme || 'default');
            },
            post(scope, element, attrs, mdSCtrl) {

              scope.initialized = false;

              let t2;
              let t1 = $t(() => {
                t2 = $t(() => {
                  $$rAF(init);
                }, 0);
              }, 0);

              function init() {

                if (
                  mdSCtrl === null
                ) $$rAF(resize.bind(null, wSidenav(), false));

                let watchPicture = attrs.$observe('picture', (pic) => {
                  if (mdSCtrl) $$rAF(resize.bind(null, wSidenav(), false));
                  else $$rAF(resize.bind(null, false, false));
                });

                $w.addEventListener('resize', $$rAF.throttle(resize.bind(null,
                  mdSCtrl ? wSidenav() : false, true)));

                scope.$on('$destroy', function(evt) {
                  watchPicture();
                  $t.cancel(t1);
                  $t.cancel(t2);
                });

              }

              function wSidenav() {

                let s = 180;

                if (mdSCtrl && $w.innerWidth >= 360) s = 162.5625;
                else if (
                  mdSCtrl && $w.innerWidth < 360
                ) s = ($w.innerWidth - 56) * (9 / 16);
                else if (
                  element[0].offsetWidth > 0
                ) s = element[0].offsetWidth * (9 / 16);

                return s;

              }

              function resize(sizeS, fromResize = false) {

                //console.log(sizeS, fromResize, mdSCtrl, attrs.picture);
                let height = sizeS ? sizeS : element[0].offsetWidth * (9 / 16);

                element.css({height: height + 'px'});

                if (fromResize) return;

                if (scope.initialized) scope.photoImage = {
                  'background-image': 'none'
                };
                scope.spinnerStop = false;

                if (
                  angular.isDefined(attrs.bordered)
                ) element.addClass('br-1111 md-whiteframe-z1');
                else element.addClass('bb');

                if (/data:image/.test(attrs.picture)) {
                  scope.$apply(() => {
                    scope.initialized = !scope.initialized ? true : true;
                    scope.spinnerStop = true;
                    scope.photoImage = {
                      'background-image': `url('${attrs.picture}')`
                    };
                  });
                }else if (/http/.test(attrs.picture)) $h({
                    method: 'get',
                    url: attrs.picture,
                    cache: false,
                    responseType: 'blob'
                  })
                    .then((res) => {
                      let reader = new FileReader();
                      reader.onload = () => {
                        scope.$apply(() => {
                          scope.initialized = !scope.initialized ?
                            true : true;
                          scope.photoImage = {
                            'background-image': `url('${reader.result}')`
                          };
                          scope.spinnerStop = true;
                        });
                      };
                      reader.readAsDataURL(res.data);
                    })
                    .catch((error) => {
                      console.error(error);
                    });
                else if (angular.isDefined(attrs.icon)) {
                  scope.spinnerStop = true;
                  if (!scope.initialized) {
                    let icon = angular.element(
                      `<span
                        class='mdi mdi-${attrs.icon !== '' ?
                          attrs.icon : 'checkbox-blank-circle'}'>
                      </span>`
                    );
                    element.append(icon);
                    $a.addClass(icon, 'ye-appear');
                    scope.initialized = true;
                  }
                }else {
                  scope.spinnerStop = true;
                  //element.css({backgroundImage: `url(${attrs.picture})`});
                }
              }

            }
          };
        }
      };

    }]);

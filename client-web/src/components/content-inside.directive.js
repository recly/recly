'use strict';

export default angular
  .module('contentInsideDirective', [])
  .directive('contentInside', () => {
    return {
      scope: {},
      transclude: true,
      compile(tEl, tA) {
        tEl.addClass('d-b mt-4 mb-4 bt bb inset-list');
        tEl.append(angular.element(`<md-toolbar class='md-hue-3'/>`));
        tEl.append(angular.element(`<div ng-transclude/></div>`));
        return angular.noop;
      }
    };
  });

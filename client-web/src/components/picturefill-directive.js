'use strict';

module.exports = angular
  .module('picturefillDirective', [
    require('../utils/picturefill.factory').name
  ])
  .directive('picturefill', picturefill);

function picturefill(yePictureFill) {

  function link(scope, elem) {
    yePictureFill({
      elements: [elem[0]]
    });

  }

  return link;

}

picturefill.$inject = ['yePictureFill'];

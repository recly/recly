'use strict';

module.exports = angular
  .module('dialog', [])
  .directive('mdxDialog', mdxDialogDirectiveFn);

function mdxDialogDirectiveFn($mdDialog, $rootScope) {

  return {
    scope: {
      title: '@',
      ok: '@',
      cancel: '@',
      onOk: '&',
      onCancel: '&'
    },
    link: link,
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'mdxDialog',
    transclude: true,
    template: require('./templates/dialog.jade')()
  }

  function link(scope, element, attrs, mdxDialog) {

  }

}
mdxDialogDirectiveFn.$inject = ['$mdDialog', '$rootScope'];

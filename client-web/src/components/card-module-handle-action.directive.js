'use strict';

function controller() {

  var handleAction  = this;
  console.log(handleAction);
}

function yeCardModuleHandleAction($log) {

  function link(scope, elem, attrs, ctrl) {

    scope.$on('$destroy', function() {
      $log.debug('destroy handle module view');
    });

  }

  return {
    scope: {
      moduleNamePlural: '@',
      item: '=',
      typeView: '@'
    },
    restrict: 'E',
    template: '<p>{{handleAction.typeView}}<p>',
    bindToController: true,
    controller: controller,
    controllerAs: 'handleAction',
    link: link
  };

}

module.exports = angular
  .module('yeCardModuleHandleAction', [])
  .directive('yeCardModuleHandleAction', yeCardModuleHandleAction);

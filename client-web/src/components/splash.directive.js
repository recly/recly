'use strict';

module.exports = angular
  .module('splashDirective', [
    require('../utils/picturefill.factory').name,
    require('../utils/verge.factory').name
  ])
  .directive('splash', splash);

function splash($window, yeVerge, $$rAF) {

  function link(scope, elem, attrs, splash) {

    resize();
    angular.element($window).on('resize', resize);

    function resize() {
      $$rAF(function() {

        var h = (yeVerge.viewportH() * 0.75) - 56;

        elem.css({
          height: h + 'px'
        });

        elem[0]
          .getElementsByTagName('img')[0].style.marginTop = ((h - 160) / 4) +
          'px';

        elem.find('article').css({
          marginTop: ((h - 160) / 4) + 'px'
        });

        angular.element(elem[0].getElementsByTagName('img')[0])
          .css({
            position: 'relative',
            left: ((yeVerge.viewportW() - 160) / 2) + 'px'
          });
      });

    }

  }

  return {
    scope: {},
    restrict: 'A',
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'splash',
    link: link
  };

}

splash.$inject = ['$window', 'yeVerge', '$$rAF'];

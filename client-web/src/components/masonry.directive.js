'use strict';

module.exports = angular
  .module('masonry', [])
  .controller('masonry', masonryControllerFn)
  .directive('masonry', masonryDirectiveFn);

function masonryControllerFn() {
  var masonry = this;
  masonry.init = false;
  masonry.cards = [];
}
masonryControllerFn.$inject = [];

function masonryDirectiveFn($$rAF, $window, $timeout, layoutService, $mdMedia,
  yeVerge) {

  return {
    controller: 'masonry',
    link: link
  }

  function link(scope, element) {

    var t1;
    var t2;

    t1 = $timeout(function() {
      t2 = $timeout(function() {

        $$rAF(init);
        $window.addEventListener('resize', $$rAF.throttle(reload));

        scope.$on('reloadMasonry', () => {$$rAF(reload);});

      }, 0);
    }, 0);

    function reload(e) {
      if (angular.isUndefined(scope.masonry)) return;
      scope.masonry.layout();
    }

    function init(noReload) {
      scope.masonry = new Masonry(element[0], {
        itemSelector: '.mmm',
        transitionDuration: '0.3s',
        columnWidth: '.mmm',
        percentPosition: true,
        gutter: 0
      });
    }

    scope.$on('$destroy', function() {
      $timeout.cancel(t1);
      $timeout.cancel(t2);
    });

  }
}
masonryDirectiveFn.$inject = ['$$rAF', '$window', '$timeout', 'layoutService',
  '$mdMedia', 'yeVerge'];

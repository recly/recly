'use strict';

export default angular
  .module('mdxOptions', [])
  .directive('mdxListMultipleActions', ['layoutFactory', '$$rAF',
    '$timeout', '$window', mdxListMultipleActionsDirectivefn]);

function mdxListMultipleActionsDirectivefn(lF, $$rAF, $timeout,
  $window) {

  return {
    //priority: 10,
    link: link
  };

  function link(scope, element) {

    var t1;
    var t2;

    t1 = $timeout(function() {
      t2 = $timeout(function() {
        $$rAF(init);
      }, 0);
    }, 0);

    window.addEventListener('resize', $$rAF.throttle(init));
    //scope.init = false;

    function init() {

      if (!scope.init && angular.isUndefined(scope.wrapper) &&
        angular.isDefined(element[0].parentNode.parentNode.parentNode)) {
        scope.wrapper = element[0].parentNode.parentNode.parentNode;
        scope.init = true;
        //console.log(scope.wrapper);
      }

      element[0].style.position = 'absolute';
      element[0].style.right = '6px';
      //element[0].style.top = (scope.wrapper.offsetHeight / 5.1) + 'px';
      element[0].style.top = (window.innerWidth > lF.ui
        .lgScreen ? 12 : 4) + 'px';
      scope.wrapper.style.position = 'relative';
      scope.wrapper.appendChild(element[0]);

    }

    scope.$on('$destroy', function() {
      $timeout.cancel(t1);
      $timeout.cancel(t2);
    });

  }

}

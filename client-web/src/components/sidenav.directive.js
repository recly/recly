/*
'use strict';

module.exports = angular
  .module('yeSidenavDirective', [])
  .directive('yeSidenav', yeSidenav);

function yeSidenav(layoutService.data) {

  function link(scope, elem, attrs, mdSidenav) {
    scope.$watch(function() {
      return mdSidenav.isOpen()
    }, function(status) {
      if(!status) {
        layoutService.data.sidenav.right.toolbar.icons.right = [];
        layoutService.data.sidenav.right.toolbar.icons.left = {};
      }
    });
  }

  return {
    scope: false,
    restrict: 'A',
    require: 'mdSidenav',
    link: link
  };

}
yeSidenav.$inject = [];
*/

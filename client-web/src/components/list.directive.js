'use strict';

module.exports = angular
  .module('componentsList', [
    require('../utils/find-by.factory').name
  ])

  .controller('DeleteDialogController', ['$http', '$mdDialog', '$translate',
    'apiConsama', DeleteDialogController])

  .directive('yeScrollable', ['$timeout', '$$rAF', '$q', '$window',
    yeScrollableFn])

  .directive('mdxList', ['$timeout', '$animate', '$controller',
    '$mdDialog', '$mdToast', '$mdSidenav', 'yeFindBy', '$translate', '$compile',
    '$ocLazyLoad', '$translatePartialLoader', 'layoutFactory', '$q', '$$rAF',
    '$http', 'apiConsama', '$log', '$window', '$rootScope',
    mdxListDirectiveFn]);

function yeScrollableFn($ti, $$r, $q, $w) {
  return {
    link(s, elem) {

      let t2;
      let t1;

      init()
        .then(() => {

          resize();

          $w.addEventListener('resize', () => resize());

          function resize() {
            //console.log('resize scrollable',
            //  elem[0].scrollWidth, elem[0].offsetWidth);
            if (
              elem[0].scrollWidth > elem[0].offsetWidth &&
              !elem[0].classList.contains('ox-s')
            ) elem[0].classList.add('ox-s');
            else if (
              elem[0].scrollWidth <= elem[0].offsetWidth &&
              elem[0].classList.contains('ox-s')
            ) elem[0].classList.remove('ox-s');
          }

        });

      function init() {
        return $q((resolve, reject) => {
          t1 = $ti(() => {
            t2 = $ti(() => {
              $$r(() => {
                resolve();
              });
            }, 0);
          }, 0);
        });
      }

      s.$on('$destroy', () => {
        $ti.cancel(t1);
        $ti.cancel(t2);
      });

    }
  };
}

function DeleteDialogController($http, $mdDialog, $translate, apiConsama) {

  var deleteModal = this;

  deleteModal.close = () => $mdDialog.cancel({showToast: false});

  deleteModal.delete = (item, pluralName) => {
    $http({
      method: 'delete',
      url: apiConsama.url + '/' + pluralName + '/' + item.id
    })
      .then(function(success) {
        $translate((pluralName + '_SECTION.' + 'DELETE_SUCCESS').toUpperCase(),
          {itemName: item.title})
          .then(function(sentence) {
            $mdDialog.hide(sentence);
          });
      }, function(error) {
        $translate((pluralName + '_SECTION.' + 'DELETE_FAIL').toUpperCase(),
          {itemName: item.title})
          .then(function(sentence) {
            $mdDialog.cancel({
              showToast: true,
              message: sentence
            });
          });
      });

  };
}

function mdxListDirectiveFn($timeout, $animate, $controller, $mdDialog,
  $mdToast, $mdSidenav, yeFindBy, $translate, $compile, $ocLazyLoad,
  $translatePartialLoader, lF, $q, $$rAF, $http, apiConsama, $log, $w, $rS) {

  return {
    restrict: 'E',
    scope: {
      module: '=',
      itemsP: '=items',
      model: '=',
      options: '=',
      modelOptions: '=',
      embedModule: '=',
      embedCtrl: '='
    },
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'list',
    template: require('./templates/list.jade')(),
    transclude: true,
    link: {
      pre: preLink,
      post: postLink
    }
  };

  function preLink(scope, elem, attrs, list) {
    list.linesClass = 'md-' + (attrs.lines || 2) + '-line';
    list.widthContainer = elem[0].offsetWidth;
  }

  function postLink(scope, elem, attrs, list, transcludeFn) {

    list.initialize = false;

    let t3;
    let t2;
    let t1 = $timeout(() => {
      t2 = $timeout(() => {

        init()

          .then((res) => {

            list.loaded = true;
            list.items = res.items;
            list.fail = false;
            list.failMessage = res.message;
            list.actions = initAction();
            list.initialize = true;

            list.watchDataListType = scope
              .$watch(() => list.module.listType, watchList);

            $w.addEventListener('resize',
              watchList.bind(null, list.module.listType));

          })

          .catch((fail) => {
            $log.warn(fail.msg);
            list.loaded = true;
            list.fail = true;
            list.failMessage = fail.message;
          });

      }, 0);
    }, 0);

    $rS.$on('$translateChangeSuccess', () => {
      return $translate('MODEL.PLURAL.' + list.module.name.toUpperCase())
      .then((t) => {
        return list.pluralName = t;
      });
    });

    function init() {

      return $q((resolve, reject) => {

        let codeI18n = '';
        let logErr = '';

        if (angular.isUndefined(list.module)) {
          codeI18n = 'WARN.NOT_MODULE_LIST';
          logErr = `set a module ${attrs.modelName}`;
        }else if (angular.isUndefined(list.module.crud)) {
          codeI18n = 'WARN.NOT_MODULE_LIST';
          logErr = `set crud for module ${list.module.name}`;
        }else if (!list.module.crud.r) {
          codeI18n = 'WARN.NOT_READ_PERMISSION';
          logErr = `don't have permit to read ${list.module.name}`;
        }else if (angular.isUndefined(list.itemsP)) {
          //codeI18n = 'WARN.NOT_ARRAY';
          //logErr = `not passed items to the list ${attrs.modelName}`;
          list.itemsP = [];
        }else if (!angular.isArray(list.itemsP)) {
          codeI18n = 'WARN.NOT_ARRAY';
          logErr = `yout must pass an array ${attrs.modelName}`;
        }else if (list.itemsP.length === 0) codeI18n = 'ITEMS.NONE';

        let moduleName = list.module && list.module.name ?
          list.module.name : attrs.modelName;

        $translate('MODEL.PLURAL.' + moduleName.toUpperCase())
          .then((t) => {
            list.pluralName = t;
            list.errCodeI18n = codeI18n;
            return $translate(codeI18n, {modulePluralName: t});
          })
          .then((t) => {
            if (logErr === '') resolve({
              message: t,
              items: lF.listFormate(
                list.itemsP,
                lF.listPattern(list.module.name)
              )
            });
            else reject({msg: logErr, message: t});
          });

      });

    }

    list.transclude = (id, items, i) => {

      if (angular.isUndefined(list.embedModule)) return;

      let iScope = scope.$new(true);
      iScope.item = yeFindBy(items, 'id', id).item;

      iScope.embed = angular.extend(list.embedCtrl);

      if (angular.isUndefined(iScope.embed.options)) iScope.embed.options = {};
      iScope.embed.options.embed = true;

      transcludeFn(iScope, (clonedElement, eScope) => {
        elem.find('aside').eq(i).append(clonedElement);
      });

    };

    list.getPhoto = (item, container) => {

      item.photoImage = require('./assets/default-image.jpg');

      if (angular.isUndefined(item.photo)) return;

      console.log(item);
      var url = apiConsama.getPhoto(container,
        item.photo, 'standard');

      $http({
        url: url,
        method: 'GET',
        cache: false,
        responseType: 'blob'
      })
        .then(function(res) {
          var reader = new FileReader();
          reader.onload = function() {
            scope.$apply(function() {
              item.photoImage = reader.result;
            });
          };
          reader.readAsDataURL(res.data);
        }, function(err) {
          console.error(err);
        });

    };

    list.showItem = (evt, item, items) => {
      lF.showItem(scope, list, evt, item, items);
    };

    list.deleteItem = (evt, item, items) => {
      lF.deleteItem(scope, list, evt, item, items, exeDelete);
    };

    list.updateItem = (evt, item, items, fromSN) => {
      lF.updateItem(scope, list, evt, item, items, fromSN);
    };

    //-

    function exeDelete(item) {
      return list.model.destroyById({id: item.id}).$promise;
    }

    function getWidthListTable(headTableList) {
      var photoExists = false;
      var headFields = headTableList[0].children;
      for (var i = 0; i < headFields.length; i++)(function(nodeC, index) {
        if (nodeC.classList.contains('photo')) photoExists = true;
      })(headFields[i], i);

      var wListTable = photoExists ?
        ((headFields.length - 1) * 120) + 72 :
        headFields.length * 120;

      return (elem[0].offsetWidth >= wListTable) ?
        elem[0].offsetWidth :
        wListTable;

    }

    function applyRight(nodeList, rightLength) {
      for (var i = 0; i < nodeList.length; i++)(function(nodeC, index) {
        nodeC.style.right = rightLength + 'px';
      })(nodeList[i], i);
      return;
    }

    function checkForScrool(tableWidth, widthContainer) {
      return (tableWidth > widthContainer) ? 'ox-s' : '';
    }

    function initAction() {
      //console.log(list.embedModule);
      var crud = ['u', 'd'];
      list.showSubItems = [];

      var actions = [];
      for (var i = 0; i < crud.length; i++) {
        if (list.module.crud[crud[i]]) actions.push({
          name: crud[i],
          icon: 'mdi-' + (crud[i] === 'u' ? 'pencil' : 'delete'),
          click: crud[i] === 'u' ? list.updateItem : list.deleteItem
        });
      }

      if (angular.isDefined(list.embedModule)) $translate(
        'MODEL.PLURAL.' + list.embedModule.name.toUpperCase())
          .then(function(t) {
            actions.push({
              modelPluralName: t,
              name: 's',
              icon: 'mdi-chevron-' + (list.showSubItems ? 'down' : 'up'),
              click(e, item, items) {

                let i = yeFindBy(items.list, 'id', item.id).index;
                if (i !== -1) {
                  list['tmpToggle' + i] = list.showSubItems[i];
                  list.showSubItems[i] = !list.showSubItems[i];
                  //layoutService.logic.toggleMasonry(scope, 320, list,
                  //  'tmpToggle' + i);
                }

              }
            });
          });

      return actions;

    }

    function watchList(type) {

      list.module.listType = type ? type : 'list';

      $$rAF(function() {
        var auxW = elem[0].querySelectorAll('.mdx-list__section--head');
        var mdList = elem[0].getElementsByTagName('md-list')[0];
        var actions = elem[0].querySelectorAll('[mdx-list-multiple-actions]');

        if (type === 'table') {
          elem[0].style.overflowX = (
            getWidthListTable(auxW) > elem[0].offsetWidth
          ) ? 'scroll' : 'hidden';
          //applyRight(actions, -61);
          mdList.style.width = getWidthListTable(auxW) + 'px';
        } else {
          elem[0].style.overflowX = 'hidden';
          //applyRight(actions, 6);
          mdList.style.width = 'auto';
        }
      });

    }

    scope.$on('$destroy', () => {
      $timeout.cancel(t1);
      $timeout.cancel(t2);
      if (angular.isDefined(list.watchDataListType)) list.watchDataListType();
    });

  }

}

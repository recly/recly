'use strict';

module.exports = angular
  .module('empresaCard', [
    //require('../utils/find-by.factory.js').name,
    //require('./list-item-multiple-actions.directive.js').name
  ])
  .directive('empresaCard', ['Empresa', 'apiConsama', '$timeout', '$$rAF',
    'yeFindBy', 'layoutService', 'Area', 'Sucursal', 'Trabajador', 'Flota',
    'Cargo', 'Camion', 'layoutFactory', empresaCardDirectiveFn])
  .directive('empresasCard', ['Empresa', 'layoutFactory', 'yeFindBy',
    '$animate', '$timeout', '$q', empresasCardDirectiveFn]);

function empresaCardDirectiveFn(Empresa, apiConsama, $timeout, $$rAF,
  yeFindBy, layoutService, Area, Sucursal, Trabajador, Flota, Cargo, Camion,
  lF) {

  return {
    restrict: 'E',
    scope: {
      empresa: '=',
      modules: '='
    },
    controller: angular.noop,
    controllerAs: 'eCard',
    bindToController: true,
    template: require('./templates/empresa-card.jade')(),
    link: link
  };

  function link(scope, element, attrs, eCard) {

    init();

    function init() {

      eCard.photo = apiConsama.getPhoto('empresas',
        eCard.empresa.photo, 'cover');

      eCard.summary = parseSubModule(eCard.modules, 'empresa');
      eCard.area = parseSubModule(eCard.modules, 'area');
      eCard.cargo = parseSubModule(eCard.modules, 'cargo');
      eCard.sucursales = parseSubModule(eCard.modules, 'sucursal');
      eCard.trabajador = parseSubModule(eCard.modules, 'trabajador');
      eCard.flota = parseSubModule(eCard.modules, 'flota');
      eCard.camion = parseSubModule(eCard.modules, 'camion');

      eCard.mArea = Area;
      eCard.mSucursal = Sucursal;
      eCard.mTrabajador = Trabajador;
      eCard.mFlota = Flota;
      eCard.mCargo = Cargo;
      eCard.mCamion = Camion;

      eCard.options = {
        empresa: eCard.empresa
      };

    }

    eCard.createArea = (evt, module_, empresa) => {

      let moduleController = {
        module: angular.extend({}, module_),
        options: {empresa: empresa}
      };
      lF.createItem(scope, moduleController, evt);

    };

    eCard.createCamion = (evt, module_, flota) => {

      let moduleController = {
        module: angular.extend({}, module_),
        options: {flota: flota}
      };
      lF.createItem(scope, moduleController, evt, 'x3');

    };

    eCard.createFlota = (evt, module_, empresa) => {

      let moduleController = {
        module: angular.extend({}, module_),
        options: {
          empresa: empresa,
          type: 0
        }
      };
      lF.createItem(scope, moduleController, evt, 'x3');

    };

    eCard.createTrabajador = (evt, module_, empresa) => {

      let moduleController = {
        module: angular.extend({}, module_),
        options: {
          empresa: empresa
        }
      };
      lF.createItem(scope, moduleController, evt, 'x3');

    };

    eCard.createCargo = (evt, module_, area) => {

      let moduleController = {
        module: angular.extend({}, module_),
        options: {area: area}
      };
      lF.createItem(scope, moduleController, evt);

    };

    eCard.editEmpresa = (evt, empresa, module_) => {

      let mC = {
        module: angular.extend({}, module_)
      };

      lF.updateItem(scope, mC, evt, empresa, [], false, true,
        'comercialName', 'x3');

    };

    //-
    function parseSubModule(modules, moduleName) {

      let modulePre = yeFindBy(modules, 'name', moduleName).item;

      if (
        angular.isUndefined(modulePre) ||
        angular.isUndefined(modulePre.crud)
      ) return {name: moduleName, visible: false};
      else if (moduleName !== 'empresa' && !modulePre.crud.r) {
        return angular.extend({}, modulePre, {visible: false});
      }else if (moduleName === 'empresa' && modulePre.crud.r) {
        return angular.extend({}, modulePre, {visible: true});
      }

      let module_ = angular.extend({}, modulePre);

      if (eCard.empresa[module_.pluralName]) {
        module_.items = eCard.empresa[module_.pluralName];
        module_.visible = true;
      }else if (moduleName === 'empresa') {
        module_ = modules;
        module_.visible = true;
      }else if (moduleName === 'camion') {
        module_.visible = true;
      }else module_.visible = false;

      return module_;

    }

  }

}

function empresasCardDirectiveFn(E, lF, yeFindBy) {

  return {
    scope: {
      modules: '='
    },
    restrict: 'E',
    template: require('./templates/empresas-card.jade')(),
    controller: angular.noop,
    controllerAs: 'empresasCard',
    bindToController: true,
    link: link
  };

  function link(scope, element, attrs, empresasCard) {

    init()
      .then((data) => {

        empresasCard.empresas = data;
        empresasCard.module = getEmpresaModule(empresasCard.modules);
        empresasCard.initialize = true;

        empresasCard.createEmpresa = (evt) => {

          let moduleController = {
            module: angular.extend({}, empresasCard.module),
            options: {mainObligate: empresasCard.empresas === 0 ? true : false}
          };

          lF.createItem(scope, moduleController, evt, 'x3');

        };

      })
      .catch((e) => reject(e));

    //--
    function init() {
      empresasCard.initialize = false;
      empresasCard.empresas = [];
      return loadData();
    }

    function loadData() {
      let includes = formateIncludes(empresasCard.modules);
      return E.find({filter: {include: includes}}).$promise;
    }

    function getEmpresaModule(modules) {
      let empresaModule = yeFindBy(modules, 'name', 'empresa').item;
      return angular.isDefined(empresaModule) ? empresaModule : {};
    }

    function formateIncludes(modules) {

      let res = [];

      return [
        {trabajadores: 'persona'},
        {areas: ['cargos']},
        {sucursales: ['almacenes']},
        {'flotas': ['camiones']}
      ];
    }

  }

}

'use strict';

module.exports = angular
  .module('yeModuleToolbar', [])
  .directive('moduleToolbar', moduleToolbarDirectiveFn);

function moduleToolbarDirectiveFn() {

  function link(scope, elem, attrs) {
  }

  return {
    scope: {
      module: '='
    },
    restrict: 'E',
    template: require('./templates/module-toolbar.jade')(),
    transclude: true,
    bindToController: true,
    controller: angular.noop,
    controllerAs: 'moduleToolbar',
    link: link
  };

}
moduleToolbarDirectiveFn.$inject = [];

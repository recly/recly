'use strict';

require('angular-file-upload');

module.exports = angular
  .module('components.inputFigure', [
    'angularFileUpload'
  ])
  .directive('inputPicture', ['$log', 'FileUploader', '$mdToast', '$translate',
    'apiConsama', inputPictureFn]);

function inputPictureFn($log, FileUploader, $mdToast,
  $translate, apiConsama) {

  return {
    scope: {
      cover: '=',
      storage: '@',
      photo: '='
    },
    controller: angular.noop,
    controllerAs: 'inputFigure',
    bindToController: true,
    restrict: 'EA',
    template: require('./templates/input-figure.directive.jade')(),
    link: {
      pre: (s, e, a, vm) => {

        vm.mdTheme = a.mdTheme || 'default';

        let tmpName = vm.storage + '-' + Date.now().toString();

        vm.uploader = new FileUploader({
          url: apiConsama.url + '/pictures/temp/upload',
          autoUpload: true,
          withCredentials: true,
          isHTML5: true,
          removeAfterUpload: true,
          filters: [
            {
              name: 'imageFilter',
              fn: (item, options) => {
                let type = item.name.slice(item.name.lastIndexOf('.'));
                type = type.toLowerCase();
                if (/(jpg|png|jpeg|bmp|gif)/.test(type)) return true;
                else $translate('SENTENCES.ACCEPTED_IMAGES',
                  {failFormat: type, formats: 'jpg, png, jpeg, bmp, gif'})
                  .then((message) => {
                    $mdToast.showSimple(message);
                    return false;
                  });
              }
            }
          ],
          formData: {
            fields: {
              storage: tmpName
            }
          }
        });

      },
      post: (scope, element, attrs, vm) => {
        vm.spinner = {show: true};

        vm.uploader.onBeforeUploadItem = (item) => {
          //console.log('launch spinner');
          console.log(item);
          vm.spinner.show = true;
        };

        vm.uploader.onProgressItem = (item, progress) => {
          //console.debug(progress);
          vm.spinner.percent = progress;
        };

        vm.uploader.onErrorItem = (item, response, status, headers) => {
          element[0].querySelector('.drop').classList.remove('success');
          element[0].querySelector('.drop').classList.add('error');
          //$mdToast.showSimple('Hubo un error al subir la imagen');
          showThumb(item._file);
        };

        vm.uploader.onSuccessItem = (item, response, status, headers) => {
          element[0].querySelector('.drop').classList.remove('error');
          element[0].querySelector('.drop').classList.add('success');
          //
          //  if (angular.isDefined(item)) showThumb(item._file);
          showThumb(item._file);
          vm.photo = response.result.files.file[0].name;
          //
          return;
        };

        //////////////////
        function showThumb(picture) {
          let reader = new FileReader();
          reader.onload = () => {
            scope.$apply(() => {
              //vm.spinner.show = false;
              vm.cover = reader.result;
            });
          };
          reader.readAsDataURL(picture);
        }
      }
    }
  };

}

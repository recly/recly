'use strict';

import ventaAM from './venta.directive.js';
import ventaFormAM from './venta-form.directive.js';

export default angular
  .module('venta', [
    ventaAM.name,
    ventaFormAM.name
  ]);

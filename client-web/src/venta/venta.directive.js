'use strict';

export default angular
  .module('ventaList', [])
  .directive('venta', ['layoutFactory', 'Venta', '$q', ventaListDirectiveFn]);

function ventaListDirectiveFn(lF, V, $q) {

  return {
    restrict: 'E',
    scope: {
      module: '='
    },
    template: require('./templates/venta.jade')(),
    controller: angular.noop,
    controllerAs: 'ventaList',
    bindToController: true,
    link(scope, element, attrs, ctrl) {

      ctrl.asignRoute = (evt) => lF.createItem(scope, ctrl, evt, 'x3');

    }
  };

}

'use strict';

export default angular
  .module('ventaForm', [])
  .directive('ventaForm', ['layoutFactory', 'Venta', '$q', 'Flota', 'Camion',
    '$log', '$mdDialog', '$translate', '$mdToast', 'Trabajador', 'Cliente',
    'gmapUtilsFactory', ventaFormFn]);

function ventaFormFn(lF, V, $q, F, C, $l, $mdD, $tr, $mdT, T, Cl, gmapUF) {

  return {
    restrict: 'E',
    scope: {
      module: '=',
      modules: '='
    },
    template: require('./templates/venta-form.jade')(),
    controller: angular.noop,
    controllerAs: 'mForm',
    bindToController: true,
    link(scope, element, attrs, ctrl) {

      init()

        .then((data) => {

          ctrl.camiones = data[0];
          ctrl.tCliente = data[1];
          ctrl.tCamionName = data[2];

          ctrl.createCliente = (evt) => $mdD.show({
            targetEvent: evt,
            template: `
              <md-dialog
                md-theme='cliente'
                aria-label='mdDialog'>
                <cliente-form/>
              </md-dialog>
            `,
            //controller: 'dialogAssign',
            //controllerAs: 'vm',
            //bindToController: true,
            clickOutsideToClose: true
          });

          ctrl.loadClientes = () => {
            return Cl.find({filter: {include: ['type']}}).$promise
              .then((data) => {

                if (data.length === 0) return $tr('MODEL.PLURAL.CLIENTE')
                    .then((t) => $tr('ITEMS.NONE', {modulePluralName: t}))
                    .then((t) => {
                      ctrl.clientes = [];
                      return $mdT.showSimple(t);
                    });
                else {
                  return ctrl.clientes = [];
                }
              })
              .catch((e) => {
                $l.warn(e);
              });
          };

          ctrl.loadChoferes = () => {
            return T.find({filter: {include: ['area', 'persona']}}).$promise
              .then((data) => {

                if (data.length === 0) return $tr('MODEL.PLURAL.TRABAJADOR')
                    .then((t) => $tr('ITEMS.NONE', {modulePluralName: t}))
                    .then((t) => {
                      ctrl.choferes = [];
                      return $mdT.showSimple(t);
                    });
                else {
                  let choferes = [];
                  angular.forEach(data, (t) => {
                    if (t.cargo.name === 'chofer') choferes.push(t);
                  });
                  if (choferes.length === 0) return $tr('FIELDS.CHOFERES')
                    .then((t) => $tr('ITEMS.NONE', {modulePluralName: t}))
                    .then((t) => {
                      ctrl.choferes = [];
                      return $mdT.showSimple(t);
                    });
                  return ctrl.choferes = choferes;
                }
              })
              .catch((e) => {
                $l.warn(e);
              });
          };

          ctrl.apiConsamaImg = (item) => {
            return lF.apiConsamaImg(scope, item, 'camiones');
          };

          ctrl.assignRoute = (evt, camion) => {

            ctrl.showAssignRoute = true;
            ctrl.camion = camion;

            $tr('SENTENCES.ASSIGN_ROUTE_FOR', {vehicle: camion.licensePlate})
              .then((t) => {
                lF.sidenavRightTitle = t;
                lF.sidenavRightIconLeft = {
                  icon: 'arrow-left',
                  click: (evt) => {
                    return $tr('MODEL.VENTA')
                      .then((t2) => $tr('ITEM.NEW', {moduleName: t2}))
                      .then((t2) => {
                        ctrl.showAssignRoute = false;
                        lF.sidenavRightTitle = t2;
                        lF.sidenavRightIconLeft = 'default';
                      });
                  }
                };
              });
          };

          ctrl.minDate = lF.getDate(0);

          ctrl.getUbication = (evt, camion) => gmapUF.launchMap(evt,
            camion.ubication, 'venta', true, camion.licensePlate);

          ctrl.save = () => {
            console.log('sent');
          };

        })

        .catch((e) => $l.warn(e));

      function init() {
        return $q.all([
          C.find().$promise,
          $tr('MODEL.CLIENTE'),
          $tr('MODEL.PLURAL.CAMION')
        ]);
      }

    }
  };

}

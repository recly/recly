'use strict';

module.exports = angular
  .module('caja', [
    require('./caja-list.directive.js').name
  ]);

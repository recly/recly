'use strict';

import clienteAM from './cliente.directive.js';
import clienteFormAM from './cliente-form.directive.js';

export default angular
  .module('clienteModule', [
    clienteAM.name,
    clienteFormAM.name
  ]);

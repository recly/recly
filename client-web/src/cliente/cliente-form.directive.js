'use strict';

export default angular
  .module('clienteForm', [])
  .directive('clienteForm', ['$q', 'layoutService', clienteFormFn]);

function clienteFormFn($q, lS) {

  return {
    scope: {
      item: '=',
      options: '='
    },
    restrict: 'E',
    template: require('./templates/cliente-form.jade')(),
    controller: angular.noop,
    controllerAs: 'mForm',
    bindToController: true,
    compile(tE) {

      tE.find('form').eq(0).attr('ng-class',
        'mForm.showSpinner ? "ye-fade-form" : "ye-appear"');

      return (scope, element, attrs, ctrlForm) => {

        lS.logic.getFormInitial(scope, element, ctrlForm, [], 'cliente')

          .then((formData) => {
            ctrlForm.form = formData;
            ctrlForm.save = (form) => {
              lS.logic.formUpsert(A, form, ctrlForm);
            };
          })

          .catch((e) => {
            console.warn(e);
          });

      };
    }
  };

}

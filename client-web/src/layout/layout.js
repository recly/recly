'use strict';

import TweenMax from 'gsap/src/uncompressed/TweenMax';

import angularAria from 'angular-aria';
import angularAnimate from 'angular-animate';
import angularMessages from 'angular-messages';
import angularResource from 'angular-resource';

import angularMaterial from 'angular-material';

import angularTranslate from 'angular-translate';
import angularTranslateLoaderPartial from 'angular-translate-loader-partial';
import angularDynamicLocale from 'angular-dynamic-locale';
import ngFx from 'ngFx';

import googleMaps from '../components/google-maps.js';
import mdxIconAngularModule from '../components/icon.directive.js';
import empresaCardAngularModule from '../components/empresa-card.directive.js';
import coverAngularModule from '../components/ye-cover.directive.js';
import findByAngularModule from '../components/settings.directive.js';
import contentInsideAM from '../components/content-inside.directive.js';
import yeForm from '../components/ye-form.directive.js';
import listAngularModule from '../components/list.directive.js';
import listOptions from '../components/list-item-multiple-actions.directive.js';
import findByAM from '../utils/find-by.factory.js';
import capitalizeFilterAM from '../utils/capitalize.filter.js';
import yeTranslateFilterAM from '../utils/ye-translate.filter.js';
import getLanguageAngularModule from '../utils/get-language.factory.js';
//import loadScriptAngularModule from '../utils/load-script.factory.js';
import validFormAngularModule from '../utils/valid-form.factory.js';
import sidenavContentAM from '../components/sidenav-content.directive.js';

import Initialize from './logic/initialize.js';

export default angular
  .module('appLayout', [
    angularAria,
    angularAnimate,
    angularMessages,
    angularResource,
    ngFx,
    'ngMaterial',
    'pascalprecht.translate',
    'tmh.dynamicLocale',
    'apiConsama',
    googleMaps.name,
    mdxIconAngularModule.name,
    empresaCardAngularModule.name,
    coverAngularModule.name,
    findByAngularModule.name,
    contentInsideAM.name,
    yeForm.name,
    listAngularModule.name,
    listOptions.name,
    findByAM.name,
    capitalizeFilterAM.name,
    yeTranslateFilterAM.name,
    getLanguageAngularModule.name,
    //loadScriptAngularModule.name,
    validFormAngularModule.name,
    sidenavContentAM.name
  ])
  .provider('layoutService', layoutProviderFn)
  .provider('layoutFactory', layoutFactoryProviderFn)
  .config(require('./layout.config'))
  .factory('randomApellido', ['$q', function($q) {
    return (apellido) => {
      let apellidos = ['asas', 'sdsd', 'qqqq', 'nnnn'];
      return apellidos[2] ;
    };
  }])
  .run(require('./layout.run'))
  .controller('LayoutController', require('./layout.controller.js'))

  .controller('ApiConsamaController', ['layoutFactory', (lF) => {
    //lF.appbarTitle = 'APPBAR_TITLE';
  }]);

window.MessageFormat = require('messageformat');
require('messageformat-locale-en');
require('messageformat-locale-es');
require('angular-translate-interpolation-messageformat');

function layoutFactoryProviderFn() {
  this.$get = ['yeFindBy', '$translate', 'tmhDynamicLocale', 'getLanguage',
    '$ocLazyLoad', '$q', 'Settings', 'apiConsamaOffline',
    '$http', 'User', '$translatePartialLoader', '$mdDialog',
    '$mdToast', 'capitalizeFilter', 'yeValidForm', '$timeout', '$$rAF',
    'apiConsama', '$mdSidenav', '$state',
    '$document', '$compile', '$log', require('./logic/layout-provider.js')];
}

function layoutProviderFn() {

  this.$get = [layoutServiceFn];

  function layoutServiceFn() {

    return {};

    function toggleMasonry(scope, timeMax = 320, ctrl,
      propToggle = 'tabSelected') {

      if (angular.isUndefined(ctrl)) return;

      let t1;
      let getDelay = (tM, i, oI) => tM + (Math.abs(i - oI) * 150);

      scope.$watch(() => {return ctrl[propToggle];}, (i, oI) => {

        //scope.$emit('reloadMasonry');
        t1 = $timeout(() => {
          scope.$emit('reloadMasonry');
        }, angular.isNumber(i) ? getDelay(timeMax, i, oI) : i ? 0 : timeMax);

      });

      scope.$on('$destroy', () => {
        $timeout.cancel(t1);
      });

    }

    function toggleCard(card, scope) {
      if (card.toggleStatus) {
        card.toggleStatus = !card.toggleStatus;
        let aux = $timeout(() => {
          $$rAF(() => {scope.$emit('reloadMasonry3');});
          $timeout.cancel(aux);
        }, 301);
      } else {
        $$rAF(() => {scope.$emit('reloadMasonry3');});
        let aux = $timeout(() => {
          card.toggleStatus = !card.toggleStatus;
          $timeout.cancel(aux);
        }, 1);
      }
    };

    function toggleTypeListCard(card, scope) {

      if (card.module.listType === 'table') {
        card.module.listType = card.module.listType === 'table' ?
          'list' : 'table';
        let aux = $timeout(() => {
          $$rAF(() => {scope.$emit('reloadMasonry3');});
          $timeout.cancel(aux);
        }, 301);
      } else {
        $$rAF(() => {scope.$emit('reloadMasonry3');});
        let aux = $timeout(() => {
          card.module.listType = card.module.listType === 'table' ?
            'list' : 'table';
          $timeout.cancel(aux);
        }, 1);
      }

    };

    //--

  }
}

'use strict';

module.exports = function listFormate(items, pattern) {
  return {
    original: items,
    list: listFormateType(items, pattern.list),
    table: listFormateType(items, pattern.table)
  };

  function listFormateType(rows, formatePattern) {

    var items = [];
    //console.log(rows.length);
    angular.forEach(rows, function(row) {
      if (angular.isArray(formatePattern)) {
        items.push(parseArray(row, formatePattern));
      }else if (angular.isObject(formatePattern)) {
        items.push(parseObject(row, formatePattern));
      }else console.warn('pattern is not array or object', formatePattern, rows);
    });
    return items;

  }

  function parseObject(row, formatePattern) {

    var item = {};

    item.id = row.id;

    angular.forEach(formatePattern, function(vPattern, fPattern) {

      if (angular.isFunction(vPattern)) item[fPattern] = vPattern(row);
      else if (vPattern.indexOf('.') !== -1) {
        var resultEmbedModel = '';
        try {
          resultEmbedModel = eval('row.' + vPattern);
        }catch (err) {
          resultEmbedModel = '';
        }
        item[fPattern] = resultEmbedModel;

      }else item[fPattern] = row[vPattern];

    });

    return item;

  }

  function parseArray(row, formatePattern) {

    var item = {};
    item.id = row.id;

    angular.forEach(formatePattern, function(vPattern) {
      if (angular.isFunction(vPattern)) item[vPattern] = vPattern(row);
      else if (vPattern.indexOf('.') !== -1) {
        var resultEmbedModel = '';
        try {
          resultEmbedModel = eval('row.' + vPattern);
        }catch (err) {
          resultEmbedModel = '';
        }
        var fieldName = vPattern.substring(vPattern.lastIndexOf('.') + 1);
        item[fieldName] = resultEmbedModel;
      }else item[vPattern] = row[vPattern];
    });
    return item;
  }

};

'use strict';

//module.exports = Initialize;

export default class Initialize {

  constructor() {
    this.idAcls = 1;
  }

  dataUserInitialize(s) {
    return {
      trabajador: {
        persona: getInitializePerson(),
        roles: getInitializeRoles.call(this)
      },
      settings: s
    };
  }
}

function getInitializeRoles() {

  let res = [
    {
      id: '0',
      name: 'initialize',
      description: 'this role only works when there is one user',
      acls: []
    }
  ];

  let c = 'create';
  let r = 'find';
  let u = 'prototype_updateAttributes';
  let d = 'deleteById';

  formatAcl.call(this, res[0].acls, 'agregado', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'area', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'acl', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'asistencia', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'caja', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'camion', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'cargo', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'cliente', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'compra', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'empresa', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'inventario', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'flota', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'role', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'remuneracion', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'sucursal', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'settings', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'trabajador', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'translate', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'user', [c, r, u, d]);
  //formatAcl.call(this, res[0].acls, 'unidadMedida', [c, r, u, d]);
  formatAcl.call(this, res[0].acls, 'venta', [c, r, u, d]);

  return res;

  function formatAcl(acls, model, props) {

    let iterator = (prop) => {
      acls.push({
        id: this.idAcls, model: model, property: prop,
        accessType: prop === 'create' || prop === 'prototype_updateAttributes' ?
          'WRITE' : 'READ',
        permission: 'ALLOW', principalType: 'ROLE', principalId: 'initialize',
      });
      this.idAcls += 1;
    };
    angular.forEach(props, iterator);
  }

}

function getInitializePerson() {
  return {
    firstName: 'initialize',
    photo: 'default.jpg',
    middleName: '',
    lastName: 'initialize',
    secondLastName: 'initialize',
    birthdayDate: 'Tue Mar 12 1991 00:00:00 GMT-0500 (PET)',
    sex: 'male',
    identityDocument: {type: 'dni', number: '555555'},
  };
}

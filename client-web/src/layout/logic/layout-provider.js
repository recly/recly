'use strict';

import Initialize from './initialize.js';

export default function layoutFactory(yeFindBy, $tr, tmhDynamicLocale,
  getLanguage, $oc, $q, S, apiConsamaOffline,
  $http, U, $translatePartialLoader, $mdD,
  $mdT, capitalizeFilter, yeValidForm, $t, $$rAF,
  apiConsama, $mdS, $state, $d, $compile, $l) {

  let Layout = class LayoutP {

    constructor() {

      this.data = {
        loggued: false,
        initialize: false,
        apiConsamaIsOffline: false,
        dataUser: {},
      };

      this.ui = {
        smScreen: 600,
        mdScreen: 960,
        lgScreen: 1200,
        xlgScreen: 1600,
        assetsLoaded: [],
        content: {
          'classContainer': '',
        },
        appbar: {
          title: 'consama'
        },
        sidenav: {
          left: {},
          right: {
            type: '',
            theme: 'default',
            toolbar: {
              title: 'sidenav right title',
              icons: {
                left: {},
                right: []
              }
            },
            content: {
              html: ''
            }
          }
        }
      };

    }

    set appbarTitle(title) {
      this.ui.appbar.title = title;
    }

    refresh() {
      return $state.reload('layout');
    }

    changeLanguage(key) {
      $$rAF(() => {
        this.ui.currentLanguage = key;
        tmhDynamicLocale.set(key);
        $tr.use(key);
      });
    }

    settings() {

      this.sidenavRightTitle = 'settings';
      this.sidenavRightTheme = 'default';

      this.sidenavRightContent('<ye-settings/>');

      return this.sidenavAction('left', false)
        .then(() => this.sidenavAction('right', true));
    }

    logout() {

      return U.logout().$promise
        .then((response) => {

          localStorage.removeItem('$LoopBack$accessTokenId');
          localStorage.removeItem('$LoopBack$currentUserId');
          sessionStorage.removeItem('$LoopBack$accessTokenId');
          sessionStorage.removeItem('$LoopBack$currentUserId');

          return $q.all([
            this.sidenavAction('left', false),
            this.sidenavAction('right', false)
          ]);
        })

        .catch((e) => {
          $l.warn(e);
          return $q.when({err: e});
        })

        .then((res) => {
          if (res.err) return $tr('API_CONSAMA.FAIL.LOGOUT')
            .then((t) => $mdT.showSimple(t));
          else return $state.current.name === 'dashboard' ?
            $state.reload('layout') : $state.go('dashboard');
        })

        .then((res) => {
          this.data.modules = [];
          this.data.loggued = false;
          return;
        });

    }

    stateLoadPre(resolveState) {

      this.data.apiConsamaIsOffline = (
        localStorage.getItem('app-consama-apiOffline') !== null &&
        localStorage.getItem('app-consama-user') === null
      );

      this.data.initialize = (
        localStorage.getItem('app-consama-apiOffline') === null &&
        localStorage.getItem('app-consama-user') !== null &&
        localStorage.getItem('app-consama-user') === 'INITIALIZE'
      );

      this.data.loggued = (
        localStorage.getItem('app-consama-apiOffline') === null &&
        localStorage.getItem('app-consama-user') !== null &&
        localStorage.getItem('app-consama-user') !== 'INITIALIZE' &&
        angular.isObject(JSON.parse(localStorage.getItem('app-consama-user')))
      );

      if (
        !angular.isString(resolveState) &&
        !angular.isFunction(resolveState)
      ) return $q((resolve, r) => r(`pass a function or name for the view`));

      let res = angular.isString(resolveState) ?
        this.requiForView(resolveState) :
        angular.isFunction(resolveState) ? resolveState(res) :
        {};

      if (this.data.apiConsamaIsOffline) {
        res = {
          toLoad: [getUrlFile('app', '.css'), 'layout.translate'],
          view: 'offline'
        };
        this.initLanguages(apiConsamaOffline);
        return $q.when(res);
      }else if (this.data.loggued) {
        this.initLanguages(this.data.dataUser.settings, true);
        return $q.when(res);
      }else if (this.data.initialize) {

        return S.public().$promise

          .then((s) => {
            this.data.initialize = true;
            let initializeInstance = new Initialize();
            this.data.dataUser = initializeInstance.dataUserInitialize(s);
            this.initLanguages(this.data.dataUser.settings);
            this.setModulesToUser(this.data.dataUser, res);
            return res;
          })

          .catch((e) => {
            throw new Error(e);
          });

      }else {//unauthenticate
        return S.public().$promise
          .then((s) => {
            this.data.dataUser = {
              settings: s
            };
            this.initLanguages(this.data.dataUser.settings);
            this.setModulesToUser(this.data.dataUser, res);
            return res;
          })
          .catch((e) => {
            throw new Error(e);
          });
      }

    }

    requiForView(viewName) {

      let res = {
        toLoad: [
          this.getUrlFile('app', '.css'),
          'layout.translate'
        ]
      };
      //console.log(res);

      if (viewName === 'dashboard') {

        //authenticate or initialize
        if (this.data.loggued || this.data.initialize) {
          res.view = 'dashboard';
          res.toLoad.push(
            'extras/masonry.pkgd.min.js',
            this.getUrlFile('dashboard'),
            this.getUrlFile('dashboard', '.css'),
            'dashboard.translate'
          );
        }else {
          res.view = 'login';
          res.toLoad.push(
            this.getUrlFile('login'),
            'login.translate'
          );
        }

      }
      return res;

    }

    initLanguages(s, loggued) {

      let langsAvaibles = s.langsAvaibles;

      $tr.fallbackLanguage(s.langFallback);
      $tr.preferredLanguage(s.preferredLanguage);

      $tr.use(loggued ?
        s.preferredLanguage : getLanguage(s.langsAvaibles));

      tmhDynamicLocale.set(loggued ?
        s.preferredLanguage : getLanguage(langsAvaibles));

      this.data.dataUser.settings = {
        langFallback: s.langFallback,
        langsAvaibles: s.langsAvaibles,
        currentLang: loggued ? s.preferredLanguage : getLanguage(langsAvaibles),
        preferredLanguage: loggued ?
          s.preferredLanguage : getLanguage(langsAvaibles)
      };

      this.ui.currentLanguage = loggued ?
        s.preferredLanguage : getLanguage(langsAvaibles);

    }

    stateLoad(resolveState) {

      return this.stateLoadPre(resolveState)

        .then((res) => this.loadFilesSetview(res))

        .then(() => this.postLoadState());
    }

    postLoadState() {

      return $tr.refresh()
        .then(() => this.clearInitialLoader())
        .catch(() => {
          console.log('can\'t refresh translates');
          return this.clearInitialLoader();
        });

    }

    isLoaded(src) {
      return this.ui.assetsLoaded.indexOf(src) === -1 ? false : true;
    }

    loadFilesSetview(res) {

      this.ui.viewTemplate = this.appTemplates(this.data.apiConsamaIsOffline ?
        'apiConsamaIsOffline' : res.view
      );

      this.ui.controllerName = this.data.apiConsamaIsOffline ?
        'ApiConsamaController' : `${res.view}Controller`;

      let files = [];

      for (let i = 0; i < res.toLoad.length; i++) ((file) => {

        if (
          !this.isLoaded(file) &&
          !/\.translate/.test(file) &&
          !/camion/.test(file)
        ) files.push(file);
        else if (
          !this.isLoaded(file) &&
          /\.translate/.test(file)
        ) $translatePartialLoader.addPart(file.slice(0, -10));

      })(res.toLoad[i]);

      return $oc.load(files)
        .then((data) => {
          for (let i = 0; i < files.length; i++) {
            this.ui.assetsLoaded.push(files[i]);
          }
          return data;
        })
        .catch((e) => e);

    }

    appTemplates(stateName) {
      let t = 'nothing';
      if (stateName === 'dashboard') t =
        require('../../dashboard/views/dashboard.jade')();
      else if (stateName === 'login') t =
        require('../../login/views/login.jade')();
      else if (stateName === 'apiConsamaIsOffline') t =
        require('../../offline/views/api-consama-offline.jade')();
      return t;
    }

    clearInitialLoader() {
      let spinner = $d[0].getElementById('spinner');
      if (!spinner.classList.contains('close')) spinner.classList.add('close');
    }

    sidenavChangeSize(idS, size = '') {
      this.ui.sidenav[idS].type = size;
    }

    sidenavAction(idS = 'right', toOpen = false, size = '', content = {}) {

      if (toOpen) this.sidenavChangeSize(idS, size);

      return $mdS(idS)[toOpen ? 'open' : 'close']()
        .then(() => {

          //sidenavRightContent(content.html, content.scope);

          if (toOpen) $d[0].body.style.overflow = 'hidden';
          else {
            this.sidenavChangeSize(idS);
            $d[0].body.style.overflow = 'visible';
            if (idS === 'right') {
              this.sidenavRightContent('');
              this.sidenavRightIconLeft = 'default';
              this.sidenavRightIconsRight = 'default';
            }
          }

        });

    }

    sidenavRightContent(html, scope) {
      this.ui.sidenav.right.content = {
        html: html,
        scope: scope
      };
      return this;
    }

    set sidenavRightTitle(title) {
      this.ui.sidenav.right.toolbar.title = title;
    }

    set sidenavRightTheme(theme) {
      this.ui.sidenav.right.theme = theme;
    }

    set sidenavRightIconsRight(icons) {
      this.ui.sidenav.right.toolbar.icons.right = angular.isArray(icons) ?
        icons : [];
    }

    set sidenavRightIconLeft(icon) {
      this.ui.sidenav.right.toolbar.icons.left = angular.isObject(icon) ?
        icon : {
          icon: 'close',
          click: this.sidenavAction.bind(this, 'right', false)
        };
    }

    setModulesToUser(dataUser, res) {

      let roles = dataUser.trabajador ? dataUser.trabajador.roles : [];

      dataUser.modules = this.formateModules(roles);

      angular.forEach(dataUser.modules, (module_) => {

        res.toLoad.push(
          this.getUrlFile(module_.name),
          `${this.getSectionName(module_.name)}.translate`
        );

      });

    }

    getSectionName(name) {
      return name === 'acl' ? 'role' :
        name === 'unidadMedida' ? 'unidad-medida' : name;
    }

    getUrlFile(name, ext = '.js') {
      return 'build/' + this.getSectionName(name) + '.bundle' + ext;
    }

    formateModules(rolesT) {

      let modules = [];
      let moduleTmp = {};
      let inRole = {};

      angular.forEach(rolesT, (role) => {

        angular.forEach(role.acls, (acl) => {

          moduleTmp = this.formateModule(acl.model, modules, true);
          inRole = yeFindBy(moduleTmp.inRoles, 'name', role.name);
          this.formateAcls(role, acl, moduleTmp, inRole);

        });

      });

      return modules;

    }

    formateModulesByRoles(rolesT, modules, aclsT) {

      let roles = [];

      angular.forEach(rolesT, (role) => {

        let roleTmp = angular.extend({modules: []}, role);

        angular.forEach(modules, (module_) => {

          let i = roleTmp.modules.push(this.formateModule(module_));

          angular.forEach(aclsT, (acl) => {

            this.formatAcls(role, acl, module_, roleTmp.modules[i - 1]);

          });

        });

        roles.push(roleTmp);

      });

      return roles;

    }

    formateM(moduleName) {
      return {
        name: moduleName,
        pluralName: this.getPluralName(moduleName),
        icon: this.getIconModule(moduleName),
        crud: {
          c: false,
          r: false,
          u: false,
          d: false
        }
      };
    }

    formateModule(moduleName, modules, orderByModules) {

      let moduleTmp;

      if (angular.isUndefined(modules)) moduleTmp = this.formateM(moduleName);
      else {
        let search = yeFindBy(modules, 'name', moduleName);

        if (search.index === -1) {

          let i = modules.push(this.formateM(moduleName));

          moduleTmp = modules[i - 1];

          if (orderByModules) {
            delete moduleTmp.acls;
            moduleTmp.inRoles = [];
            moduleTmp.crud.cAcls = [];
            moduleTmp.crud.rAcls = [];
            moduleTmp.crud.uAcls = [];
            moduleTmp.crud.dAcls = [];
          }

        }else moduleTmp = search.item;
      }

      return moduleTmp;

    }

    formateAcls(role, acl, moduleTmp, inRole) {

      if (inRole && inRole.index === -1) {
        //let inRole = angular.extend({}, role);
        //delete inRole.acls;
        moduleTmp.inRoles.push({role: role.name});
      }

      if (acl.principalType === 'ROLE' && acl.permission === 'ALLOW' &&
        acl.principalId === role.name && acl.model === moduleTmp.name) {

        if (acl.property === 'create') {
          moduleTmp.crud.c = true;
          moduleTmp.crud.cAcl = acl;
          if (inRole) moduleTmp.crud.cAcls.push(acl);
        }else if (acl.property === 'find') {
          moduleTmp.crud.r = true;
          moduleTmp.crud.rAcl = acl;
          if (inRole) moduleTmp.crud.rAcls.push(acl);
        }else if (acl.property === 'prototype_updateAttributes') {
          moduleTmp.crud.u = true;
          moduleTmp.crud.uAcl = acl;
          if (inRole) moduleTmp.crud.uAcls.push(acl);
        }else if (acl.property === 'deleteById') {

          moduleTmp.crud.d = true;
          moduleTmp.crud.dAcl = acl;
          if (inRole) moduleTmp.crud.dAcls.push(acl);
        }

      }

    }

    formatAcls(role, acl, moduleName, moduleSettings) {

      if (acl.principalType === 'ROLE' && acl.permission === 'ALLOW' &&
        acl.principalId === role.name && acl.model === moduleName) {

        if (acl.property === 'create') {
          moduleSettings.crud.c = true;
          moduleSettings.crud.cAcl = acl;
        }else if (acl.property === 'find') {
          moduleSettings.crud.r = true;
          moduleSettings.crud.rAcl = acl;
        }else if (acl.property === 'prototype_updateAttributes') {
          moduleSettings.crud.u = true;
          moduleSettings.crud.uAcl = acl;
        }else if (acl.property === 'deleteById') {
          moduleSettings.crud.d = true;
          moduleSettings.crud.dAcl = acl;
        }

      }

    }

    getPluralName(model) {
      let result = model;
      switch (model) {
        case 'acl': result = 'acls'; break;
        case 'agregado': result = 'agregados'; break;
        case 'area': result = 'areas'; break;
        case 'camion': result = 'camiones'; break;
        case 'compra': result = 'compras'; break;
        case 'cargo': result = 'cargos'; break;
        case 'empresa': result = 'empresas'; break;
        case 'flota': result = 'flotas'; break;
        case 'persona': result = 'personas'; break;
        case 'remuneracion': result = 'remuneraciones'; break;
        case 'role': result = 'roles'; break;
        case 'sucursal': result = 'sucursales'; break;
        case 'trabajador': result = 'trabajadores'; break;
        case 'unidadMedida': result = 'unidad-medidas'; break;
        case 'user': result = 'users'; break;
        case 'venta': result = 'ventas'; break;
      }
      return result;
    }

    getIconModule(module_) {
      let iconName = 'checkbox-blank-circle';
      switch (module_) {
        case 'acl': iconName = 'account-key'; break;
        case 'agregado': iconName = 'image-filter-hdr'; break;
        case 'empresa': iconName = 'factory'; break;
        case 'unidadMedida': iconName = 'scale'; break;
        case 'trabajador': iconName = 'human-male-female'; break;
        case 'user': iconName = 'account-multiple'; break;
        case 'role': iconName = 'clipboard-account'; break;
        case 'asistencia': iconName = 'calendar-today'; break;
        case 'caja': iconName = 'cash-multiple'; break;
        case 'flota': iconName = 'truck'; break;
      }
      return iconName;
    }

    createItem(scope, moduleList, evt, sidenavSize = '') {

      let iScope = scope.$new(true);
      iScope.options = moduleList.options;

      return $tr(`MODEL.${moduleList.module.name.toUpperCase()}`)

        .then(function(t) {
          return $tr('ITEM.NEW', {moduleName: t});
        })

        .then((t) => {

          this.sidenavRightTitle = t;
          this.sidenavRightTheme = moduleList.module.name;
          this.sidenavRightIconLeft = 'default';
          this.sidenavRightIconsRight = 'default';
          this.sidenavRightContent(
            `<${moduleList.module.name}-form
              item="item" options="options" module-form
            />`,
            iScope
          );

          return this.sidenavAction('right', true, sidenavSize);
        });

    }

    updateItem(scope, moduleList, evt, item, items, fromSN = false,
      noMdxLi = false, itemName = 'name', sidenavSize = ''
    ) {

      let itemS = noMdxLi ? item : yeFindBy(items.original, 'id', item.id)
        .item;
      let toolbarTitle = noMdxLi ? item[itemName] || 'item' : item.title;

      $tr('ITEM.NAME', {moduleItem: toolbarTitle})
        .then((t) => {

          let iScope = scope.$new(true);

          iScope.item = angular.extend({}, itemS);
          iScope.options = angular.extend({}, moduleList.options);

          this.sidenavRightTitle = t;
          this.sidenavRightTheme = moduleList.module.name;
          this.sidenavRightIconsRight = 'default';
          this.sidenavRightIconLeft = fromSN ? {
            icon: 'arrow-left',
            click: function(e) {
              moduleList.showItem(e, item, items);
            }
          } : 'default';
          //console.log(this.ui);
          this.sidenavRightContent(
            `<${moduleList.module.name}-form
              item="item" options="options" module-form
            />`,
            iScope
          );

          return this.sidenavAction('right', true, sidenavSize);

        });

    }

    deleteItem(scope, moduleList, evt, item, items, exeDelete,
      nonMdxList, itemName
    ) {

      let itemS = nonMdxList ?
        item : yeFindBy(items.original, 'id', item.id).item;

      let dialogTitle =  nonMdxList ? item[itemName] : item.title;

      return $tr([
        'DIALOG_DELETE.TITLE',
        'DIALOG_DELETE.CONTENT',
        'CANCEL',
        'DELETE',
        'ITEM.DELETED_MODULE',
        'ITEM.DELETED_MODULE_FAIL',
        'MODEL.' + moduleList.module.name.toUpperCase()
      ], {moduleItem: dialogTitle || 'item'})
        .then((translations) => {

          let confirm = $mdD.confirm()
            .parent(angular.element($d[0].body))
            .title(translations['DIALOG_DELETE.TITLE'].toUpperCase())
            .content(capitalizeFilter(translations['DIALOG_DELETE.CONTENT']))
            .ariaLabel(translations.delete +
              translations['MODEL.' + moduleList.module.name.toUpperCase()])
            .ok(translations.DELETE)
            .cancel(translations.CANCEL)
            .theme(moduleList.module.name)
            .targetEvent(evt);

          return $mdD.show(confirm).then(() => {return translations;});
        })

        .then((t) => {
          if (angular.isUndefined(exeDelete)) {
            let err = new Error('there is not a logic for delete item');
            err.translations = t;
            err.resServer = null;
            throw err;
          }else return exeDelete(itemS).then(function(resServer) {
            return {resServer: resServer, translations: t};
          }, function(errServer) {
            return {resServer: errServer, translations: t};
          });
        })

        .then((data) => {
          return this.sidenavAction('right', false).then(() => {
            return $mdT.showSimple(data.translations['ITEM.DELETED_MODULE']);
          });
        })

        .catch((err) => {
          return err.translations ?
            $mdT.showSimple(err.translations['ITEM.DELETED_MODULE_FAIL']) :
            err;
        });

    }

    showItem(scope, moduleList, evt, item, items, noMdxLi, itemName) {

      let itemS = noMdxLi ? item : yeFindBy(items.original, 'id', item.id).item;
      let optionsS = moduleList.options;
      let toolbarTitle = noMdxLi ? item[itemName] : item.title;

      return $tr('ITEM.NAME', {moduleItem: toolbarTitle || 'item'})

        .then((translation) => {

          let iScope = scope.$new(true);

          iScope.item = itemS;
          iScope.options = optionsS;

          let iconsRight = [];

          if (moduleList.module.crud.u) iconsRight.push({
            icon: 'pencil',
            click: (e) => {
              moduleList.updateItem(e, item, items, true);
            }
          });

          if (moduleList.module.crud.d) iconsRight.push({
            icon: 'delete',
            click: (e) => {
              moduleList.deleteItem(e, item, items);
            }
          });

          this.sidenavRightTitle = translation;
          this.sidenavRightIconsRight = iconsRight;
          this.sidenavRightIconLeft = 'default';
          this.sidenavRightTheme = moduleList.module.name;
          this.sidenavRightContent('<' + moduleList.module.name +
            '-show item="item" data-options="options"/>', iScope);

          return this.sidenavAction('right', true);
        });

    }

    formUpsert(Model, form, ctrlForm, remoteMethod) {

      ctrlForm.showSpinner = true;

      form.$setDirty();

      let t1;

      let update = angular.isDefined(ctrlForm.form.id) ? true : false;

      return yeValidForm(form)

        .then((result) => {

          return Model[angular.isString(remoteMethod) ?
            remoteMethod :
            update ?
            'prototype$updateAttributes' : 'create'](update ?
            {id: ctrlForm.form.id} : {}, ctrlForm.form).$promise

              .then((instance) => {

                return $tr('FORM.' + (update ? 'UPDATE' : 'NEW') + '_SUCCESS')
                  .then((t) => $q.when({msg: t, instance: instance}));

              })
              .catch((err) => apiConsama.handlerMessages(err));

        })

        .catch((err) => {
          return err.errorOne.message.then((errMessage) => {
            return $q.when({msg: errMessage, details: err});
          });
        })

        .then((res) => {

          return $q.all([
            res && res.instance ? this.sidenavAction('right', false) : $q.when()
          ])
            .then(() => {
              t1 = $t(() => {
                ctrlForm.showSpinner = false;
                $mdT.hide();
              }, 3000);
              return $mdT.showSimple(res.msg);
            });

        })

        .then(() => {
          if (t1) $t.cancel(t1);
          return;
        });

    }

    listPattern(module_) {

      let formatedList = {
        list: {
          title: 'name',
          description: 'description'
        },
        table: ['name', 'description']
      };

      switch (module_) {
        case 'acl':
          formatedList = {
            list: {
              title: 'role'
            },
            table: ['role']
          };
          break;
        case 'unidadMedida':
          formatedList = {
            list: {
              letter: 'suffix',
              title: 'name',
              description: 'description'
            },
            table: ['name', 'suffix', 'description']
          };
          break;
        case 'agregado':
          formatedList = {
            list: {
              photo: 'photo',
              title: 'name',
              description: 'description'
            },
            table: ['name', 'description']
          };
          break;
        case 'camion':
          formatedList = {
            list: {
              photo: 'photo',
              title: 'aka',
              description: 'licensePlate'
            },
            table: ['aka', 'licensePlate']
          };
          break;
        case 'flota':
          formatedList = {
            list: {
              icon(item) {
                if (item.type === 'camion') return 'truck';
                else if (item.type === 'maquinaria') return 'train';
                else if (item.type === 'carro') return 'car';
                else if (item.type === 'moto') return 'motorbike';
                else return 'checkbox-blank-circle';
              },
              title: 'type',
            },
            table: ['type']
          };
          break;
        case 'remuneracion':
          formatedList = {
            list: {
              payType: 'payType',
              wages: 'wages'
            },
            table: ['payType', 'wages']
          };
          break;
        case 'empresa':
          formatedList = {
            list: {
              photo: 'photo',
              title: 'comercialName',
              description: function(item) {
                return item.socialName + ' - ' + item.ruc;
              }
            },
            table: ['comercialName', 'socialName', 'ruc']
          };
          break;
        case 'user':
          formatedList = {
            list: {
              title: 'username',
              description: 'email'
            },
            table: ['username', 'email']
          };
          break;
        case 'trabajador':
          formatedList = {
            list: {
              photo: 'persona.photo',
              title: function(item) {
                return item.persona.firstName + ' ' +
                  (item.persona.middleName || '') + ' ' + item.persona.lastName;
              },
              description: function(item) {
                let res = '';
                for (let i = 0; i < item.roles.length; i++) {
                  res += item.roles[i].name + ', ';
                }
                res = res.substring(0, res.lastIndexOf(','));
                return res;
              }
            },
            table: ['persona.photo', 'persona.firstName', 'persona.lastName']
          };
          break;
      }

      return formatedList;

    }

    listFormate(items, pattern) {
      return {
        original: items,
        list: listFormateType(items, pattern.list),
        table: listFormateType(items, pattern.table)
      };

      function listFormateType(rows, formatePattern) {

        var items = [];
        //console.log(rows.length);
        angular.forEach(rows, function(row) {
          if (angular.isArray(formatePattern)) {
            items.push(parseArray(row, formatePattern));
          }else if (angular.isObject(formatePattern)) {
            items.push(parseObject(row, formatePattern));
          }else console.warn('pattern is not array or object',
            formatePattern, rows);
        });
        return items;

      }

      function parseObject(row, formatePattern) {

        var item = {};

        item.id = row.id;

        angular.forEach(formatePattern, function(vPattern, fPattern) {

          if (angular.isFunction(vPattern)) item[fPattern] = vPattern(row);
          else if (vPattern.indexOf('.') !== -1) {
            var resultEmbedModel = '';
            try {
              resultEmbedModel = eval('row.' + vPattern);
            }catch (err) {
              resultEmbedModel = '';
            }
            item[fPattern] = resultEmbedModel;

          }else item[fPattern] = row[vPattern];

        });

        return item;

      }

      function parseArray(row, formatePattern) {

        var item = {};
        item.id = row.id;

        angular.forEach(formatePattern, function(vPattern) {
          if (angular.isFunction(vPattern)) item[vPattern] = vPattern(row);
          else if (vPattern.indexOf('.') !== -1) {
            var resultEmbedModel = '';
            try {
              resultEmbedModel = eval('row.' + vPattern);
            }catch (err) {
              resultEmbedModel = '';
            }
            var fieldName = vPattern.substring(vPattern.lastIndexOf('.') + 1);
            item[fieldName] = resultEmbedModel;
          }else item[vPattern] = row[vPattern];
        });
        return item;
      }

    }

    apiConsamaImg(scope, item, container, photoProp = 'photo',
      typePhoto = 'standard'
    ) {

      item.photoImage = require('../assets/images/default-image.jpg');

      if (angular.isUndefined(item[photoProp])) return;

      if (container === 'trabajadores') container = 'personas';

      let url = apiConsama.getPhoto(container, item[photoProp], typePhoto);

      $http({
        url: url,
        method: 'GET',
        cache: false,
        responseType: 'blob'
      })
        .then(function(res) {
          var reader = new FileReader();
          reader.onload = function() {
            scope.$apply(function() {
              item.photoImage = reader.result;
            });
          };
          reader.readAsDataURL(res.data);
        })
        .catch((e) => {
          console.warn(e);
        });

    };

    getDate(age, min) {

      let today = new Date(Date.now());

      if (age === 0) return today.getFullYear() + '-' +
        fixMonth(today.getMonth() + 1) + '-' + fixDay(today.getDate());
      else if (angular.isString(age)) {
        let sp = age.split('-');
        return sp[0] + '-' + fixMonth((parseInt(sp[1]) - 1)) + '-' +
          fixDay(parseInt(sp[2]));
      }

      min = min || false;

      return (today.getFullYear() - age) + '-' + (min ? '01-01' : '12-31');

      function fixMonth(month) {
        return month < 10 ? '0' + month : month;
      }
      function fixDay(day) {
        return day < 10 ? '0' + day : day;
      }
    }

    getFormInitial(scope, element, ctrlForm, belongsTo = [],
      moduleName = 'moduleXX', coverOps
    ) {

      return $q((resolve, reject) => {

        ctrlForm.showSpinner = true;

        if (
          angular.isObject(coverOps)
        ) ctrlForm.cover = apiConsama.getPhoto(
            coverOps.container || this.getPluralName(moduleName),
            angular.isDefined(ctrlForm.item) &&
            coverOps.photo.indexOf('.') === -1 ?
              ctrlForm.item[coverOps.photo] :
            angular.isDefined(ctrlForm.item) &&
            coverOps.photo.indexOf('.') !== -1 ?
              (() => {
                let res = 'default.jpg';
                try {
                  res = eval(`${ctrlForm.item}.${coverOps.photo}`);
                } catch (e) {
                  console.warn(e);
                };
                console.log(res);
                return res;
              })() :
              'default.jpg',
            'cover');

        if (angular.isObject(ctrlForm.item)) {
          ctrlForm.showSpinner = false;
          resolve(ctrlForm.item);
        }else if (belongsTo.length > 0 &&
          angular.isUndefined(ctrlForm.options)) {
          reject(`${moduleName} has no options with parents modules set`);
        }else {

          let res = {};
          let fail = false;

          angular.forEach(belongsTo, (mName) => {
            if (
              angular.isUndefined(ctrlForm.options[mName])
            ) fail = `${moduleName} has no options with ${mName} module set`;
            else if (
              angular.isUndefined(ctrlForm.options[mName].id)
            ) fail = `${moduleName} has no id of ${mName} set`;
            else res[mName + 'Id'] = ctrlForm.options[mName].id;
          });

          if (angular.isString(fail)) reject(fail);
          else {
            ctrlForm.showSpinner = false;
            resolve(res);
          }
        }
      });

    }

    getConsamaPhoto(modelPluralName, photoName, type = 'cover') {
      return apiConsama.getPhoto(modelPluralName, photoName, type);
    }

  };

  return new Layout();

}

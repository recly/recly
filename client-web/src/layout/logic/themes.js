'use strict';
/*
red -
purple -
light-blue -
green -
grey -
brown -
indigo -
deep-purple -
light-green -
teal -
cyan -
orange -
blue-grey -
amber -
deep-orange -
lime -
yellow-
pink
blue
*/
module.exports = themes;

function themes(mdThemingProvider) {

  mdThemingProvider
    .theme('default')
      .primaryPalette('green')
      .accentPalette('indigo')
      .backgroundPalette('grey');

  mdThemingProvider
    .theme('acl')
      .primaryPalette('pink');

  mdThemingProvider
    .theme('agregado')
      .primaryPalette('brown');

  mdThemingProvider
    .theme('asistencia')
      .primaryPalette('deep-purple');

  mdThemingProvider
    .theme('area')
      .primaryPalette('orange');

  mdThemingProvider
    .theme('caja')
      .primaryPalette('light-green');

  mdThemingProvider
    .theme('compra')
      .primaryPalette('purple');

  mdThemingProvider
    .theme('empresa')
      .primaryPalette('light-blue');

  mdThemingProvider
    .theme('flota')
      .primaryPalette('orange');

  mdThemingProvider
    .theme('camion')
      .primaryPalette('orange');

  mdThemingProvider
    .theme('maquinaria')
      .primaryPalette('orange');

  mdThemingProvider
    .theme('inventario')
      .primaryPalette('blue-grey');

  mdThemingProvider
    .theme('persona')
      .primaryPalette('deep-orange');

  mdThemingProvider
    .theme('remuneracion')
      .primaryPalette('teal');

  mdThemingProvider
    .theme('role')
      .primaryPalette('pink');

  mdThemingProvider
    .theme('settings')
      .primaryPalette('blue');

  mdThemingProvider
    .theme('sucursal')
      .primaryPalette('purple');

  mdThemingProvider
    .theme('trabajador')
      .primaryPalette('purple');

  mdThemingProvider
    .theme('unidadMedida')
      .primaryPalette('teal');

  mdThemingProvider
    .theme('user')
      .primaryPalette('amber');

  mdThemingProvider
    .theme('venta')
      .primaryPalette('cyan');

  mdThemingProvider
    .theme('cargo')
      .primaryPalette('cyan');

  mdThemingProvider
    .theme('cliente')
      .primaryPalette('cyan');

}

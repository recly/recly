'use strict';

module.exports = config;

function config($translateProvider, $translatePartialLoaderProvider,
  tmhDLP, $mdThemingProvider, LoopBackResourceProvider) {

  var apiUrl = 'http://localhost:3000/api/v1';
  var ls = localStorage;

  tmhDLP.localeLocationPattern('/i18n/ngLocale/angular-locale_{{locale}}.js');

  $translateProvider
    .addInterpolation('$translateMessageFormatInterpolation')
    .useSanitizeValueStrategy('escaped')
    .useLoader('$translatePartialLoader', {
      urlTemplate: ls.getItem('app-consama-apiOffline') === null ?
        apiUrl + '/settings/translate?part={part}&lang={lang}' :
        '/i18n/{part}/{part}-{lang}.json'
    });

  require('./logic/themes')($mdThemingProvider);
}

config.$inject = ['$translateProvider', '$translatePartialLoaderProvider',
  'tmhDynamicLocaleProvider', '$mdThemingProvider', 'LoopBackResourceProvider'];

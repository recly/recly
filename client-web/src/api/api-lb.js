(function(window, angular, undefined) {'use strict';

var urlBase = "/api/v1";
var authHeader = 'authorization';

/**
 * @ngdoc overview
 * @name apiLb
 * @module
 * @description
 *
 * The `apiLb` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
var module = angular.module("apiLb", ['ngResource']);

/**
 * @ngdoc object
 * @name apiLb.User
 * @header apiLb.User
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `User` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "User",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/users/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use User.settings() instead.
        "prototype$__get__settings": {
          url: urlBase + "/users/:id/settings",
          method: "GET"
        },

        // INTERNAL. Use User.settings.create() instead.
        "prototype$__create__settings": {
          url: urlBase + "/users/:id/settings",
          method: "POST"
        },

        // INTERNAL. Use User.settings.update() instead.
        "prototype$__update__settings": {
          url: urlBase + "/users/:id/settings",
          method: "PUT"
        },

        // INTERNAL. Use User.settings.destroy() instead.
        "prototype$__destroy__settings": {
          url: urlBase + "/users/:id/settings",
          method: "DELETE"
        },

        // INTERNAL. Use User.accessTokens.findById() instead.
        "prototype$__findById__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "GET"
        },

        // INTERNAL. Use User.accessTokens.destroyById() instead.
        "prototype$__destroyById__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use User.accessTokens.updateById() instead.
        "prototype$__updateById__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "PUT"
        },

        // INTERNAL. Use User.roles.findById() instead.
        "prototype$__findById__roles": {
          url: urlBase + "/users/:id/roles/:fk",
          method: "GET"
        },

        // INTERNAL. Use User.roles.destroyById() instead.
        "prototype$__destroyById__roles": {
          url: urlBase + "/users/:id/roles/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use User.roles.updateById() instead.
        "prototype$__updateById__roles": {
          url: urlBase + "/users/:id/roles/:fk",
          method: "PUT"
        },

        // INTERNAL. Use User.roles.link() instead.
        "prototype$__link__roles": {
          url: urlBase + "/users/:id/roles/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use User.roles.unlink() instead.
        "prototype$__unlink__roles": {
          url: urlBase + "/users/:id/roles/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use User.roles.exists() instead.
        "prototype$__exists__roles": {
          url: urlBase + "/users/:id/roles/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use User.trabajador() instead.
        "prototype$__get__trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "GET"
        },

        // INTERNAL. Use User.trabajador.create() instead.
        "prototype$__create__trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "POST"
        },

        // INTERNAL. Use User.trabajador.update() instead.
        "prototype$__update__trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "PUT"
        },

        // INTERNAL. Use User.trabajador.destroy() instead.
        "prototype$__destroy__trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "DELETE"
        },

        // INTERNAL. Use User.accessTokens() instead.
        "prototype$__get__accessTokens": {
          isArray: true,
          url: urlBase + "/users/:id/accessTokens",
          method: "GET"
        },

        // INTERNAL. Use User.accessTokens.create() instead.
        "prototype$__create__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "POST"
        },

        // INTERNAL. Use User.accessTokens.destroyAll() instead.
        "prototype$__delete__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "DELETE"
        },

        // INTERNAL. Use User.accessTokens.count() instead.
        "prototype$__count__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/count",
          method: "GET"
        },

        // INTERNAL. Use User.roles() instead.
        "prototype$__get__roles": {
          isArray: true,
          url: urlBase + "/users/:id/roles",
          method: "GET"
        },

        // INTERNAL. Use User.roles.create() instead.
        "prototype$__create__roles": {
          url: urlBase + "/users/:id/roles",
          method: "POST"
        },

        // INTERNAL. Use User.roles.destroyAll() instead.
        "prototype$__delete__roles": {
          url: urlBase + "/users/:id/roles",
          method: "DELETE"
        },

        // INTERNAL. Use User.roles.count() instead.
        "prototype$__count__roles": {
          url: urlBase + "/users/:id/roles/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#create
         * @methodOf apiLb.User
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/users",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#upsert
         * @methodOf apiLb.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/users",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#exists
         * @methodOf apiLb.User
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/users/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#findById
         * @methodOf apiLb.User
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/users/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#find
         * @methodOf apiLb.User
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/users",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#findOne
         * @methodOf apiLb.User
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/users/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#updateAll
         * @methodOf apiLb.User
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/users/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#deleteById
         * @methodOf apiLb.User
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/users/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#count
         * @methodOf apiLb.User
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/users/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#prototype$updateAttributes
         * @methodOf apiLb.User
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/users/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#createChangeStream
         * @methodOf apiLb.User
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/users/change-stream",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#login
         * @methodOf apiLb.User
         *
         * @description
         *
         * Login a user with username/email and password.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
         *   Default value: `user`.
         *
         *  - `rememberMe` - `boolean` - Whether the authentication credentials
         *     should be remembered in localStorage across app/browser restarts.
         *     Default: `true`.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * The response body contains properties of the AccessToken created on login.
         * Depending on the value of `include` parameter, the body may contain additional properties:
         * 
         *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
         * 
         *
         */
        "login": {
          params: {
            include: "user"
          },
          interceptor: {
            response: function(response) {
              var accessToken = response.data;
              LoopBackAuth.setUser(accessToken.id, accessToken.userId, accessToken.user);
              LoopBackAuth.rememberMe = response.config.params.rememberMe !== false;
              LoopBackAuth.save();
              return response.resource;
            }
          },
          url: urlBase + "/users/login",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#logout
         * @methodOf apiLb.User
         *
         * @description
         *
         * Logout a user with access token
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "logout": {
          interceptor: {
            response: function(response) {
              LoopBackAuth.clearUser();
              LoopBackAuth.clearStorage();
              return response.resource;
            }
          },
          url: urlBase + "/users/logout",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#confirm
         * @methodOf apiLb.User
         *
         * @description
         *
         * Confirm a user registration with email verification token
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `uid` – `{string}` - 
         *
         *  - `token` – `{string}` - 
         *
         *  - `redirect` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "confirm": {
          url: urlBase + "/users/confirm",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#resetPassword
         * @methodOf apiLb.User
         *
         * @description
         *
         * Reset password for a user with email
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "resetPassword": {
          url: urlBase + "/users/reset",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#checkinitiaize
         * @methodOf apiLb.User
         *
         * @description
         *
         * check if the app has the initialize user
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "checkinitiaize": {
          url: urlBase + "/users/checkinitiaize",
          method: "GET"
        },

        // INTERNAL. Use Settings.user() instead.
        "::get::settings::user": {
          url: urlBase + "/settings/:id/user",
          method: "GET"
        },

        // INTERNAL. Use AccessToken.user() instead.
        "::get::accessToken::user": {
          url: urlBase + "/accessTokens/:id/user",
          method: "GET"
        },

        // INTERNAL. Use Role.users.findById() instead.
        "::findById::role::users": {
          url: urlBase + "/roles/:id/users/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.users.destroyById() instead.
        "::destroyById::role::users": {
          url: urlBase + "/roles/:id/users/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.users.updateById() instead.
        "::updateById::role::users": {
          url: urlBase + "/roles/:id/users/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.users.link() instead.
        "::link::role::users": {
          url: urlBase + "/roles/:id/users/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.users.unlink() instead.
        "::unlink::role::users": {
          url: urlBase + "/roles/:id/users/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.users.exists() instead.
        "::exists::role::users": {
          url: urlBase + "/roles/:id/users/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Role.users() instead.
        "::get::role::users": {
          isArray: true,
          url: urlBase + "/roles/:id/users",
          method: "GET"
        },

        // INTERNAL. Use Role.users.create() instead.
        "::create::role::users": {
          url: urlBase + "/roles/:id/users",
          method: "POST"
        },

        // INTERNAL. Use Role.users.destroyAll() instead.
        "::delete::role::users": {
          url: urlBase + "/roles/:id/users",
          method: "DELETE"
        },

        // INTERNAL. Use Role.users.count() instead.
        "::count::role::users": {
          url: urlBase + "/roles/:id/users/count",
          method: "GET"
        },

        // INTERNAL. Use RoleMapping.user() instead.
        "::get::roleMapping::user": {
          url: urlBase + "/roleMappings/:id/user",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.user() instead.
        "::get::trabajador::user": {
          url: urlBase + "/trabajadores/:id/user",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.User#getCurrent
         * @methodOf apiLb.User
         *
         * @description
         *
         * Get data of the currently logged user. Fail with HTTP result 401
         * when there is no user logged in.
         *
         * @param {function(Object,Object)=} successCb
         *    Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *    `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         */
        "getCurrent": {
           url: urlBase + "/users" + "/:id",
           method: "GET",
           params: {
             id: function() {
              var id = LoopBackAuth.currentUserId;
              if (id == null) id = '__anonymous__';
              return id;
            },
          },
          interceptor: {
            response: function(response) {
              LoopBackAuth.currentUserData = response.data;
              return response.resource;
            }
          },
          __isGetCurrentUser__ : true
        }
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.User#updateOrCreate
         * @methodOf apiLb.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.User#update
         * @methodOf apiLb.User
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.User#destroyById
         * @methodOf apiLb.User
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.User#removeById
         * @methodOf apiLb.User
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.User#getCachedCurrent
         * @methodOf apiLb.User
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link apiLb.User#login} or
         * {@link apiLb.User#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A User instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name apiLb.User#isAuthenticated
         * @methodOf apiLb.User
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name apiLb.User#getCurrentId
         * @methodOf apiLb.User
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

    /**
    * @ngdoc property
    * @name apiLb.User#modelName
    * @propertyOf apiLb.User
    * @description
    * The name of the model represented by this $resource,
    * i.e. `User`.
    */
    R.modelName = "User";

    /**
     * @ngdoc object
     * @name lbServices.User.settings
     * @header lbServices.User.settings
     * @object
     * @description
     *
     * The object `User.settings` groups methods
     * manipulating `Settings` instances related to `User`.
     *
     * Call {@link lbServices.User#settings User.settings()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.User#settings
         * @methodOf apiLb.User
         *
         * @description
         *
         * Fetches hasOne relation settings.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        R.settings = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::get::user::settings"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.settings#create
         * @methodOf apiLb.User.settings
         *
         * @description
         *
         * Creates a new instance in settings of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        R.settings.create = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::create::user::settings"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.settings#destroy
         * @methodOf apiLb.User.settings
         *
         * @description
         *
         * Deletes settings of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.settings.destroy = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::destroy::user::settings"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.settings#update
         * @methodOf apiLb.User.settings
         *
         * @description
         *
         * Update settings of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        R.settings.update = function() {
          var TargetResource = $injector.get("Settings");
          var action = TargetResource["::update::user::settings"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.User.accessTokens
     * @header lbServices.User.accessTokens
     * @object
     * @description
     *
     * The object `User.accessTokens` groups methods
     * manipulating `AccessToken` instances related to `User`.
     *
     * Call {@link lbServices.User#accessTokens User.accessTokens()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.User#accessTokens
         * @methodOf apiLb.User
         *
         * @description
         *
         * Queries accessTokens of user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::get::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.accessTokens#count
         * @methodOf apiLb.User.accessTokens
         *
         * @description
         *
         * Counts accessTokens of user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.accessTokens.count = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::count::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.accessTokens#create
         * @methodOf apiLb.User.accessTokens
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens.create = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::create::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.accessTokens#destroyAll
         * @methodOf apiLb.User.accessTokens
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.accessTokens.destroyAll = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::delete::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.accessTokens#destroyById
         * @methodOf apiLb.User.accessTokens
         *
         * @description
         *
         * Delete a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.accessTokens.destroyById = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::destroyById::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.accessTokens#findById
         * @methodOf apiLb.User.accessTokens
         *
         * @description
         *
         * Find a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens.findById = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::findById::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.accessTokens#updateById
         * @methodOf apiLb.User.accessTokens
         *
         * @description
         *
         * Update a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens.updateById = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::updateById::user::accessTokens"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.User.roles
     * @header lbServices.User.roles
     * @object
     * @description
     *
     * The object `User.roles` groups methods
     * manipulating `Role` instances related to `User`.
     *
     * Call {@link lbServices.User#roles User.roles()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.User#roles
         * @methodOf apiLb.User
         *
         * @description
         *
         * Queries roles of user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::get::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#count
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Counts roles of user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.roles.count = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::count::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#create
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Creates a new instance in roles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.create = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::create::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#destroyAll
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Deletes all roles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.roles.destroyAll = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::delete::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#destroyById
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Delete a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.roles.destroyById = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::destroyById::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#exists
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Check the existence of roles relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.exists = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::exists::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#findById
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Find a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.findById = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::findById::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#link
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Add a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.link = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::link::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#unlink
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Remove the roles relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.roles.unlink = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::unlink::user::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.roles#updateById
         * @methodOf apiLb.User.roles
         *
         * @description
         *
         * Update a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.updateById = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::updateById::user::roles"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.User.trabajador
     * @header lbServices.User.trabajador
     * @object
     * @description
     *
     * The object `User.trabajador` groups methods
     * manipulating `Trabajador` instances related to `User`.
     *
     * Call {@link lbServices.User#trabajador User.trabajador()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.User#trabajador
         * @methodOf apiLb.User
         *
         * @description
         *
         * Fetches hasOne relation trabajador.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajador = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::get::user::trabajador"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.trabajador#create
         * @methodOf apiLb.User.trabajador
         *
         * @description
         *
         * Creates a new instance in trabajador of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajador.create = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::create::user::trabajador"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.trabajador#destroy
         * @methodOf apiLb.User.trabajador
         *
         * @description
         *
         * Deletes trabajador of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajador.destroy = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::destroy::user::trabajador"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.User.trabajador#update
         * @methodOf apiLb.User.trabajador
         *
         * @description
         *
         * Update trabajador of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajador.update = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::update::user::trabajador"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Settings
 * @header apiLb.Settings
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Settings` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Settings",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/settings/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Settings.user() instead.
        "prototype$__get__user": {
          url: urlBase + "/settings/:id/user",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#create
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/settings",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#upsert
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/settings",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#exists
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/settings/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#findById
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/settings/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#find
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/settings",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#findOne
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/settings/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#updateAll
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/settings/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#deleteById
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/settings/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#count
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/settings/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#prototype$updateAttributes
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/settings/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#createChangeStream
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/settings/change-stream",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#public
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * get public info
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "public": {
          url: urlBase + "/settings/public",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#modules
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * get modules for consama app
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "modules": {
          isArray: true,
          url: urlBase + "/settings/modules",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Settings#translate
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * get translate for determined section
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `part` – `{string}` - 
         *
         *  - `lang` – `{string}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        "translate": {
          url: urlBase + "/settings/translate",
          method: "GET"
        },

        // INTERNAL. Use User.settings() instead.
        "::get::user::settings": {
          url: urlBase + "/users/:id/settings",
          method: "GET"
        },

        // INTERNAL. Use User.settings.create() instead.
        "::create::user::settings": {
          url: urlBase + "/users/:id/settings",
          method: "POST"
        },

        // INTERNAL. Use User.settings.update() instead.
        "::update::user::settings": {
          url: urlBase + "/users/:id/settings",
          method: "PUT"
        },

        // INTERNAL. Use User.settings.destroy() instead.
        "::destroy::user::settings": {
          url: urlBase + "/users/:id/settings",
          method: "DELETE"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Settings#updateOrCreate
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Settings` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Settings#update
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Settings#destroyById
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Settings#removeById
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Settings#modelName
    * @propertyOf apiLb.Settings
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Settings`.
    */
    R.modelName = "Settings";


        /**
         * @ngdoc method
         * @name apiLb.Settings#user
         * @methodOf apiLb.Settings
         *
         * @description
         *
         * Fetches belongsTo relation user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.user = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::settings::user"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Agregado
 * @header apiLb.Agregado
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Agregado` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Agregado",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/agregados/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Agregado.agregadoAsignaciones.findById() instead.
        "prototype$__findById__agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/:fk",
          method: "GET"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.destroyById() instead.
        "prototype$__destroyById__agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.updateById() instead.
        "prototype$__updateById__agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones() instead.
        "prototype$__get__agregadoAsignaciones": {
          isArray: true,
          url: urlBase + "/agregados/:id/agregadoAsignaciones",
          method: "GET"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.create() instead.
        "prototype$__create__agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones",
          method: "POST"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.destroyAll() instead.
        "prototype$__delete__agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones",
          method: "DELETE"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.count() instead.
        "prototype$__count__agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#create
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/agregados",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#upsert
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/agregados",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#exists
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/agregados/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#findById
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/agregados/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#find
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/agregados",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#findOne
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/agregados/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#updateAll
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/agregados/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#deleteById
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/agregados/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#count
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/agregados/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#prototype$updateAttributes
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/agregados/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Agregado#createChangeStream
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/agregados/change-stream",
          method: "POST"
        },

        // INTERNAL. Use AgregadoAsignacion.agregado() instead.
        "::get::agregadoAsignacion::agregado": {
          url: urlBase + "/agregado-asignacion/:id/agregado",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Agregado#updateOrCreate
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Agregado#update
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Agregado#destroyById
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Agregado#removeById
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Agregado#modelName
    * @propertyOf apiLb.Agregado
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Agregado`.
    */
    R.modelName = "Agregado";

    /**
     * @ngdoc object
     * @name lbServices.Agregado.agregadoAsignaciones
     * @header lbServices.Agregado.agregadoAsignaciones
     * @object
     * @description
     *
     * The object `Agregado.agregadoAsignaciones` groups methods
     * manipulating `AgregadoAsignacion` instances related to `Agregado`.
     *
     * Call {@link lbServices.Agregado#agregadoAsignaciones Agregado.agregadoAsignaciones()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Agregado#agregadoAsignaciones
         * @methodOf apiLb.Agregado
         *
         * @description
         *
         * Queries agregadoAsignaciones of agregado.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        R.agregadoAsignaciones = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::get::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Agregado.agregadoAsignaciones#count
         * @methodOf apiLb.Agregado.agregadoAsignaciones
         *
         * @description
         *
         * Counts agregadoAsignaciones of agregado.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.agregadoAsignaciones.count = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::count::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Agregado.agregadoAsignaciones#create
         * @methodOf apiLb.Agregado.agregadoAsignaciones
         *
         * @description
         *
         * Creates a new instance in agregadoAsignaciones of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        R.agregadoAsignaciones.create = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::create::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Agregado.agregadoAsignaciones#destroyAll
         * @methodOf apiLb.Agregado.agregadoAsignaciones
         *
         * @description
         *
         * Deletes all agregadoAsignaciones of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.agregadoAsignaciones.destroyAll = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::delete::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Agregado.agregadoAsignaciones#destroyById
         * @methodOf apiLb.Agregado.agregadoAsignaciones
         *
         * @description
         *
         * Delete a related item by id for agregadoAsignaciones.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for agregadoAsignaciones
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.agregadoAsignaciones.destroyById = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::destroyById::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Agregado.agregadoAsignaciones#findById
         * @methodOf apiLb.Agregado.agregadoAsignaciones
         *
         * @description
         *
         * Find a related item by id for agregadoAsignaciones.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for agregadoAsignaciones
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        R.agregadoAsignaciones.findById = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::findById::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Agregado.agregadoAsignaciones#updateById
         * @methodOf apiLb.Agregado.agregadoAsignaciones
         *
         * @description
         *
         * Update a related item by id for agregadoAsignaciones.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for agregadoAsignaciones
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        R.agregadoAsignaciones.updateById = function() {
          var TargetResource = $injector.get("AgregadoAsignacion");
          var action = TargetResource["::updateById::agregado::agregadoAsignaciones"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.AgregadoAsignacion
 * @header apiLb.AgregadoAsignacion
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `AgregadoAsignacion` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "AgregadoAsignacion",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/agregado-asignacion/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use AgregadoAsignacion.agregado() instead.
        "prototype$__get__agregado": {
          url: urlBase + "/agregado-asignacion/:id/agregado",
          method: "GET"
        },

        // INTERNAL. Use AgregadoAsignacion.almacen() instead.
        "prototype$__get__almacen": {
          url: urlBase + "/agregado-asignacion/:id/almacen",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#create
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/agregado-asignacion",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#upsert
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/agregado-asignacion",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#exists
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/agregado-asignacion/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#findById
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/agregado-asignacion/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#find
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/agregado-asignacion",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#findOne
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/agregado-asignacion/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#updateAll
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/agregado-asignacion/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#deleteById
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/agregado-asignacion/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#count
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/agregado-asignacion/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#prototype$updateAttributes
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/agregado-asignacion/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#createChangeStream
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/agregado-asignacion/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.findById() instead.
        "::findById::agregado::agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/:fk",
          method: "GET"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.destroyById() instead.
        "::destroyById::agregado::agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.updateById() instead.
        "::updateById::agregado::agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones() instead.
        "::get::agregado::agregadoAsignaciones": {
          isArray: true,
          url: urlBase + "/agregados/:id/agregadoAsignaciones",
          method: "GET"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.create() instead.
        "::create::agregado::agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones",
          method: "POST"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.destroyAll() instead.
        "::delete::agregado::agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones",
          method: "DELETE"
        },

        // INTERNAL. Use Agregado.agregadoAsignaciones.count() instead.
        "::count::agregado::agregadoAsignaciones": {
          url: urlBase + "/agregados/:id/agregadoAsignaciones/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#updateOrCreate
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AgregadoAsignacion` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#update
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#destroyById
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#removeById
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.AgregadoAsignacion#modelName
    * @propertyOf apiLb.AgregadoAsignacion
    * @description
    * The name of the model represented by this $resource,
    * i.e. `AgregadoAsignacion`.
    */
    R.modelName = "AgregadoAsignacion";


        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#agregado
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Fetches belongsTo relation agregado.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Agregado` object.)
         * </em>
         */
        R.agregado = function() {
          var TargetResource = $injector.get("Agregado");
          var action = TargetResource["::get::agregadoAsignacion::agregado"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.AgregadoAsignacion#almacen
         * @methodOf apiLb.AgregadoAsignacion
         *
         * @description
         *
         * Fetches belongsTo relation almacen.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacen = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::get::agregadoAsignacion::almacen"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Picture
 * @header apiLb.Picture
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Picture` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Picture",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/pictures/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Picture#getContainers
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Picture` object.)
         * </em>
         */
        "getContainers": {
          isArray: true,
          url: urlBase + "/pictures",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#createContainer
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Picture` object.)
         * </em>
         */
        "createContainer": {
          url: urlBase + "/pictures",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#destroyContainer
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "destroyContainer": {
          url: urlBase + "/pictures/:container",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#getContainer
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Picture` object.)
         * </em>
         */
        "getContainer": {
          url: urlBase + "/pictures/:container",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#getFiles
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Picture` object.)
         * </em>
         */
        "getFiles": {
          isArray: true,
          url: urlBase + "/pictures/:container/files",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#getFile
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Picture` object.)
         * </em>
         */
        "getFile": {
          url: urlBase + "/pictures/:container/files/:file",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#removeFile
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "removeFile": {
          url: urlBase + "/pictures/:container/files/:file",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#upload
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `req` – `{object=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `result` – `{object=}` - 
         */
        "upload": {
          url: urlBase + "/pictures/:container/upload",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Picture#download
         * @methodOf apiLb.Picture
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "download": {
          url: urlBase + "/pictures/:container/download/:file",
          method: "GET"
        },
      }
    );




    /**
    * @ngdoc property
    * @name apiLb.Picture#modelName
    * @propertyOf apiLb.Picture
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Picture`.
    */
    R.modelName = "Picture";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Acl
 * @header apiLb.Acl
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Acl` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Acl",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/acls/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Acl.role() instead.
        "prototype$__get__role": {
          url: urlBase + "/acls/:id/role",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#create
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/acls",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#upsert
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/acls",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#exists
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/acls/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#findById
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/acls/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#find
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/acls",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#findOne
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/acls/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#updateAll
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/acls/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#deleteById
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/acls/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#count
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/acls/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#prototype$updateAttributes
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - ACL id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/acls/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Acl#createChangeStream
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/acls/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Role.acls.findById() instead.
        "::findById::role::acls": {
          url: urlBase + "/roles/:id/acls/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.acls.destroyById() instead.
        "::destroyById::role::acls": {
          url: urlBase + "/roles/:id/acls/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.acls.updateById() instead.
        "::updateById::role::acls": {
          url: urlBase + "/roles/:id/acls/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.acls() instead.
        "::get::role::acls": {
          isArray: true,
          url: urlBase + "/roles/:id/acls",
          method: "GET"
        },

        // INTERNAL. Use Role.acls.create() instead.
        "::create::role::acls": {
          url: urlBase + "/roles/:id/acls",
          method: "POST"
        },

        // INTERNAL. Use Role.acls.destroyAll() instead.
        "::delete::role::acls": {
          url: urlBase + "/roles/:id/acls",
          method: "DELETE"
        },

        // INTERNAL. Use Role.acls.count() instead.
        "::count::role::acls": {
          url: urlBase + "/roles/:id/acls/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Acl#updateOrCreate
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Acl#update
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Acl#destroyById
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Acl#removeById
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Acl#modelName
    * @propertyOf apiLb.Acl
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Acl`.
    */
    R.modelName = "Acl";


        /**
         * @ngdoc method
         * @name apiLb.Acl#role
         * @methodOf apiLb.Acl
         *
         * @description
         *
         * Fetches belongsTo relation role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - ACL id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.role = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::get::acl::role"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.AccessToken
 * @header apiLb.AccessToken
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `AccessToken` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "AccessToken",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/accessTokens/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use AccessToken.user() instead.
        "prototype$__get__user": {
          url: urlBase + "/accessTokens/:id/user",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#create
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/accessTokens",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#upsert
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/accessTokens",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#exists
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/accessTokens/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#findById
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/accessTokens/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#find
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/accessTokens",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#findOne
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/accessTokens/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#updateAll
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/accessTokens/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#deleteById
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/accessTokens/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#count
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/accessTokens/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#prototype$updateAttributes
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - AccessToken id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/accessTokens/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#createChangeStream
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/accessTokens/change-stream",
          method: "POST"
        },

        // INTERNAL. Use User.accessTokens.findById() instead.
        "::findById::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "GET"
        },

        // INTERNAL. Use User.accessTokens.destroyById() instead.
        "::destroyById::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use User.accessTokens.updateById() instead.
        "::updateById::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "PUT"
        },

        // INTERNAL. Use User.accessTokens() instead.
        "::get::user::accessTokens": {
          isArray: true,
          url: urlBase + "/users/:id/accessTokens",
          method: "GET"
        },

        // INTERNAL. Use User.accessTokens.create() instead.
        "::create::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "POST"
        },

        // INTERNAL. Use User.accessTokens.destroyAll() instead.
        "::delete::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "DELETE"
        },

        // INTERNAL. Use User.accessTokens.count() instead.
        "::count::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.AccessToken#updateOrCreate
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#update
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#destroyById
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.AccessToken#removeById
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.AccessToken#modelName
    * @propertyOf apiLb.AccessToken
    * @description
    * The name of the model represented by this $resource,
    * i.e. `AccessToken`.
    */
    R.modelName = "AccessToken";


        /**
         * @ngdoc method
         * @name apiLb.AccessToken#user
         * @methodOf apiLb.AccessToken
         *
         * @description
         *
         * Fetches belongsTo relation user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - AccessToken id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.user = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::accessToken::user"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Role
 * @header apiLb.Role
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Role` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Role",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/roles/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Role.principals.findById() instead.
        "prototype$__findById__principals": {
          url: urlBase + "/roles/:id/principals/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.principals.destroyById() instead.
        "prototype$__destroyById__principals": {
          url: urlBase + "/roles/:id/principals/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.principals.updateById() instead.
        "prototype$__updateById__principals": {
          url: urlBase + "/roles/:id/principals/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.acls.findById() instead.
        "prototype$__findById__acls": {
          url: urlBase + "/roles/:id/acls/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.acls.destroyById() instead.
        "prototype$__destroyById__acls": {
          url: urlBase + "/roles/:id/acls/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.acls.updateById() instead.
        "prototype$__updateById__acls": {
          url: urlBase + "/roles/:id/acls/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.users.findById() instead.
        "prototype$__findById__users": {
          url: urlBase + "/roles/:id/users/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.users.destroyById() instead.
        "prototype$__destroyById__users": {
          url: urlBase + "/roles/:id/users/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.users.updateById() instead.
        "prototype$__updateById__users": {
          url: urlBase + "/roles/:id/users/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.users.link() instead.
        "prototype$__link__users": {
          url: urlBase + "/roles/:id/users/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.users.unlink() instead.
        "prototype$__unlink__users": {
          url: urlBase + "/roles/:id/users/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.users.exists() instead.
        "prototype$__exists__users": {
          url: urlBase + "/roles/:id/users/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Role.trabajadores.findById() instead.
        "prototype$__findById__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.trabajadores.destroyById() instead.
        "prototype$__destroyById__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.updateById() instead.
        "prototype$__updateById__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.trabajadores.link() instead.
        "prototype$__link__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.trabajadores.unlink() instead.
        "prototype$__unlink__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.exists() instead.
        "prototype$__exists__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Role.principals() instead.
        "prototype$__get__principals": {
          isArray: true,
          url: urlBase + "/roles/:id/principals",
          method: "GET"
        },

        // INTERNAL. Use Role.principals.create() instead.
        "prototype$__create__principals": {
          url: urlBase + "/roles/:id/principals",
          method: "POST"
        },

        // INTERNAL. Use Role.principals.destroyAll() instead.
        "prototype$__delete__principals": {
          url: urlBase + "/roles/:id/principals",
          method: "DELETE"
        },

        // INTERNAL. Use Role.principals.count() instead.
        "prototype$__count__principals": {
          url: urlBase + "/roles/:id/principals/count",
          method: "GET"
        },

        // INTERNAL. Use Role.acls() instead.
        "prototype$__get__acls": {
          isArray: true,
          url: urlBase + "/roles/:id/acls",
          method: "GET"
        },

        // INTERNAL. Use Role.acls.create() instead.
        "prototype$__create__acls": {
          url: urlBase + "/roles/:id/acls",
          method: "POST"
        },

        // INTERNAL. Use Role.acls.destroyAll() instead.
        "prototype$__delete__acls": {
          url: urlBase + "/roles/:id/acls",
          method: "DELETE"
        },

        // INTERNAL. Use Role.acls.count() instead.
        "prototype$__count__acls": {
          url: urlBase + "/roles/:id/acls/count",
          method: "GET"
        },

        // INTERNAL. Use Role.users() instead.
        "prototype$__get__users": {
          isArray: true,
          url: urlBase + "/roles/:id/users",
          method: "GET"
        },

        // INTERNAL. Use Role.users.create() instead.
        "prototype$__create__users": {
          url: urlBase + "/roles/:id/users",
          method: "POST"
        },

        // INTERNAL. Use Role.users.destroyAll() instead.
        "prototype$__delete__users": {
          url: urlBase + "/roles/:id/users",
          method: "DELETE"
        },

        // INTERNAL. Use Role.users.count() instead.
        "prototype$__count__users": {
          url: urlBase + "/roles/:id/users/count",
          method: "GET"
        },

        // INTERNAL. Use Role.trabajadores() instead.
        "prototype$__get__trabajadores": {
          isArray: true,
          url: urlBase + "/roles/:id/trabajadores",
          method: "GET"
        },

        // INTERNAL. Use Role.trabajadores.create() instead.
        "prototype$__create__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores",
          method: "POST"
        },

        // INTERNAL. Use Role.trabajadores.destroyAll() instead.
        "prototype$__delete__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.count() instead.
        "prototype$__count__trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#create
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/roles",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#upsert
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/roles",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#exists
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/roles/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#findById
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/roles/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#find
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/roles",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#findOne
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/roles/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#updateAll
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/roles/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#deleteById
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/roles/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#count
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/roles/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#prototype$updateAttributes
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/roles/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Role#createChangeStream
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/roles/change-stream",
          method: "POST"
        },

        // INTERNAL. Use User.roles.findById() instead.
        "::findById::user::roles": {
          url: urlBase + "/users/:id/roles/:fk",
          method: "GET"
        },

        // INTERNAL. Use User.roles.destroyById() instead.
        "::destroyById::user::roles": {
          url: urlBase + "/users/:id/roles/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use User.roles.updateById() instead.
        "::updateById::user::roles": {
          url: urlBase + "/users/:id/roles/:fk",
          method: "PUT"
        },

        // INTERNAL. Use User.roles.link() instead.
        "::link::user::roles": {
          url: urlBase + "/users/:id/roles/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use User.roles.unlink() instead.
        "::unlink::user::roles": {
          url: urlBase + "/users/:id/roles/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use User.roles.exists() instead.
        "::exists::user::roles": {
          url: urlBase + "/users/:id/roles/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use User.roles() instead.
        "::get::user::roles": {
          isArray: true,
          url: urlBase + "/users/:id/roles",
          method: "GET"
        },

        // INTERNAL. Use User.roles.create() instead.
        "::create::user::roles": {
          url: urlBase + "/users/:id/roles",
          method: "POST"
        },

        // INTERNAL. Use User.roles.destroyAll() instead.
        "::delete::user::roles": {
          url: urlBase + "/users/:id/roles",
          method: "DELETE"
        },

        // INTERNAL. Use User.roles.count() instead.
        "::count::user::roles": {
          url: urlBase + "/users/:id/roles/count",
          method: "GET"
        },

        // INTERNAL. Use Acl.role() instead.
        "::get::acl::role": {
          url: urlBase + "/acls/:id/role",
          method: "GET"
        },

        // INTERNAL. Use RoleMapping.role() instead.
        "::get::roleMapping::role": {
          url: urlBase + "/roleMappings/:id/role",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.roles.findById() instead.
        "::findById::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/:fk",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.roles.destroyById() instead.
        "::destroyById::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.updateById() instead.
        "::updateById::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Trabajador.roles.link() instead.
        "::link::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Trabajador.roles.unlink() instead.
        "::unlink::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.exists() instead.
        "::exists::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Trabajador.roles() instead.
        "::get::trabajador::roles": {
          isArray: true,
          url: urlBase + "/trabajadores/:id/roles",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.roles.create() instead.
        "::create::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.roles.destroyAll() instead.
        "::delete::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.count() instead.
        "::count::trabajador::roles": {
          url: urlBase + "/trabajadores/:id/roles/count",
          method: "GET"
        },

        // INTERNAL. Use RoleTrabajador.role() instead.
        "::get::roleTrabajador::role": {
          url: urlBase + "/role-trabajador/:id/role",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Role#updateOrCreate
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Role#update
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Role#destroyById
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Role#removeById
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Role#modelName
    * @propertyOf apiLb.Role
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Role`.
    */
    R.modelName = "Role";

    /**
     * @ngdoc object
     * @name lbServices.Role.principals
     * @header lbServices.Role.principals
     * @object
     * @description
     *
     * The object `Role.principals` groups methods
     * manipulating `RoleMapping` instances related to `Role`.
     *
     * Call {@link lbServices.Role#principals Role.principals()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Role#principals
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Queries principals of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        R.principals = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::get::role::principals"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.principals#count
         * @methodOf apiLb.Role.principals
         *
         * @description
         *
         * Counts principals of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.principals.count = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::count::role::principals"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.principals#create
         * @methodOf apiLb.Role.principals
         *
         * @description
         *
         * Creates a new instance in principals of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        R.principals.create = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::create::role::principals"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.principals#destroyAll
         * @methodOf apiLb.Role.principals
         *
         * @description
         *
         * Deletes all principals of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.principals.destroyAll = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::delete::role::principals"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.principals#destroyById
         * @methodOf apiLb.Role.principals
         *
         * @description
         *
         * Delete a related item by id for principals.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for principals
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.principals.destroyById = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::destroyById::role::principals"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.principals#findById
         * @methodOf apiLb.Role.principals
         *
         * @description
         *
         * Find a related item by id for principals.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for principals
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        R.principals.findById = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::findById::role::principals"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.principals#updateById
         * @methodOf apiLb.Role.principals
         *
         * @description
         *
         * Update a related item by id for principals.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for principals
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        R.principals.updateById = function() {
          var TargetResource = $injector.get("RoleMapping");
          var action = TargetResource["::updateById::role::principals"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Role.acls
     * @header lbServices.Role.acls
     * @object
     * @description
     *
     * The object `Role.acls` groups methods
     * manipulating `Acl` instances related to `Role`.
     *
     * Call {@link lbServices.Role#acls Role.acls()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Role#acls
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Queries acls of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        R.acls = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::get::role::acls"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.acls#count
         * @methodOf apiLb.Role.acls
         *
         * @description
         *
         * Counts acls of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.acls.count = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::count::role::acls"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.acls#create
         * @methodOf apiLb.Role.acls
         *
         * @description
         *
         * Creates a new instance in acls of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        R.acls.create = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::create::role::acls"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.acls#destroyAll
         * @methodOf apiLb.Role.acls
         *
         * @description
         *
         * Deletes all acls of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.acls.destroyAll = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::delete::role::acls"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.acls#destroyById
         * @methodOf apiLb.Role.acls
         *
         * @description
         *
         * Delete a related item by id for acls.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for acls
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.acls.destroyById = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::destroyById::role::acls"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.acls#findById
         * @methodOf apiLb.Role.acls
         *
         * @description
         *
         * Find a related item by id for acls.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for acls
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        R.acls.findById = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::findById::role::acls"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.acls#updateById
         * @methodOf apiLb.Role.acls
         *
         * @description
         *
         * Update a related item by id for acls.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for acls
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Acl` object.)
         * </em>
         */
        R.acls.updateById = function() {
          var TargetResource = $injector.get("Acl");
          var action = TargetResource["::updateById::role::acls"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Role.users
     * @header lbServices.Role.users
     * @object
     * @description
     *
     * The object `Role.users` groups methods
     * manipulating `User` instances related to `Role`.
     *
     * Call {@link lbServices.Role#users Role.users()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Role#users
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Queries users of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.users = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#count
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Counts users of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.users.count = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::count::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#create
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Creates a new instance in users of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.users.create = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::create::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#destroyAll
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Deletes all users of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.users.destroyAll = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::delete::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#destroyById
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Delete a related item by id for users.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for users
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.users.destroyById = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::destroyById::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#exists
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Check the existence of users relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for users
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.users.exists = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::exists::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#findById
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Find a related item by id for users.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for users
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.users.findById = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::findById::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#link
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Add a related item by id for users.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for users
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.users.link = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::link::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#unlink
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Remove the users relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for users
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.users.unlink = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::unlink::role::users"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.users#updateById
         * @methodOf apiLb.Role.users
         *
         * @description
         *
         * Update a related item by id for users.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for users
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.users.updateById = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::updateById::role::users"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Role.trabajadores
     * @header lbServices.Role.trabajadores
     * @object
     * @description
     *
     * The object `Role.trabajadores` groups methods
     * manipulating `Trabajador` instances related to `Role`.
     *
     * Call {@link lbServices.Role#trabajadores Role.trabajadores()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Role#trabajadores
         * @methodOf apiLb.Role
         *
         * @description
         *
         * Queries trabajadores of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::get::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#count
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Counts trabajadores of role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.trabajadores.count = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::count::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#create
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Creates a new instance in trabajadores of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.create = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::create::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#destroyAll
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Deletes all trabajadores of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajadores.destroyAll = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::delete::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#destroyById
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Delete a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajadores.destroyById = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::destroyById::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#exists
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Check the existence of trabajadores relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.exists = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::exists::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#findById
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Find a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.findById = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::findById::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#link
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Add a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.link = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::link::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#unlink
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Remove the trabajadores relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajadores.unlink = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::unlink::role::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Role.trabajadores#updateById
         * @methodOf apiLb.Role.trabajadores
         *
         * @description
         *
         * Update a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Role id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.updateById = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::updateById::role::trabajadores"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.RoleMapping
 * @header apiLb.RoleMapping
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `RoleMapping` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "RoleMapping",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/roleMappings/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use RoleMapping.role() instead.
        "prototype$__get__role": {
          url: urlBase + "/roleMappings/:id/role",
          method: "GET"
        },

        // INTERNAL. Use RoleMapping.user() instead.
        "prototype$__get__user": {
          url: urlBase + "/roleMappings/:id/user",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#create
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/roleMappings",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#upsert
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/roleMappings",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#exists
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/roleMappings/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#findById
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/roleMappings/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#find
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/roleMappings",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#findOne
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/roleMappings/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#updateAll
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/roleMappings/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#deleteById
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/roleMappings/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#count
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/roleMappings/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#prototype$updateAttributes
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - RoleMapping id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/roleMappings/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#createChangeStream
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/roleMappings/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Role.principals.findById() instead.
        "::findById::role::principals": {
          url: urlBase + "/roles/:id/principals/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.principals.destroyById() instead.
        "::destroyById::role::principals": {
          url: urlBase + "/roles/:id/principals/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.principals.updateById() instead.
        "::updateById::role::principals": {
          url: urlBase + "/roles/:id/principals/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.principals() instead.
        "::get::role::principals": {
          isArray: true,
          url: urlBase + "/roles/:id/principals",
          method: "GET"
        },

        // INTERNAL. Use Role.principals.create() instead.
        "::create::role::principals": {
          url: urlBase + "/roles/:id/principals",
          method: "POST"
        },

        // INTERNAL. Use Role.principals.destroyAll() instead.
        "::delete::role::principals": {
          url: urlBase + "/roles/:id/principals",
          method: "DELETE"
        },

        // INTERNAL. Use Role.principals.count() instead.
        "::count::role::principals": {
          url: urlBase + "/roles/:id/principals/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#updateOrCreate
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleMapping` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#update
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#destroyById
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#removeById
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.RoleMapping#modelName
    * @propertyOf apiLb.RoleMapping
    * @description
    * The name of the model represented by this $resource,
    * i.e. `RoleMapping`.
    */
    R.modelName = "RoleMapping";


        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#role
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Fetches belongsTo relation role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - RoleMapping id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.role = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::get::roleMapping::role"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.RoleMapping#user
         * @methodOf apiLb.RoleMapping
         *
         * @description
         *
         * Fetches belongsTo relation user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - RoleMapping id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.user = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::roleMapping::user"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Empresa
 * @header apiLb.Empresa
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Empresa` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Empresa",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/empresas/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Empresa.flotas.findById() instead.
        "prototype$__findById__flotas": {
          url: urlBase + "/empresas/:id/flotas/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.flotas.destroyById() instead.
        "prototype$__destroyById__flotas": {
          url: urlBase + "/empresas/:id/flotas/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.flotas.updateById() instead.
        "prototype$__updateById__flotas": {
          url: urlBase + "/empresas/:id/flotas/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.areas.findById() instead.
        "prototype$__findById__areas": {
          url: urlBase + "/empresas/:id/areas/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.areas.destroyById() instead.
        "prototype$__destroyById__areas": {
          url: urlBase + "/empresas/:id/areas/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.areas.updateById() instead.
        "prototype$__updateById__areas": {
          url: urlBase + "/empresas/:id/areas/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.sucursales.findById() instead.
        "prototype$__findById__sucursales": {
          url: urlBase + "/empresas/:id/sucursales/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.sucursales.destroyById() instead.
        "prototype$__destroyById__sucursales": {
          url: urlBase + "/empresas/:id/sucursales/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.sucursales.updateById() instead.
        "prototype$__updateById__sucursales": {
          url: urlBase + "/empresas/:id/sucursales/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.almacenes.findById() instead.
        "prototype$__findById__almacenes": {
          url: urlBase + "/empresas/:id/almacenes/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.almacenes.destroyById() instead.
        "prototype$__destroyById__almacenes": {
          url: urlBase + "/empresas/:id/almacenes/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.almacenes.updateById() instead.
        "prototype$__updateById__almacenes": {
          url: urlBase + "/empresas/:id/almacenes/:fk",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#__get__checkMain
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Queries checkMain of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "__get__checkMain": {
          isArray: true,
          url: urlBase + "/empresas/checkMain",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#__create__checkMain
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Creates a new instance in checkMain of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "__create__checkMain": {
          url: urlBase + "/empresas/checkMain",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#__delete__checkMain
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Deletes all checkMain of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "__delete__checkMain": {
          url: urlBase + "/empresas/checkMain",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#__count__checkMain
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Counts checkMain of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "__count__checkMain": {
          url: urlBase + "/empresas/checkMain/count",
          method: "GET"
        },

        // INTERNAL. Use Empresa.flotas() instead.
        "prototype$__get__flotas": {
          isArray: true,
          url: urlBase + "/empresas/:id/flotas",
          method: "GET"
        },

        // INTERNAL. Use Empresa.flotas.create() instead.
        "prototype$__create__flotas": {
          url: urlBase + "/empresas/:id/flotas",
          method: "POST"
        },

        // INTERNAL. Use Empresa.flotas.destroyAll() instead.
        "prototype$__delete__flotas": {
          url: urlBase + "/empresas/:id/flotas",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.flotas.count() instead.
        "prototype$__count__flotas": {
          url: urlBase + "/empresas/:id/flotas/count",
          method: "GET"
        },

        // INTERNAL. Use Empresa.areas() instead.
        "prototype$__get__areas": {
          isArray: true,
          url: urlBase + "/empresas/:id/areas",
          method: "GET"
        },

        // INTERNAL. Use Empresa.areas.create() instead.
        "prototype$__create__areas": {
          url: urlBase + "/empresas/:id/areas",
          method: "POST"
        },

        // INTERNAL. Use Empresa.areas.destroyAll() instead.
        "prototype$__delete__areas": {
          url: urlBase + "/empresas/:id/areas",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.areas.count() instead.
        "prototype$__count__areas": {
          url: urlBase + "/empresas/:id/areas/count",
          method: "GET"
        },

        // INTERNAL. Use Empresa.sucursales() instead.
        "prototype$__get__sucursales": {
          isArray: true,
          url: urlBase + "/empresas/:id/sucursales",
          method: "GET"
        },

        // INTERNAL. Use Empresa.sucursales.create() instead.
        "prototype$__create__sucursales": {
          url: urlBase + "/empresas/:id/sucursales",
          method: "POST"
        },

        // INTERNAL. Use Empresa.sucursales.destroyAll() instead.
        "prototype$__delete__sucursales": {
          url: urlBase + "/empresas/:id/sucursales",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.sucursales.count() instead.
        "prototype$__count__sucursales": {
          url: urlBase + "/empresas/:id/sucursales/count",
          method: "GET"
        },

        // INTERNAL. Use Empresa.almacenes() instead.
        "prototype$__get__almacenes": {
          isArray: true,
          url: urlBase + "/empresas/:id/almacenes",
          method: "GET"
        },

        // INTERNAL. Use Empresa.almacenes.create() instead.
        "prototype$__create__almacenes": {
          url: urlBase + "/empresas/:id/almacenes",
          method: "POST"
        },

        // INTERNAL. Use Empresa.almacenes.destroyAll() instead.
        "prototype$__delete__almacenes": {
          url: urlBase + "/empresas/:id/almacenes",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.almacenes.count() instead.
        "prototype$__count__almacenes": {
          url: urlBase + "/empresas/:id/almacenes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#create
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/empresas",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#upsert
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/empresas",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#exists
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/empresas/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#findById
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/empresas/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#find
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/empresas",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#findOne
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/empresas/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#updateAll
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/empresas/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#deleteById
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/empresas/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#count
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/empresas/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#prototype$updateAttributes
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/empresas/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Empresa#createChangeStream
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/empresas/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Flota.empresa() instead.
        "::get::flota::empresa": {
          url: urlBase + "/flota/:id/empresa",
          method: "GET"
        },

        // INTERNAL. Use Area.empresa() instead.
        "::get::area::empresa": {
          url: urlBase + "/areas/:id/empresa",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.empresa() instead.
        "::get::sucursal::empresa": {
          url: urlBase + "/sucursales/:id/empresa",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Empresa#updateOrCreate
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Empresa#update
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Empresa#destroyById
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Empresa#removeById
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Empresa#modelName
    * @propertyOf apiLb.Empresa
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Empresa`.
    */
    R.modelName = "Empresa";

    /**
     * @ngdoc object
     * @name lbServices.Empresa.flotas
     * @header lbServices.Empresa.flotas
     * @object
     * @description
     *
     * The object `Empresa.flotas` groups methods
     * manipulating `Flota` instances related to `Empresa`.
     *
     * Call {@link lbServices.Empresa#flotas Empresa.flotas()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Empresa#flotas
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Queries flotas of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flotas = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::get::empresa::flotas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.flotas#count
         * @methodOf apiLb.Empresa.flotas
         *
         * @description
         *
         * Counts flotas of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.flotas.count = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::count::empresa::flotas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.flotas#create
         * @methodOf apiLb.Empresa.flotas
         *
         * @description
         *
         * Creates a new instance in flotas of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flotas.create = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::create::empresa::flotas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.flotas#destroyAll
         * @methodOf apiLb.Empresa.flotas
         *
         * @description
         *
         * Deletes all flotas of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.flotas.destroyAll = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::delete::empresa::flotas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.flotas#destroyById
         * @methodOf apiLb.Empresa.flotas
         *
         * @description
         *
         * Delete a related item by id for flotas.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for flotas
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.flotas.destroyById = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::destroyById::empresa::flotas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.flotas#findById
         * @methodOf apiLb.Empresa.flotas
         *
         * @description
         *
         * Find a related item by id for flotas.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for flotas
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flotas.findById = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::findById::empresa::flotas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.flotas#updateById
         * @methodOf apiLb.Empresa.flotas
         *
         * @description
         *
         * Update a related item by id for flotas.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for flotas
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flotas.updateById = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::updateById::empresa::flotas"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Empresa.areas
     * @header lbServices.Empresa.areas
     * @object
     * @description
     *
     * The object `Empresa.areas` groups methods
     * manipulating `Area` instances related to `Empresa`.
     *
     * Call {@link lbServices.Empresa#areas Empresa.areas()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Empresa#areas
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Queries areas of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        R.areas = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::get::empresa::areas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.areas#count
         * @methodOf apiLb.Empresa.areas
         *
         * @description
         *
         * Counts areas of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.areas.count = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::count::empresa::areas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.areas#create
         * @methodOf apiLb.Empresa.areas
         *
         * @description
         *
         * Creates a new instance in areas of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        R.areas.create = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::create::empresa::areas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.areas#destroyAll
         * @methodOf apiLb.Empresa.areas
         *
         * @description
         *
         * Deletes all areas of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.areas.destroyAll = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::delete::empresa::areas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.areas#destroyById
         * @methodOf apiLb.Empresa.areas
         *
         * @description
         *
         * Delete a related item by id for areas.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for areas
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.areas.destroyById = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::destroyById::empresa::areas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.areas#findById
         * @methodOf apiLb.Empresa.areas
         *
         * @description
         *
         * Find a related item by id for areas.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for areas
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        R.areas.findById = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::findById::empresa::areas"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.areas#updateById
         * @methodOf apiLb.Empresa.areas
         *
         * @description
         *
         * Update a related item by id for areas.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for areas
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        R.areas.updateById = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::updateById::empresa::areas"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Empresa.sucursales
     * @header lbServices.Empresa.sucursales
     * @object
     * @description
     *
     * The object `Empresa.sucursales` groups methods
     * manipulating `Sucursal` instances related to `Empresa`.
     *
     * Call {@link lbServices.Empresa#sucursales Empresa.sucursales()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Empresa#sucursales
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Queries sucursales of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R.sucursales = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::get::empresa::sucursales"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.sucursales#count
         * @methodOf apiLb.Empresa.sucursales
         *
         * @description
         *
         * Counts sucursales of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.sucursales.count = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::count::empresa::sucursales"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.sucursales#create
         * @methodOf apiLb.Empresa.sucursales
         *
         * @description
         *
         * Creates a new instance in sucursales of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R.sucursales.create = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::create::empresa::sucursales"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.sucursales#destroyAll
         * @methodOf apiLb.Empresa.sucursales
         *
         * @description
         *
         * Deletes all sucursales of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.sucursales.destroyAll = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::delete::empresa::sucursales"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.sucursales#destroyById
         * @methodOf apiLb.Empresa.sucursales
         *
         * @description
         *
         * Delete a related item by id for sucursales.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sucursales
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.sucursales.destroyById = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::destroyById::empresa::sucursales"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.sucursales#findById
         * @methodOf apiLb.Empresa.sucursales
         *
         * @description
         *
         * Find a related item by id for sucursales.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sucursales
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R.sucursales.findById = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::findById::empresa::sucursales"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.sucursales#updateById
         * @methodOf apiLb.Empresa.sucursales
         *
         * @description
         *
         * Update a related item by id for sucursales.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for sucursales
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R.sucursales.updateById = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::updateById::empresa::sucursales"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Empresa.almacenes
     * @header lbServices.Empresa.almacenes
     * @object
     * @description
     *
     * The object `Empresa.almacenes` groups methods
     * manipulating `Almacen` instances related to `Empresa`.
     *
     * Call {@link lbServices.Empresa#almacenes Empresa.almacenes()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Empresa#almacenes
         * @methodOf apiLb.Empresa
         *
         * @description
         *
         * Queries almacenes of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::get::empresa::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.almacenes#count
         * @methodOf apiLb.Empresa.almacenes
         *
         * @description
         *
         * Counts almacenes of empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.almacenes.count = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::count::empresa::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.almacenes#create
         * @methodOf apiLb.Empresa.almacenes
         *
         * @description
         *
         * Creates a new instance in almacenes of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes.create = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::create::empresa::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.almacenes#destroyAll
         * @methodOf apiLb.Empresa.almacenes
         *
         * @description
         *
         * Deletes all almacenes of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.almacenes.destroyAll = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::delete::empresa::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.almacenes#destroyById
         * @methodOf apiLb.Empresa.almacenes
         *
         * @description
         *
         * Delete a related item by id for almacenes.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for almacenes
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.almacenes.destroyById = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::destroyById::empresa::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.almacenes#findById
         * @methodOf apiLb.Empresa.almacenes
         *
         * @description
         *
         * Find a related item by id for almacenes.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for almacenes
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes.findById = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::findById::empresa::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Empresa.almacenes#updateById
         * @methodOf apiLb.Empresa.almacenes
         *
         * @description
         *
         * Update a related item by id for almacenes.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for almacenes
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes.updateById = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::updateById::empresa::almacenes"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.UnidadMedida
 * @header apiLb.UnidadMedida
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `UnidadMedida` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "UnidadMedida",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/unidad-medidas/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#create
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/unidad-medidas",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#upsert
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/unidad-medidas",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#exists
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/unidad-medidas/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#findById
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/unidad-medidas/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#find
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/unidad-medidas",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#findOne
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/unidad-medidas/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#updateAll
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/unidad-medidas/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#deleteById
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/unidad-medidas/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#count
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/unidad-medidas/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#prototype$updateAttributes
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/unidad-medidas/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#createChangeStream
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/unidad-medidas/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#updateOrCreate
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `UnidadMedida` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#update
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#destroyById
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.UnidadMedida#removeById
         * @methodOf apiLb.UnidadMedida
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.UnidadMedida#modelName
    * @propertyOf apiLb.UnidadMedida
    * @description
    * The name of the model represented by this $resource,
    * i.e. `UnidadMedida`.
    */
    R.modelName = "UnidadMedida";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Venta
 * @header apiLb.Venta
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Venta` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Venta",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/ventas/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Venta#create
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/ventas",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#upsert
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/ventas",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#exists
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/ventas/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#findById
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/ventas/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#find
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/ventas",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#findOne
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/ventas/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#updateAll
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/ventas/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#deleteById
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/ventas/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#count
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/ventas/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#prototype$updateAttributes
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/ventas/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Venta#createChangeStream
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/ventas/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Venta#updateOrCreate
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Venta` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Venta#update
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Venta#destroyById
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Venta#removeById
         * @methodOf apiLb.Venta
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Venta#modelName
    * @propertyOf apiLb.Venta
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Venta`.
    */
    R.modelName = "Venta";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Flota
 * @header apiLb.Flota
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Flota` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Flota",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/flota/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Flota.empresa() instead.
        "prototype$__get__empresa": {
          url: urlBase + "/flota/:id/empresa",
          method: "GET"
        },

        // INTERNAL. Use Flota.camiones.findById() instead.
        "prototype$__findById__camiones": {
          url: urlBase + "/flota/:id/camiones/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.camiones.destroyById() instead.
        "prototype$__destroyById__camiones": {
          url: urlBase + "/flota/:id/camiones/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.camiones.updateById() instead.
        "prototype$__updateById__camiones": {
          url: urlBase + "/flota/:id/camiones/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.maquinaria.findById() instead.
        "prototype$__findById__maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.maquinaria.destroyById() instead.
        "prototype$__destroyById__maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.maquinaria.updateById() instead.
        "prototype$__updateById__maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.motos.findById() instead.
        "prototype$__findById__motos": {
          url: urlBase + "/flota/:id/motos/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.motos.destroyById() instead.
        "prototype$__destroyById__motos": {
          url: urlBase + "/flota/:id/motos/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.motos.updateById() instead.
        "prototype$__updateById__motos": {
          url: urlBase + "/flota/:id/motos/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.carros.findById() instead.
        "prototype$__findById__carros": {
          url: urlBase + "/flota/:id/carros/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.carros.destroyById() instead.
        "prototype$__destroyById__carros": {
          url: urlBase + "/flota/:id/carros/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.carros.updateById() instead.
        "prototype$__updateById__carros": {
          url: urlBase + "/flota/:id/carros/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.camiones() instead.
        "prototype$__get__camiones": {
          isArray: true,
          url: urlBase + "/flota/:id/camiones",
          method: "GET"
        },

        // INTERNAL. Use Flota.camiones.create() instead.
        "prototype$__create__camiones": {
          url: urlBase + "/flota/:id/camiones",
          method: "POST"
        },

        // INTERNAL. Use Flota.camiones.destroyAll() instead.
        "prototype$__delete__camiones": {
          url: urlBase + "/flota/:id/camiones",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.camiones.count() instead.
        "prototype$__count__camiones": {
          url: urlBase + "/flota/:id/camiones/count",
          method: "GET"
        },

        // INTERNAL. Use Flota.maquinaria() instead.
        "prototype$__get__maquinaria": {
          isArray: true,
          url: urlBase + "/flota/:id/maquinaria",
          method: "GET"
        },

        // INTERNAL. Use Flota.maquinaria.create() instead.
        "prototype$__create__maquinaria": {
          url: urlBase + "/flota/:id/maquinaria",
          method: "POST"
        },

        // INTERNAL. Use Flota.maquinaria.destroyAll() instead.
        "prototype$__delete__maquinaria": {
          url: urlBase + "/flota/:id/maquinaria",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.maquinaria.count() instead.
        "prototype$__count__maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/count",
          method: "GET"
        },

        // INTERNAL. Use Flota.motos() instead.
        "prototype$__get__motos": {
          isArray: true,
          url: urlBase + "/flota/:id/motos",
          method: "GET"
        },

        // INTERNAL. Use Flota.motos.create() instead.
        "prototype$__create__motos": {
          url: urlBase + "/flota/:id/motos",
          method: "POST"
        },

        // INTERNAL. Use Flota.motos.destroyAll() instead.
        "prototype$__delete__motos": {
          url: urlBase + "/flota/:id/motos",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.motos.count() instead.
        "prototype$__count__motos": {
          url: urlBase + "/flota/:id/motos/count",
          method: "GET"
        },

        // INTERNAL. Use Flota.carros() instead.
        "prototype$__get__carros": {
          isArray: true,
          url: urlBase + "/flota/:id/carros",
          method: "GET"
        },

        // INTERNAL. Use Flota.carros.create() instead.
        "prototype$__create__carros": {
          url: urlBase + "/flota/:id/carros",
          method: "POST"
        },

        // INTERNAL. Use Flota.carros.destroyAll() instead.
        "prototype$__delete__carros": {
          url: urlBase + "/flota/:id/carros",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.carros.count() instead.
        "prototype$__count__carros": {
          url: urlBase + "/flota/:id/carros/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#create
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/flota",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#upsert
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/flota",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#exists
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/flota/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#findById
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/flota/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#find
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/flota",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#findOne
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/flota/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#updateAll
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/flota/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#deleteById
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/flota/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#count
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/flota/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#prototype$updateAttributes
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/flota/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Flota#createChangeStream
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/flota/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Empresa.flotas.findById() instead.
        "::findById::empresa::flotas": {
          url: urlBase + "/empresas/:id/flotas/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.flotas.destroyById() instead.
        "::destroyById::empresa::flotas": {
          url: urlBase + "/empresas/:id/flotas/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.flotas.updateById() instead.
        "::updateById::empresa::flotas": {
          url: urlBase + "/empresas/:id/flotas/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.flotas() instead.
        "::get::empresa::flotas": {
          isArray: true,
          url: urlBase + "/empresas/:id/flotas",
          method: "GET"
        },

        // INTERNAL. Use Empresa.flotas.create() instead.
        "::create::empresa::flotas": {
          url: urlBase + "/empresas/:id/flotas",
          method: "POST"
        },

        // INTERNAL. Use Empresa.flotas.destroyAll() instead.
        "::delete::empresa::flotas": {
          url: urlBase + "/empresas/:id/flotas",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.flotas.count() instead.
        "::count::empresa::flotas": {
          url: urlBase + "/empresas/:id/flotas/count",
          method: "GET"
        },

        // INTERNAL. Use Camion.flota() instead.
        "::get::camion::flota": {
          url: urlBase + "/camiones/:id/flota",
          method: "GET"
        },

        // INTERNAL. Use FlotaMapping.flota() instead.
        "::get::flotaMapping::flota": {
          url: urlBase + "/flotaMapping/:id/flota",
          method: "GET"
        },

        // INTERNAL. Use Maquinaria.flota() instead.
        "::get::maquinaria::flota": {
          url: urlBase + "/maquinaria/:id/flota",
          method: "GET"
        },

        // INTERNAL. Use Moto.flota() instead.
        "::get::moto::flota": {
          url: urlBase + "/motos/:id/flota",
          method: "GET"
        },

        // INTERNAL. Use Carro.flota() instead.
        "::get::carro::flota": {
          url: urlBase + "/carros/:id/flota",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Flota#updateOrCreate
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Flota#update
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Flota#destroyById
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Flota#removeById
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Flota#modelName
    * @propertyOf apiLb.Flota
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Flota`.
    */
    R.modelName = "Flota";


        /**
         * @ngdoc method
         * @name apiLb.Flota#empresa
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Fetches belongsTo relation empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        R.empresa = function() {
          var TargetResource = $injector.get("Empresa");
          var action = TargetResource["::get::flota::empresa"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Flota.camiones
     * @header lbServices.Flota.camiones
     * @object
     * @description
     *
     * The object `Flota.camiones` groups methods
     * manipulating `Camion` instances related to `Flota`.
     *
     * Call {@link lbServices.Flota#camiones Flota.camiones()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Flota#camiones
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Queries camiones of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        R.camiones = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::get::flota::camiones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.camiones#count
         * @methodOf apiLb.Flota.camiones
         *
         * @description
         *
         * Counts camiones of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.camiones.count = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::count::flota::camiones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.camiones#create
         * @methodOf apiLb.Flota.camiones
         *
         * @description
         *
         * Creates a new instance in camiones of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        R.camiones.create = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::create::flota::camiones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.camiones#destroyAll
         * @methodOf apiLb.Flota.camiones
         *
         * @description
         *
         * Deletes all camiones of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.camiones.destroyAll = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::delete::flota::camiones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.camiones#destroyById
         * @methodOf apiLb.Flota.camiones
         *
         * @description
         *
         * Delete a related item by id for camiones.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for camiones
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.camiones.destroyById = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::destroyById::flota::camiones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.camiones#findById
         * @methodOf apiLb.Flota.camiones
         *
         * @description
         *
         * Find a related item by id for camiones.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for camiones
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        R.camiones.findById = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::findById::flota::camiones"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.camiones#updateById
         * @methodOf apiLb.Flota.camiones
         *
         * @description
         *
         * Update a related item by id for camiones.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for camiones
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        R.camiones.updateById = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::updateById::flota::camiones"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Flota.maquinaria
     * @header lbServices.Flota.maquinaria
     * @object
     * @description
     *
     * The object `Flota.maquinaria` groups methods
     * manipulating `Maquinaria` instances related to `Flota`.
     *
     * Call {@link lbServices.Flota#maquinaria Flota.maquinaria()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Flota#maquinaria
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Queries maquinaria of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        R.maquinaria = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::get::flota::maquinaria"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.maquinaria#count
         * @methodOf apiLb.Flota.maquinaria
         *
         * @description
         *
         * Counts maquinaria of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.maquinaria.count = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::count::flota::maquinaria"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.maquinaria#create
         * @methodOf apiLb.Flota.maquinaria
         *
         * @description
         *
         * Creates a new instance in maquinaria of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        R.maquinaria.create = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::create::flota::maquinaria"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.maquinaria#destroyAll
         * @methodOf apiLb.Flota.maquinaria
         *
         * @description
         *
         * Deletes all maquinaria of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.maquinaria.destroyAll = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::delete::flota::maquinaria"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.maquinaria#destroyById
         * @methodOf apiLb.Flota.maquinaria
         *
         * @description
         *
         * Delete a related item by id for maquinaria.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for maquinaria
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.maquinaria.destroyById = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::destroyById::flota::maquinaria"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.maquinaria#findById
         * @methodOf apiLb.Flota.maquinaria
         *
         * @description
         *
         * Find a related item by id for maquinaria.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for maquinaria
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        R.maquinaria.findById = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::findById::flota::maquinaria"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.maquinaria#updateById
         * @methodOf apiLb.Flota.maquinaria
         *
         * @description
         *
         * Update a related item by id for maquinaria.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for maquinaria
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        R.maquinaria.updateById = function() {
          var TargetResource = $injector.get("Maquinaria");
          var action = TargetResource["::updateById::flota::maquinaria"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Flota.motos
     * @header lbServices.Flota.motos
     * @object
     * @description
     *
     * The object `Flota.motos` groups methods
     * manipulating `Moto` instances related to `Flota`.
     *
     * Call {@link lbServices.Flota#motos Flota.motos()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Flota#motos
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Queries motos of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        R.motos = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::get::flota::motos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.motos#count
         * @methodOf apiLb.Flota.motos
         *
         * @description
         *
         * Counts motos of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.motos.count = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::count::flota::motos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.motos#create
         * @methodOf apiLb.Flota.motos
         *
         * @description
         *
         * Creates a new instance in motos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        R.motos.create = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::create::flota::motos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.motos#destroyAll
         * @methodOf apiLb.Flota.motos
         *
         * @description
         *
         * Deletes all motos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.motos.destroyAll = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::delete::flota::motos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.motos#destroyById
         * @methodOf apiLb.Flota.motos
         *
         * @description
         *
         * Delete a related item by id for motos.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for motos
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.motos.destroyById = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::destroyById::flota::motos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.motos#findById
         * @methodOf apiLb.Flota.motos
         *
         * @description
         *
         * Find a related item by id for motos.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for motos
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        R.motos.findById = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::findById::flota::motos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.motos#updateById
         * @methodOf apiLb.Flota.motos
         *
         * @description
         *
         * Update a related item by id for motos.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for motos
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        R.motos.updateById = function() {
          var TargetResource = $injector.get("Moto");
          var action = TargetResource["::updateById::flota::motos"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Flota.carros
     * @header lbServices.Flota.carros
     * @object
     * @description
     *
     * The object `Flota.carros` groups methods
     * manipulating `Carro` instances related to `Flota`.
     *
     * Call {@link lbServices.Flota#carros Flota.carros()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Flota#carros
         * @methodOf apiLb.Flota
         *
         * @description
         *
         * Queries carros of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        R.carros = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::get::flota::carros"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.carros#count
         * @methodOf apiLb.Flota.carros
         *
         * @description
         *
         * Counts carros of flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.carros.count = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::count::flota::carros"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.carros#create
         * @methodOf apiLb.Flota.carros
         *
         * @description
         *
         * Creates a new instance in carros of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        R.carros.create = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::create::flota::carros"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.carros#destroyAll
         * @methodOf apiLb.Flota.carros
         *
         * @description
         *
         * Deletes all carros of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carros.destroyAll = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::delete::flota::carros"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.carros#destroyById
         * @methodOf apiLb.Flota.carros
         *
         * @description
         *
         * Delete a related item by id for carros.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carros
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carros.destroyById = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::destroyById::flota::carros"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.carros#findById
         * @methodOf apiLb.Flota.carros
         *
         * @description
         *
         * Find a related item by id for carros.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carros
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        R.carros.findById = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::findById::flota::carros"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Flota.carros#updateById
         * @methodOf apiLb.Flota.carros
         *
         * @description
         *
         * Update a related item by id for carros.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carros
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        R.carros.updateById = function() {
          var TargetResource = $injector.get("Carro");
          var action = TargetResource["::updateById::flota::carros"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Compra
 * @header apiLb.Compra
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Compra` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Compra",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/compras/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Compra#create
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/compras",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#upsert
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/compras",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#exists
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/compras/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#findById
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/compras/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#find
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/compras",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#findOne
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/compras/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#updateAll
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/compras/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#deleteById
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/compras/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#count
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/compras/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#prototype$updateAttributes
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/compras/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Compra#createChangeStream
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/compras/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Compra#updateOrCreate
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Compra` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Compra#update
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Compra#destroyById
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Compra#removeById
         * @methodOf apiLb.Compra
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Compra#modelName
    * @propertyOf apiLb.Compra
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Compra`.
    */
    R.modelName = "Compra";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Caja
 * @header apiLb.Caja
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Caja` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Caja",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/caja/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Caja#create
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/caja",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#upsert
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/caja",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#exists
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/caja/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#findById
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/caja/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#find
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/caja",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#findOne
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/caja/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#updateAll
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/caja/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#deleteById
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/caja/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#count
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/caja/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#prototype$updateAttributes
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/caja/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Caja#createChangeStream
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/caja/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Caja#updateOrCreate
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Caja` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Caja#update
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Caja#destroyById
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Caja#removeById
         * @methodOf apiLb.Caja
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Caja#modelName
    * @propertyOf apiLb.Caja
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Caja`.
    */
    R.modelName = "Caja";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Asistencia
 * @header apiLb.Asistencia
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Asistencia` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Asistencia",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/asistencia/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#create
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/asistencia",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#upsert
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/asistencia",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#exists
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/asistencia/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#findById
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/asistencia/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#find
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/asistencia",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#findOne
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/asistencia/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#updateAll
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/asistencia/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#deleteById
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/asistencia/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#count
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/asistencia/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#prototype$updateAttributes
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/asistencia/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#createChangeStream
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/asistencia/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Asistencia#updateOrCreate
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Asistencia` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#update
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#destroyById
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Asistencia#removeById
         * @methodOf apiLb.Asistencia
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Asistencia#modelName
    * @propertyOf apiLb.Asistencia
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Asistencia`.
    */
    R.modelName = "Asistencia";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Inventario
 * @header apiLb.Inventario
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Inventario` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Inventario",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/inventario/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Inventario#create
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/inventario",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#upsert
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/inventario",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#exists
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/inventario/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#findById
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/inventario/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#find
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/inventario",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#findOne
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/inventario/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#updateAll
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/inventario/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#deleteById
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/inventario/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#count
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/inventario/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#prototype$updateAttributes
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/inventario/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Inventario#createChangeStream
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/inventario/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Inventario#updateOrCreate
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Inventario` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Inventario#update
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Inventario#destroyById
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Inventario#removeById
         * @methodOf apiLb.Inventario
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Inventario#modelName
    * @propertyOf apiLb.Inventario
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Inventario`.
    */
    R.modelName = "Inventario";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Remuneracion
 * @header apiLb.Remuneracion
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Remuneracion` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Remuneracion",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/remuneracion/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Remuneracion.trabajador() instead.
        "prototype$__get__trabajador": {
          url: urlBase + "/remuneracion/:id/trabajador",
          method: "GET"
        },

        // INTERNAL. Use Remuneracion.afp() instead.
        "prototype$__get__afp": {
          url: urlBase + "/remuneracion/:id/afp",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#create
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/remuneracion",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#upsert
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/remuneracion",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#exists
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/remuneracion/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#findById
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/remuneracion/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#find
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/remuneracion",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#findOne
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/remuneracion/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#updateAll
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/remuneracion/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#deleteById
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/remuneracion/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#count
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/remuneracion/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#prototype$updateAttributes
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/remuneracion/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#createChangeStream
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/remuneracion/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.remuneracion() instead.
        "::get::trabajador::remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.remuneracion.create() instead.
        "::create::trabajador::remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.remuneracion.update() instead.
        "::update::trabajador::remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "PUT"
        },

        // INTERNAL. Use Trabajador.remuneracion.destroy() instead.
        "::destroy::trabajador::remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "DELETE"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#updateOrCreate
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#update
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#destroyById
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#removeById
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Remuneracion#modelName
    * @propertyOf apiLb.Remuneracion
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Remuneracion`.
    */
    R.modelName = "Remuneracion";


        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#trabajador
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Fetches belongsTo relation trabajador.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajador = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::get::remuneracion::trabajador"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Remuneracion#afp
         * @methodOf apiLb.Remuneracion
         *
         * @description
         *
         * Fetches belongsTo relation afp.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        R.afp = function() {
          var TargetResource = $injector.get("Afp");
          var action = TargetResource["::get::remuneracion::afp"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Persona
 * @header apiLb.Persona
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Persona` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Persona",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/personas/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Persona#create
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/personas",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#upsert
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/personas",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#exists
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/personas/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#findById
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/personas/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#find
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/personas",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#findOne
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/personas/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#updateAll
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/personas/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#deleteById
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/personas/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#count
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/personas/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#prototype$updateAttributes
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/personas/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Persona#createChangeStream
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/personas/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.persona() instead.
        "::get::trabajador::persona": {
          url: urlBase + "/trabajadores/:id/persona",
          method: "GET"
        },

        // INTERNAL. Use Cliente.type() instead.
        "::get::cliente::type": {
          url: urlBase + "/clientes/:id/type",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Persona#updateOrCreate
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Persona#update
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Persona#destroyById
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Persona#removeById
         * @methodOf apiLb.Persona
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Persona#modelName
    * @propertyOf apiLb.Persona
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Persona`.
    */
    R.modelName = "Persona";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Trabajador
 * @header apiLb.Trabajador
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Trabajador` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Trabajador",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/trabajadores/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Trabajador.user() instead.
        "prototype$__get__user": {
          url: urlBase + "/trabajadores/:id/user",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.persona() instead.
        "prototype$__get__persona": {
          url: urlBase + "/trabajadores/:id/persona",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.remuneracion() instead.
        "prototype$__get__remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.remuneracion.create() instead.
        "prototype$__create__remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.remuneracion.update() instead.
        "prototype$__update__remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "PUT"
        },

        // INTERNAL. Use Trabajador.remuneracion.destroy() instead.
        "prototype$__destroy__remuneracion": {
          url: urlBase + "/trabajadores/:id/remuneracion",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.findById() instead.
        "prototype$__findById__roles": {
          url: urlBase + "/trabajadores/:id/roles/:fk",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.roles.destroyById() instead.
        "prototype$__destroyById__roles": {
          url: urlBase + "/trabajadores/:id/roles/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.updateById() instead.
        "prototype$__updateById__roles": {
          url: urlBase + "/trabajadores/:id/roles/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Trabajador.roles.link() instead.
        "prototype$__link__roles": {
          url: urlBase + "/trabajadores/:id/roles/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Trabajador.roles.unlink() instead.
        "prototype$__unlink__roles": {
          url: urlBase + "/trabajadores/:id/roles/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.exists() instead.
        "prototype$__exists__roles": {
          url: urlBase + "/trabajadores/:id/roles/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Trabajador.cargo() instead.
        "prototype$__get__cargo": {
          url: urlBase + "/trabajadores/:id/cargo",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.roles() instead.
        "prototype$__get__roles": {
          isArray: true,
          url: urlBase + "/trabajadores/:id/roles",
          method: "GET"
        },

        // INTERNAL. Use Trabajador.roles.create() instead.
        "prototype$__create__roles": {
          url: urlBase + "/trabajadores/:id/roles",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.roles.destroyAll() instead.
        "prototype$__delete__roles": {
          url: urlBase + "/trabajadores/:id/roles",
          method: "DELETE"
        },

        // INTERNAL. Use Trabajador.roles.count() instead.
        "prototype$__count__roles": {
          url: urlBase + "/trabajadores/:id/roles/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#create
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/trabajadores",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#upsert
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/trabajadores",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#exists
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/trabajadores/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#findById
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/trabajadores/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#find
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/trabajadores",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#findOne
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/trabajadores/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#updateAll
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/trabajadores/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#deleteById
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/trabajadores/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#count
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/trabajadores/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#prototype$updateAttributes
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/trabajadores/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#createChangeStream
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/trabajadores/change-stream",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#createNew
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * create new trabajador with all dependencies
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        "createNew": {
          url: urlBase + "/trabajadores/createNew",
          method: "POST"
        },

        // INTERNAL. Use User.trabajador() instead.
        "::get::user::trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "GET"
        },

        // INTERNAL. Use User.trabajador.create() instead.
        "::create::user::trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "POST"
        },

        // INTERNAL. Use User.trabajador.update() instead.
        "::update::user::trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "PUT"
        },

        // INTERNAL. Use User.trabajador.destroy() instead.
        "::destroy::user::trabajador": {
          url: urlBase + "/users/:id/trabajador",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.findById() instead.
        "::findById::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/:fk",
          method: "GET"
        },

        // INTERNAL. Use Role.trabajadores.destroyById() instead.
        "::destroyById::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.updateById() instead.
        "::updateById::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.trabajadores.link() instead.
        "::link::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Role.trabajadores.unlink() instead.
        "::unlink::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.exists() instead.
        "::exists::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Role.trabajadores() instead.
        "::get::role::trabajadores": {
          isArray: true,
          url: urlBase + "/roles/:id/trabajadores",
          method: "GET"
        },

        // INTERNAL. Use Role.trabajadores.create() instead.
        "::create::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores",
          method: "POST"
        },

        // INTERNAL. Use Role.trabajadores.destroyAll() instead.
        "::delete::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores",
          method: "DELETE"
        },

        // INTERNAL. Use Role.trabajadores.count() instead.
        "::count::role::trabajadores": {
          url: urlBase + "/roles/:id/trabajadores/count",
          method: "GET"
        },

        // INTERNAL. Use Remuneracion.trabajador() instead.
        "::get::remuneracion::trabajador": {
          url: urlBase + "/remuneracion/:id/trabajador",
          method: "GET"
        },

        // INTERNAL. Use RoleTrabajador.trabajador() instead.
        "::get::roleTrabajador::trabajador": {
          url: urlBase + "/role-trabajador/:id/trabajador",
          method: "GET"
        },

        // INTERNAL. Use Cargo.trabajadores.findById() instead.
        "::findById::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/:fk",
          method: "GET"
        },

        // INTERNAL. Use Cargo.trabajadores.destroyById() instead.
        "::destroyById::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Cargo.trabajadores.updateById() instead.
        "::updateById::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Cargo.trabajadores.link() instead.
        "::link::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Cargo.trabajadores.unlink() instead.
        "::unlink::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Cargo.trabajadores.exists() instead.
        "::exists::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Cargo.trabajadores() instead.
        "::get::cargo::trabajadores": {
          isArray: true,
          url: urlBase + "/cargos/:id/trabajadores",
          method: "GET"
        },

        // INTERNAL. Use Cargo.trabajadores.create() instead.
        "::create::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores",
          method: "POST"
        },

        // INTERNAL. Use Cargo.trabajadores.destroyAll() instead.
        "::delete::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores",
          method: "DELETE"
        },

        // INTERNAL. Use Cargo.trabajadores.count() instead.
        "::count::cargo::trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Trabajador#updateOrCreate
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#update
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#destroyById
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#removeById
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Trabajador#modelName
    * @propertyOf apiLb.Trabajador
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Trabajador`.
    */
    R.modelName = "Trabajador";


        /**
         * @ngdoc method
         * @name apiLb.Trabajador#user
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Fetches belongsTo relation user.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.user = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::trabajador::user"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#persona
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Fetches belongsTo relation persona.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        R.persona = function() {
          var TargetResource = $injector.get("Persona");
          var action = TargetResource["::get::trabajador::persona"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Trabajador.remuneracion
     * @header lbServices.Trabajador.remuneracion
     * @object
     * @description
     *
     * The object `Trabajador.remuneracion` groups methods
     * manipulating `Remuneracion` instances related to `Trabajador`.
     *
     * Call {@link lbServices.Trabajador#remuneracion Trabajador.remuneracion()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Trabajador#remuneracion
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Fetches hasOne relation remuneracion.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        R.remuneracion = function() {
          var TargetResource = $injector.get("Remuneracion");
          var action = TargetResource["::get::trabajador::remuneracion"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.remuneracion#create
         * @methodOf apiLb.Trabajador.remuneracion
         *
         * @description
         *
         * Creates a new instance in remuneracion of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        R.remuneracion.create = function() {
          var TargetResource = $injector.get("Remuneracion");
          var action = TargetResource["::create::trabajador::remuneracion"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.remuneracion#destroy
         * @methodOf apiLb.Trabajador.remuneracion
         *
         * @description
         *
         * Deletes remuneracion of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.remuneracion.destroy = function() {
          var TargetResource = $injector.get("Remuneracion");
          var action = TargetResource["::destroy::trabajador::remuneracion"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.remuneracion#update
         * @methodOf apiLb.Trabajador.remuneracion
         *
         * @description
         *
         * Update remuneracion of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Remuneracion` object.)
         * </em>
         */
        R.remuneracion.update = function() {
          var TargetResource = $injector.get("Remuneracion");
          var action = TargetResource["::update::trabajador::remuneracion"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Trabajador.roles
     * @header lbServices.Trabajador.roles
     * @object
     * @description
     *
     * The object `Trabajador.roles` groups methods
     * manipulating `Role` instances related to `Trabajador`.
     *
     * Call {@link lbServices.Trabajador#roles Trabajador.roles()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Trabajador#roles
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Queries roles of trabajador.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::get::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#count
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Counts roles of trabajador.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.roles.count = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::count::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#create
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Creates a new instance in roles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.create = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::create::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#destroyAll
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Deletes all roles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.roles.destroyAll = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::delete::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#destroyById
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Delete a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.roles.destroyById = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::destroyById::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#exists
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Check the existence of roles relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.exists = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::exists::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#findById
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Find a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.findById = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::findById::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#link
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Add a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.link = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::link::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#unlink
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Remove the roles relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.roles.unlink = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::unlink::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador.roles#updateById
         * @methodOf apiLb.Trabajador.roles
         *
         * @description
         *
         * Update a related item by id for roles.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for roles
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.roles.updateById = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::updateById::trabajador::roles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Trabajador#cargo
         * @methodOf apiLb.Trabajador
         *
         * @description
         *
         * Fetches belongsTo relation cargo.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        R.cargo = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::get::trabajador::cargo"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Afp
 * @header apiLb.Afp
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Afp` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Afp",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/afp/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Afp#create
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/afp",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#upsert
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/afp",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#exists
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/afp/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#findById
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/afp/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#find
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/afp",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#findOne
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/afp/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#updateAll
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/afp/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#deleteById
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/afp/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#count
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/afp/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#prototype$updateAttributes
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/afp/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Afp#createChangeStream
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/afp/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Remuneracion.afp() instead.
        "::get::remuneracion::afp": {
          url: urlBase + "/remuneracion/:id/afp",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Afp#updateOrCreate
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Afp` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Afp#update
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Afp#destroyById
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Afp#removeById
         * @methodOf apiLb.Afp
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Afp#modelName
    * @propertyOf apiLb.Afp
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Afp`.
    */
    R.modelName = "Afp";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Snp
 * @header apiLb.Snp
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Snp` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Snp",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/snp/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Snp#create
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/snp",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#upsert
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/snp",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#exists
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/snp/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#findById
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/snp/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#find
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/snp",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#findOne
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/snp/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#updateAll
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/snp/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#deleteById
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/snp/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#count
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/snp/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#prototype$updateAttributes
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/snp/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Snp#createChangeStream
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/snp/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Snp#updateOrCreate
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Snp` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Snp#update
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Snp#destroyById
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Snp#removeById
         * @methodOf apiLb.Snp
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Snp#modelName
    * @propertyOf apiLb.Snp
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Snp`.
    */
    R.modelName = "Snp";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Translates
 * @header apiLb.Translates
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Translates` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Translates",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/translates/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name apiLb.Translates#create
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/translates",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#upsert
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/translates",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#exists
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/translates/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#findById
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/translates/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#find
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/translates",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#findOne
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/translates/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#updateAll
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/translates/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#deleteById
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/translates/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#count
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/translates/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#prototype$updateAttributes
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/translates/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Translates#createChangeStream
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/translates/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Translates#updateOrCreate
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Translates` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Translates#update
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Translates#destroyById
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Translates#removeById
         * @methodOf apiLb.Translates
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Translates#modelName
    * @propertyOf apiLb.Translates
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Translates`.
    */
    R.modelName = "Translates";


    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Area
 * @header apiLb.Area
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Area` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Area",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/areas/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Area.empresa() instead.
        "prototype$__get__empresa": {
          url: urlBase + "/areas/:id/empresa",
          method: "GET"
        },

        // INTERNAL. Use Area.cargos.findById() instead.
        "prototype$__findById__cargos": {
          url: urlBase + "/areas/:id/cargos/:fk",
          method: "GET"
        },

        // INTERNAL. Use Area.cargos.destroyById() instead.
        "prototype$__destroyById__cargos": {
          url: urlBase + "/areas/:id/cargos/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Area.cargos.updateById() instead.
        "prototype$__updateById__cargos": {
          url: urlBase + "/areas/:id/cargos/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Area.cargos() instead.
        "prototype$__get__cargos": {
          isArray: true,
          url: urlBase + "/areas/:id/cargos",
          method: "GET"
        },

        // INTERNAL. Use Area.cargos.create() instead.
        "prototype$__create__cargos": {
          url: urlBase + "/areas/:id/cargos",
          method: "POST"
        },

        // INTERNAL. Use Area.cargos.destroyAll() instead.
        "prototype$__delete__cargos": {
          url: urlBase + "/areas/:id/cargos",
          method: "DELETE"
        },

        // INTERNAL. Use Area.cargos.count() instead.
        "prototype$__count__cargos": {
          url: urlBase + "/areas/:id/cargos/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#create
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/areas",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#upsert
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/areas",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#exists
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/areas/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#findById
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/areas/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#find
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/areas",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#findOne
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/areas/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#updateAll
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/areas/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#deleteById
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/areas/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#count
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/areas/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#prototype$updateAttributes
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/areas/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Area#createChangeStream
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/areas/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Empresa.areas.findById() instead.
        "::findById::empresa::areas": {
          url: urlBase + "/empresas/:id/areas/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.areas.destroyById() instead.
        "::destroyById::empresa::areas": {
          url: urlBase + "/empresas/:id/areas/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.areas.updateById() instead.
        "::updateById::empresa::areas": {
          url: urlBase + "/empresas/:id/areas/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.areas() instead.
        "::get::empresa::areas": {
          isArray: true,
          url: urlBase + "/empresas/:id/areas",
          method: "GET"
        },

        // INTERNAL. Use Empresa.areas.create() instead.
        "::create::empresa::areas": {
          url: urlBase + "/empresas/:id/areas",
          method: "POST"
        },

        // INTERNAL. Use Empresa.areas.destroyAll() instead.
        "::delete::empresa::areas": {
          url: urlBase + "/empresas/:id/areas",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.areas.count() instead.
        "::count::empresa::areas": {
          url: urlBase + "/empresas/:id/areas/count",
          method: "GET"
        },

        // INTERNAL. Use Cargo.area() instead.
        "::get::cargo::area": {
          url: urlBase + "/cargos/:id/area",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Area#updateOrCreate
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Area#update
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Area#destroyById
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Area#removeById
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Area#modelName
    * @propertyOf apiLb.Area
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Area`.
    */
    R.modelName = "Area";


        /**
         * @ngdoc method
         * @name apiLb.Area#empresa
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Fetches belongsTo relation empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        R.empresa = function() {
          var TargetResource = $injector.get("Empresa");
          var action = TargetResource["::get::area::empresa"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Area.cargos
     * @header lbServices.Area.cargos
     * @object
     * @description
     *
     * The object `Area.cargos` groups methods
     * manipulating `Cargo` instances related to `Area`.
     *
     * Call {@link lbServices.Area#cargos Area.cargos()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Area#cargos
         * @methodOf apiLb.Area
         *
         * @description
         *
         * Queries cargos of area.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        R.cargos = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::get::area::cargos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Area.cargos#count
         * @methodOf apiLb.Area.cargos
         *
         * @description
         *
         * Counts cargos of area.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.cargos.count = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::count::area::cargos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Area.cargos#create
         * @methodOf apiLb.Area.cargos
         *
         * @description
         *
         * Creates a new instance in cargos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        R.cargos.create = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::create::area::cargos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Area.cargos#destroyAll
         * @methodOf apiLb.Area.cargos
         *
         * @description
         *
         * Deletes all cargos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.cargos.destroyAll = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::delete::area::cargos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Area.cargos#destroyById
         * @methodOf apiLb.Area.cargos
         *
         * @description
         *
         * Delete a related item by id for cargos.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for cargos
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.cargos.destroyById = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::destroyById::area::cargos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Area.cargos#findById
         * @methodOf apiLb.Area.cargos
         *
         * @description
         *
         * Find a related item by id for cargos.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for cargos
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        R.cargos.findById = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::findById::area::cargos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Area.cargos#updateById
         * @methodOf apiLb.Area.cargos
         *
         * @description
         *
         * Update a related item by id for cargos.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for cargos
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        R.cargos.updateById = function() {
          var TargetResource = $injector.get("Cargo");
          var action = TargetResource["::updateById::area::cargos"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.RoleTrabajador
 * @header apiLb.RoleTrabajador
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `RoleTrabajador` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "RoleTrabajador",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/role-trabajador/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use RoleTrabajador.trabajador() instead.
        "prototype$__get__trabajador": {
          url: urlBase + "/role-trabajador/:id/trabajador",
          method: "GET"
        },

        // INTERNAL. Use RoleTrabajador.role() instead.
        "prototype$__get__role": {
          url: urlBase + "/role-trabajador/:id/role",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#create
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/role-trabajador",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#upsert
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/role-trabajador",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#exists
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/role-trabajador/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#findById
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/role-trabajador/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#find
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/role-trabajador",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#findOne
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/role-trabajador/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#updateAll
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/role-trabajador/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#deleteById
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/role-trabajador/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#count
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/role-trabajador/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#prototype$updateAttributes
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/role-trabajador/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#createChangeStream
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/role-trabajador/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#updateOrCreate
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RoleTrabajador` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#update
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#destroyById
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#removeById
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.RoleTrabajador#modelName
    * @propertyOf apiLb.RoleTrabajador
    * @description
    * The name of the model represented by this $resource,
    * i.e. `RoleTrabajador`.
    */
    R.modelName = "RoleTrabajador";


        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#trabajador
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Fetches belongsTo relation trabajador.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajador = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::get::roleTrabajador::trabajador"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.RoleTrabajador#role
         * @methodOf apiLb.RoleTrabajador
         *
         * @description
         *
         * Fetches belongsTo relation role.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Role` object.)
         * </em>
         */
        R.role = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::get::roleTrabajador::role"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Sucursal
 * @header apiLb.Sucursal
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Sucursal` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Sucursal",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/sucursales/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Sucursal.empresa() instead.
        "prototype$__get__empresa": {
          url: urlBase + "/sucursales/:id/empresa",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.almacenes.findById() instead.
        "prototype$__findById__almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/:fk",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.almacenes.destroyById() instead.
        "prototype$__destroyById__almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Sucursal.almacenes.updateById() instead.
        "prototype$__updateById__almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Sucursal.almacenes() instead.
        "prototype$__get__almacenes": {
          isArray: true,
          url: urlBase + "/sucursales/:id/almacenes",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.almacenes.create() instead.
        "prototype$__create__almacenes": {
          url: urlBase + "/sucursales/:id/almacenes",
          method: "POST"
        },

        // INTERNAL. Use Sucursal.almacenes.destroyAll() instead.
        "prototype$__delete__almacenes": {
          url: urlBase + "/sucursales/:id/almacenes",
          method: "DELETE"
        },

        // INTERNAL. Use Sucursal.almacenes.count() instead.
        "prototype$__count__almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#create
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/sucursales",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#upsert
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/sucursales",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#exists
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/sucursales/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#findById
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/sucursales/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#find
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/sucursales",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#findOne
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/sucursales/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#updateAll
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/sucursales/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#deleteById
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/sucursales/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#count
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/sucursales/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#prototype$updateAttributes
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/sucursales/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#createChangeStream
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/sucursales/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Empresa.sucursales.findById() instead.
        "::findById::empresa::sucursales": {
          url: urlBase + "/empresas/:id/sucursales/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.sucursales.destroyById() instead.
        "::destroyById::empresa::sucursales": {
          url: urlBase + "/empresas/:id/sucursales/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.sucursales.updateById() instead.
        "::updateById::empresa::sucursales": {
          url: urlBase + "/empresas/:id/sucursales/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.sucursales() instead.
        "::get::empresa::sucursales": {
          isArray: true,
          url: urlBase + "/empresas/:id/sucursales",
          method: "GET"
        },

        // INTERNAL. Use Empresa.sucursales.create() instead.
        "::create::empresa::sucursales": {
          url: urlBase + "/empresas/:id/sucursales",
          method: "POST"
        },

        // INTERNAL. Use Empresa.sucursales.destroyAll() instead.
        "::delete::empresa::sucursales": {
          url: urlBase + "/empresas/:id/sucursales",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.sucursales.count() instead.
        "::count::empresa::sucursales": {
          url: urlBase + "/empresas/:id/sucursales/count",
          method: "GET"
        },

        // INTERNAL. Use Almacen.sucursal() instead.
        "::get::almacen::sucursal": {
          url: urlBase + "/almacenes/:id/sucursal",
          method: "GET"
        },

        // INTERNAL. Use Almacen.empresa() instead.
        "::get::almacen::empresa": {
          url: urlBase + "/almacenes/:id/empresa",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Sucursal#updateOrCreate
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#update
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#destroyById
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Sucursal#removeById
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Sucursal#modelName
    * @propertyOf apiLb.Sucursal
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Sucursal`.
    */
    R.modelName = "Sucursal";


        /**
         * @ngdoc method
         * @name apiLb.Sucursal#empresa
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Fetches belongsTo relation empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Empresa` object.)
         * </em>
         */
        R.empresa = function() {
          var TargetResource = $injector.get("Empresa");
          var action = TargetResource["::get::sucursal::empresa"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Sucursal.almacenes
     * @header lbServices.Sucursal.almacenes
     * @object
     * @description
     *
     * The object `Sucursal.almacenes` groups methods
     * manipulating `Almacen` instances related to `Sucursal`.
     *
     * Call {@link lbServices.Sucursal#almacenes Sucursal.almacenes()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Sucursal#almacenes
         * @methodOf apiLb.Sucursal
         *
         * @description
         *
         * Queries almacenes of sucursal.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::get::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Sucursal.almacenes#count
         * @methodOf apiLb.Sucursal.almacenes
         *
         * @description
         *
         * Counts almacenes of sucursal.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.almacenes.count = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::count::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Sucursal.almacenes#create
         * @methodOf apiLb.Sucursal.almacenes
         *
         * @description
         *
         * Creates a new instance in almacenes of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes.create = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::create::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Sucursal.almacenes#destroyAll
         * @methodOf apiLb.Sucursal.almacenes
         *
         * @description
         *
         * Deletes all almacenes of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.almacenes.destroyAll = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::delete::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Sucursal.almacenes#destroyById
         * @methodOf apiLb.Sucursal.almacenes
         *
         * @description
         *
         * Delete a related item by id for almacenes.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for almacenes
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.almacenes.destroyById = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::destroyById::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Sucursal.almacenes#findById
         * @methodOf apiLb.Sucursal.almacenes
         *
         * @description
         *
         * Find a related item by id for almacenes.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for almacenes
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes.findById = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::findById::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Sucursal.almacenes#updateById
         * @methodOf apiLb.Sucursal.almacenes
         *
         * @description
         *
         * Update a related item by id for almacenes.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for almacenes
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R.almacenes.updateById = function() {
          var TargetResource = $injector.get("Almacen");
          var action = TargetResource["::updateById::sucursal::almacenes"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Almacen
 * @header apiLb.Almacen
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Almacen` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Almacen",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/almacenes/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Almacen.sucursal() instead.
        "prototype$__get__sucursal": {
          url: urlBase + "/almacenes/:id/sucursal",
          method: "GET"
        },

        // INTERNAL. Use Almacen.empresa() instead.
        "prototype$__get__empresa": {
          url: urlBase + "/almacenes/:id/empresa",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#create
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/almacenes",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#upsert
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/almacenes",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#exists
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/almacenes/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#findById
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/almacenes/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#find
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/almacenes",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#findOne
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/almacenes/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#updateAll
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/almacenes/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#deleteById
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/almacenes/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#count
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/almacenes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#prototype$updateAttributes
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/almacenes/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Almacen#createChangeStream
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/almacenes/change-stream",
          method: "POST"
        },

        // INTERNAL. Use AgregadoAsignacion.almacen() instead.
        "::get::agregadoAsignacion::almacen": {
          url: urlBase + "/agregado-asignacion/:id/almacen",
          method: "GET"
        },

        // INTERNAL. Use Empresa.almacenes.findById() instead.
        "::findById::empresa::almacenes": {
          url: urlBase + "/empresas/:id/almacenes/:fk",
          method: "GET"
        },

        // INTERNAL. Use Empresa.almacenes.destroyById() instead.
        "::destroyById::empresa::almacenes": {
          url: urlBase + "/empresas/:id/almacenes/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.almacenes.updateById() instead.
        "::updateById::empresa::almacenes": {
          url: urlBase + "/empresas/:id/almacenes/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Empresa.almacenes() instead.
        "::get::empresa::almacenes": {
          isArray: true,
          url: urlBase + "/empresas/:id/almacenes",
          method: "GET"
        },

        // INTERNAL. Use Empresa.almacenes.create() instead.
        "::create::empresa::almacenes": {
          url: urlBase + "/empresas/:id/almacenes",
          method: "POST"
        },

        // INTERNAL. Use Empresa.almacenes.destroyAll() instead.
        "::delete::empresa::almacenes": {
          url: urlBase + "/empresas/:id/almacenes",
          method: "DELETE"
        },

        // INTERNAL. Use Empresa.almacenes.count() instead.
        "::count::empresa::almacenes": {
          url: urlBase + "/empresas/:id/almacenes/count",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.almacenes.findById() instead.
        "::findById::sucursal::almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/:fk",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.almacenes.destroyById() instead.
        "::destroyById::sucursal::almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Sucursal.almacenes.updateById() instead.
        "::updateById::sucursal::almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Sucursal.almacenes() instead.
        "::get::sucursal::almacenes": {
          isArray: true,
          url: urlBase + "/sucursales/:id/almacenes",
          method: "GET"
        },

        // INTERNAL. Use Sucursal.almacenes.create() instead.
        "::create::sucursal::almacenes": {
          url: urlBase + "/sucursales/:id/almacenes",
          method: "POST"
        },

        // INTERNAL. Use Sucursal.almacenes.destroyAll() instead.
        "::delete::sucursal::almacenes": {
          url: urlBase + "/sucursales/:id/almacenes",
          method: "DELETE"
        },

        // INTERNAL. Use Sucursal.almacenes.count() instead.
        "::count::sucursal::almacenes": {
          url: urlBase + "/sucursales/:id/almacenes/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Almacen#updateOrCreate
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Almacen` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Almacen#update
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Almacen#destroyById
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Almacen#removeById
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Almacen#modelName
    * @propertyOf apiLb.Almacen
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Almacen`.
    */
    R.modelName = "Almacen";


        /**
         * @ngdoc method
         * @name apiLb.Almacen#sucursal
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Fetches belongsTo relation sucursal.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R.sucursal = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::get::almacen::sucursal"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Almacen#empresa
         * @methodOf apiLb.Almacen
         *
         * @description
         *
         * Fetches belongsTo relation empresa.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Sucursal` object.)
         * </em>
         */
        R.empresa = function() {
          var TargetResource = $injector.get("Sucursal");
          var action = TargetResource["::get::almacen::empresa"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Cargo
 * @header apiLb.Cargo
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Cargo` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Cargo",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/cargos/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Cargo.area() instead.
        "prototype$__get__area": {
          url: urlBase + "/cargos/:id/area",
          method: "GET"
        },

        // INTERNAL. Use Cargo.trabajadores.findById() instead.
        "prototype$__findById__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/:fk",
          method: "GET"
        },

        // INTERNAL. Use Cargo.trabajadores.destroyById() instead.
        "prototype$__destroyById__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Cargo.trabajadores.updateById() instead.
        "prototype$__updateById__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Cargo.trabajadores.link() instead.
        "prototype$__link__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/rel/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Cargo.trabajadores.unlink() instead.
        "prototype$__unlink__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/rel/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Cargo.trabajadores.exists() instead.
        "prototype$__exists__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/rel/:fk",
          method: "HEAD"
        },

        // INTERNAL. Use Cargo.trabajadores() instead.
        "prototype$__get__trabajadores": {
          isArray: true,
          url: urlBase + "/cargos/:id/trabajadores",
          method: "GET"
        },

        // INTERNAL. Use Cargo.trabajadores.create() instead.
        "prototype$__create__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores",
          method: "POST"
        },

        // INTERNAL. Use Cargo.trabajadores.destroyAll() instead.
        "prototype$__delete__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores",
          method: "DELETE"
        },

        // INTERNAL. Use Cargo.trabajadores.count() instead.
        "prototype$__count__trabajadores": {
          url: urlBase + "/cargos/:id/trabajadores/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#create
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/cargos",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#upsert
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/cargos",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#exists
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/cargos/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#findById
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/cargos/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#find
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/cargos",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#findOne
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/cargos/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#updateAll
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/cargos/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#deleteById
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/cargos/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#count
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/cargos/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#prototype$updateAttributes
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/cargos/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cargo#createChangeStream
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/cargos/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Trabajador.cargo() instead.
        "::get::trabajador::cargo": {
          url: urlBase + "/trabajadores/:id/cargo",
          method: "GET"
        },

        // INTERNAL. Use Area.cargos.findById() instead.
        "::findById::area::cargos": {
          url: urlBase + "/areas/:id/cargos/:fk",
          method: "GET"
        },

        // INTERNAL. Use Area.cargos.destroyById() instead.
        "::destroyById::area::cargos": {
          url: urlBase + "/areas/:id/cargos/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Area.cargos.updateById() instead.
        "::updateById::area::cargos": {
          url: urlBase + "/areas/:id/cargos/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Area.cargos() instead.
        "::get::area::cargos": {
          isArray: true,
          url: urlBase + "/areas/:id/cargos",
          method: "GET"
        },

        // INTERNAL. Use Area.cargos.create() instead.
        "::create::area::cargos": {
          url: urlBase + "/areas/:id/cargos",
          method: "POST"
        },

        // INTERNAL. Use Area.cargos.destroyAll() instead.
        "::delete::area::cargos": {
          url: urlBase + "/areas/:id/cargos",
          method: "DELETE"
        },

        // INTERNAL. Use Area.cargos.count() instead.
        "::count::area::cargos": {
          url: urlBase + "/areas/:id/cargos/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Cargo#updateOrCreate
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cargo` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Cargo#update
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Cargo#destroyById
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Cargo#removeById
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Cargo#modelName
    * @propertyOf apiLb.Cargo
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Cargo`.
    */
    R.modelName = "Cargo";


        /**
         * @ngdoc method
         * @name apiLb.Cargo#area
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Fetches belongsTo relation area.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Area` object.)
         * </em>
         */
        R.area = function() {
          var TargetResource = $injector.get("Area");
          var action = TargetResource["::get::cargo::area"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Cargo.trabajadores
     * @header lbServices.Cargo.trabajadores
     * @object
     * @description
     *
     * The object `Cargo.trabajadores` groups methods
     * manipulating `Trabajador` instances related to `Cargo`.
     *
     * Call {@link lbServices.Cargo#trabajadores Cargo.trabajadores()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name apiLb.Cargo#trabajadores
         * @methodOf apiLb.Cargo
         *
         * @description
         *
         * Queries trabajadores of cargo.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::get::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#count
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Counts trabajadores of cargo.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.trabajadores.count = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::count::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#create
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Creates a new instance in trabajadores of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.create = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::create::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#destroyAll
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Deletes all trabajadores of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajadores.destroyAll = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::delete::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#destroyById
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Delete a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajadores.destroyById = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::destroyById::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#exists
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Check the existence of trabajadores relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.exists = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::exists::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#findById
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Find a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.findById = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::findById::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#link
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Add a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.link = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::link::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#unlink
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Remove the trabajadores relation to an item by id.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trabajadores.unlink = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::unlink::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.Cargo.trabajadores#updateById
         * @methodOf apiLb.Cargo.trabajadores
         *
         * @description
         *
         * Update a related item by id for trabajadores.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trabajadores
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trabajador` object.)
         * </em>
         */
        R.trabajadores.updateById = function() {
          var TargetResource = $injector.get("Trabajador");
          var action = TargetResource["::updateById::cargo::trabajadores"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Camion
 * @header apiLb.Camion
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Camion` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Camion",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/camiones/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Camion.flota() instead.
        "prototype$__get__flota": {
          url: urlBase + "/camiones/:id/flota",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#create
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/camiones",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#upsert
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/camiones",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#exists
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/camiones/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#findById
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/camiones/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#find
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/camiones",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#findOne
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/camiones/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#updateAll
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/camiones/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#deleteById
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/camiones/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#count
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/camiones/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#prototype$updateAttributes
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/camiones/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Camion#createChangeStream
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/camiones/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Flota.camiones.findById() instead.
        "::findById::flota::camiones": {
          url: urlBase + "/flota/:id/camiones/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.camiones.destroyById() instead.
        "::destroyById::flota::camiones": {
          url: urlBase + "/flota/:id/camiones/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.camiones.updateById() instead.
        "::updateById::flota::camiones": {
          url: urlBase + "/flota/:id/camiones/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.camiones() instead.
        "::get::flota::camiones": {
          isArray: true,
          url: urlBase + "/flota/:id/camiones",
          method: "GET"
        },

        // INTERNAL. Use Flota.camiones.create() instead.
        "::create::flota::camiones": {
          url: urlBase + "/flota/:id/camiones",
          method: "POST"
        },

        // INTERNAL. Use Flota.camiones.destroyAll() instead.
        "::delete::flota::camiones": {
          url: urlBase + "/flota/:id/camiones",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.camiones.count() instead.
        "::count::flota::camiones": {
          url: urlBase + "/flota/:id/camiones/count",
          method: "GET"
        },

        // INTERNAL. Use FlotaMapping.camion() instead.
        "::get::flotaMapping::camion": {
          url: urlBase + "/flotaMapping/:id/camion",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Camion#updateOrCreate
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Camion#update
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Camion#destroyById
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Camion#removeById
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Camion#modelName
    * @propertyOf apiLb.Camion
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Camion`.
    */
    R.modelName = "Camion";


        /**
         * @ngdoc method
         * @name apiLb.Camion#flota
         * @methodOf apiLb.Camion
         *
         * @description
         *
         * Fetches belongsTo relation flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flota = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::get::camion::flota"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.FlotaMapping
 * @header apiLb.FlotaMapping
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `FlotaMapping` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "FlotaMapping",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/flotaMapping/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use FlotaMapping.flota() instead.
        "prototype$__get__flota": {
          url: urlBase + "/flotaMapping/:id/flota",
          method: "GET"
        },

        // INTERNAL. Use FlotaMapping.camion() instead.
        "prototype$__get__camion": {
          url: urlBase + "/flotaMapping/:id/camion",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#create
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/flotaMapping",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#upsert
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/flotaMapping",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#exists
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/flotaMapping/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#findById
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/flotaMapping/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#find
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/flotaMapping",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#findOne
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/flotaMapping/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#updateAll
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/flotaMapping/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#deleteById
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/flotaMapping/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#count
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/flotaMapping/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#prototype$updateAttributes
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/flotaMapping/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#createChangeStream
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/flotaMapping/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#updateOrCreate
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `FlotaMapping` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#update
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#destroyById
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#removeById
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.FlotaMapping#modelName
    * @propertyOf apiLb.FlotaMapping
    * @description
    * The name of the model represented by this $resource,
    * i.e. `FlotaMapping`.
    */
    R.modelName = "FlotaMapping";


        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#flota
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Fetches belongsTo relation flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flota = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::get::flotaMapping::flota"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name apiLb.FlotaMapping#camion
         * @methodOf apiLb.FlotaMapping
         *
         * @description
         *
         * Fetches belongsTo relation camion.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Camion` object.)
         * </em>
         */
        R.camion = function() {
          var TargetResource = $injector.get("Camion");
          var action = TargetResource["::get::flotaMapping::camion"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Maquinaria
 * @header apiLb.Maquinaria
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Maquinaria` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Maquinaria",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/maquinaria/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Maquinaria.flota() instead.
        "prototype$__get__flota": {
          url: urlBase + "/maquinaria/:id/flota",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#create
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/maquinaria",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#upsert
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/maquinaria",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#exists
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/maquinaria/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#findById
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/maquinaria/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#find
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/maquinaria",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#findOne
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/maquinaria/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#updateAll
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/maquinaria/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#deleteById
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/maquinaria/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#count
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/maquinaria/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#prototype$updateAttributes
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/maquinaria/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#createChangeStream
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/maquinaria/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Flota.maquinaria.findById() instead.
        "::findById::flota::maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.maquinaria.destroyById() instead.
        "::destroyById::flota::maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.maquinaria.updateById() instead.
        "::updateById::flota::maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.maquinaria() instead.
        "::get::flota::maquinaria": {
          isArray: true,
          url: urlBase + "/flota/:id/maquinaria",
          method: "GET"
        },

        // INTERNAL. Use Flota.maquinaria.create() instead.
        "::create::flota::maquinaria": {
          url: urlBase + "/flota/:id/maquinaria",
          method: "POST"
        },

        // INTERNAL. Use Flota.maquinaria.destroyAll() instead.
        "::delete::flota::maquinaria": {
          url: urlBase + "/flota/:id/maquinaria",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.maquinaria.count() instead.
        "::count::flota::maquinaria": {
          url: urlBase + "/flota/:id/maquinaria/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#updateOrCreate
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Maquinaria` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#update
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#destroyById
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#removeById
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Maquinaria#modelName
    * @propertyOf apiLb.Maquinaria
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Maquinaria`.
    */
    R.modelName = "Maquinaria";


        /**
         * @ngdoc method
         * @name apiLb.Maquinaria#flota
         * @methodOf apiLb.Maquinaria
         *
         * @description
         *
         * Fetches belongsTo relation flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flota = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::get::maquinaria::flota"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Moto
 * @header apiLb.Moto
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Moto` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Moto",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/motos/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Moto.flota() instead.
        "prototype$__get__flota": {
          url: urlBase + "/motos/:id/flota",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#create
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/motos",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#upsert
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/motos",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#exists
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/motos/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#findById
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/motos/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#find
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/motos",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#findOne
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/motos/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#updateAll
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/motos/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#deleteById
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/motos/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#count
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/motos/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#prototype$updateAttributes
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/motos/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Moto#createChangeStream
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/motos/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Flota.motos.findById() instead.
        "::findById::flota::motos": {
          url: urlBase + "/flota/:id/motos/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.motos.destroyById() instead.
        "::destroyById::flota::motos": {
          url: urlBase + "/flota/:id/motos/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.motos.updateById() instead.
        "::updateById::flota::motos": {
          url: urlBase + "/flota/:id/motos/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.motos() instead.
        "::get::flota::motos": {
          isArray: true,
          url: urlBase + "/flota/:id/motos",
          method: "GET"
        },

        // INTERNAL. Use Flota.motos.create() instead.
        "::create::flota::motos": {
          url: urlBase + "/flota/:id/motos",
          method: "POST"
        },

        // INTERNAL. Use Flota.motos.destroyAll() instead.
        "::delete::flota::motos": {
          url: urlBase + "/flota/:id/motos",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.motos.count() instead.
        "::count::flota::motos": {
          url: urlBase + "/flota/:id/motos/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Moto#updateOrCreate
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Moto` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Moto#update
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Moto#destroyById
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Moto#removeById
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Moto#modelName
    * @propertyOf apiLb.Moto
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Moto`.
    */
    R.modelName = "Moto";


        /**
         * @ngdoc method
         * @name apiLb.Moto#flota
         * @methodOf apiLb.Moto
         *
         * @description
         *
         * Fetches belongsTo relation flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flota = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::get::moto::flota"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Carro
 * @header apiLb.Carro
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Carro` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Carro",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/carros/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Carro.flota() instead.
        "prototype$__get__flota": {
          url: urlBase + "/carros/:id/flota",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#create
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/carros",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#upsert
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/carros",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#exists
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/carros/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#findById
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/carros/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#find
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/carros",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#findOne
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/carros/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#updateAll
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/carros/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#deleteById
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/carros/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#count
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/carros/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#prototype$updateAttributes
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/carros/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Carro#createChangeStream
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/carros/change-stream",
          method: "POST"
        },

        // INTERNAL. Use Flota.carros.findById() instead.
        "::findById::flota::carros": {
          url: urlBase + "/flota/:id/carros/:fk",
          method: "GET"
        },

        // INTERNAL. Use Flota.carros.destroyById() instead.
        "::destroyById::flota::carros": {
          url: urlBase + "/flota/:id/carros/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.carros.updateById() instead.
        "::updateById::flota::carros": {
          url: urlBase + "/flota/:id/carros/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Flota.carros() instead.
        "::get::flota::carros": {
          isArray: true,
          url: urlBase + "/flota/:id/carros",
          method: "GET"
        },

        // INTERNAL. Use Flota.carros.create() instead.
        "::create::flota::carros": {
          url: urlBase + "/flota/:id/carros",
          method: "POST"
        },

        // INTERNAL. Use Flota.carros.destroyAll() instead.
        "::delete::flota::carros": {
          url: urlBase + "/flota/:id/carros",
          method: "DELETE"
        },

        // INTERNAL. Use Flota.carros.count() instead.
        "::count::flota::carros": {
          url: urlBase + "/flota/:id/carros/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Carro#updateOrCreate
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Carro` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Carro#update
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Carro#destroyById
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Carro#removeById
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Carro#modelName
    * @propertyOf apiLb.Carro
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Carro`.
    */
    R.modelName = "Carro";


        /**
         * @ngdoc method
         * @name apiLb.Carro#flota
         * @methodOf apiLb.Carro
         *
         * @description
         *
         * Fetches belongsTo relation flota.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Flota` object.)
         * </em>
         */
        R.flota = function() {
          var TargetResource = $injector.get("Flota");
          var action = TargetResource["::get::carro::flota"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name apiLb.Cliente
 * @header apiLb.Cliente
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Cliente` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Cliente",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/clientes/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Cliente.type() instead.
        "prototype$__get__type": {
          url: urlBase + "/clientes/:id/type",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#create
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/clientes",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#upsert
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/clientes",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#exists
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/clientes/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#findById
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/clientes/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#find
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/clientes",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#findOne
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/clientes/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#updateAll
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/clientes/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#deleteById
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/clientes/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#count
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/clientes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#prototype$updateAttributes
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/clientes/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name apiLb.Cliente#createChangeStream
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Create a change stream.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `options` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `changes` – `{ReadableStream=}` - 
         */
        "createChangeStream": {
          url: urlBase + "/clientes/change-stream",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name apiLb.Cliente#updateOrCreate
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cliente` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name apiLb.Cliente#update
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name apiLb.Cliente#destroyById
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name apiLb.Cliente#removeById
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name apiLb.Cliente#modelName
    * @propertyOf apiLb.Cliente
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Cliente`.
    */
    R.modelName = "Cliente";


        /**
         * @ngdoc method
         * @name apiLb.Cliente#type
         * @methodOf apiLb.Cliente
         *
         * @description
         *
         * Fetches belongsTo relation type.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Persona` object.)
         * </em>
         */
        R.type = function() {
          var TargetResource = $injector.get("Persona");
          var action = TargetResource["::get::cliente::type"];
          return action.apply(R, arguments);
        };

    return R;
  }]);


module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    }

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = propsPrefix + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {

          // filter out non urlBase requests
          if (config.url.substr(0, urlBase.length) !== urlBase) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 } },
              status: 401,
              config: config,
              headers: function() { return undefined; }
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        }
      }
    }])

  /**
   * @ngdoc object
   * @name apiLb.LoopBackResourceProvider
   * @header apiLb.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name apiLb.LoopBackResourceProvider#setAuthHeader
     * @methodOf apiLb.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name apiLb.LoopBackResourceProvider#setUrlBase
     * @methodOf apiLb.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
    };

    this.$get = ['$resource', function($resource) {
      return function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };
    }];
  });

})(window, window.angular);

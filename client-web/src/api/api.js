'use strict';

require('./api-lb.js');

module.exports = angular
  .module('apiConsama', [
    require('ui.router'),
    'apiLb'
  ])
  .config(config)
  //.run(run)
  .factory('apiConsama', apiConsama)
  //.factory('pubsubClient', pubsubClientFactoryFn)
  .value('apiConsamaOffline', apiConsamaOfflineValue());

var apiUrlBase = 'http://localhost:3000';
var apiUrl = 'http://localhost:3000/api/v1';

function config(LoopBackResourceProvider, $provide) {

  LoopBackResourceProvider.setUrlBase(apiUrl);

}
config.$inject = ['LoopBackResourceProvider', '$provide'];

function run(pubsubClient, $state) {

  //pubsubClient.ready(function() {console.log('connection w api is ready')});
  //pubsubClient.end(function(err) {console.log('connection w api has ended')});

}
run.$inject = ['pubsubClient', '$state'];

function pubsubClientFactoryFn() {
  return require('./api-pubsub.js')(3000);
}
pubsubClientFactoryFn.$inject = [];

function apiConsama($q, $translate) {

  return {
    url: apiUrl,
    urlBase: apiUrlBase,
    getPhoto: getPhoto,
    handlerMessages: handlerMessages
  };

  function getPhoto(container, file, type) {
    var baseFile = file.substring(0, file.lastIndexOf('.'));
    var ext = file.substring(file.lastIndexOf('.'));
    return apiUrl + '/' + 'pictures' + '/' + container + '/' +
      'download' + '/' + baseFile + '-' + (type || 'standard') + '.jpg';//ext;
  }

  function handlerMessages(data) {
    //console.warn('check this', data);
    if (angular.isUndefined(data.data.error)) return $q.when();

    let msg = {};
    let res;

    if (data.data.error && angular.isUndefined(data.data.error.details)) {
      try {
        msg = JSON.parse(data.data.error.message);
      } catch (e) {
        msg = {
          property: '',
          messages: ''
        };
      };

    }else {

      res = {message: '', messages: []};
      var tmpMessage = {};

      angular.forEach(data.data.error.details.messages,
        function(messages, property) {
        tmpMessage = {};
        tmpMessage.property = property;
        tmpMessage.messages = messages;
        res.messages.push(angular.extend({}, tmpMessage));
      });

      res.message = (/ /.test(res.messages[0].messages[0])) ?
        parseMessageDefaultApi(res.messages[0]) :
        JSON.parse(res.messages[0].messages[0]);

      msg = res.message;

    }

    let prop = ('FIELDS.' + (msg.property || 'NN')).toUpperCase();
    let modelName = ('MODEL.' + (msg.model || 'NN')).toUpperCase();

    return $translate([prop, modelName])

      .then((t) => {
        return $translate(msg.code, {
          property: t[prop],
          modelName: t[modelName],
          field: t[prop]
        });
      })

      .then((t) => {
        return {msg: t, details: res};
      });

  }

}
apiConsama.$inject =  ['$q', '$translate'];

function apiConsamaOfflineValue() {

  return {
    langFallback: 'en',
    preferredLanguage: 'en',
    langsAvaibles: ['en', 'es']
  };

}

function parseMessageDefaultApi(fieldErr) {

  let res = {
    code: 'FORM.FIELD_SUBMITED_ERROR.ERROR_DEFAULT',
    property: fieldErr.property
  };

  if (
    fieldErr.messages[0] === 'can\'t be blank'
  ) res.code = 'FORM.FIELD_SUBMITED_ERROR.REQUIRED';
  return res;

}

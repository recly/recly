'use strict';

module.exports = angular
  .module('asistenciaList', [
    require('../utils/find-by.factory').name
  ])
  .directive('asistencia', asistenciaListDirectiveFn);

function asistenciaListDirectiveFn(layoutService, Asistencia, $q) {

  return {
    restrict: 'E',
    scope: {
      module: '='
    },
    template: require('./templates/asistencia.jade')(),
    controller: angular.noop,
    controllerAs: 'asistenciaList',
    bindToController: true,
    link: link
  }

  function link(scope, element, attrs, asistenciaList) {

    asistenciaList.today = Date.now();

    //---
    var moduleResource = Asistencia;
    var moduleController = asistenciaList;

    moduleController.data = function() {
      var deferred = $q.defer();
      moduleResource.find().$promise
        .then(function(data) {
          deferred.resolve({
            items: data,
            pattern: layoutService.logic.listPattern(moduleController.name)
          });
        }, function(err) {
          console.error(err);
          deferred.reject(err);
        });
      return deferred.promise;
    }

    moduleController.showItem = function(evt, item, items) {
      layoutService.logic.showItem(scope, moduleController, evt, item, items);
    };

    moduleController.deleteItem = function deleteItem(evt, item, items) {
      layoutService.logic.deleteItem(scope, moduleController, evt, item, items,
        exeDelete);
    }

    moduleController.updateItem = function(evt, item, items, fromSN) {
      layoutService.logic.updateItem(scope, moduleController, evt, item, items,
        fromSN);
    };

    moduleController.createItem = function(evt) {
      layoutService.logic.createItem(scope, moduleController, evt);
    };

    //--

    moduleController.toggleCard = layoutService
      .logic.toggleCard.bind(this, moduleController, scope);

    moduleController.toggleTypeListCard = layoutService
      .logic.toggleTypeListCard.bind(this, moduleController, scope);

    function exeDelete(item) {
      return moduleResource.destroyById({id: item.id}).$promise;
    }

  }

}
asistenciaListDirectiveFn.$inject = ['layoutService', 'Asistencia', '$q'];

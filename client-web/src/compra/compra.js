'use strict';

module.exports = angular
  .module('compra', [
    require('./compra-list.directive.js').name
  ]);

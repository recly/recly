var path = require('path');
var Webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var srcPath = path.join(__dirname, 'src');
var buildPath = path.resolve(__dirname, 'public', 'build');

var npmPath = path.join(__dirname, 'node_modules');
var bowerPath = path.join(__dirname, 'bower_components');

var port = process.env.PORT || 8081;
var DEV = JSON.parse(process.env.BUILD_DEV || 'true');
var DEPLOY = JSON.parse(process.env.BUILD_DEPLOY || 'false');

var config = {
  addVendor: function(name, path_, isParse) {
    this.resolve.alias[name] = path_;
    if (!isParse) this.module.noParse.push(new RegExp(path_));
  },
  context: srcPath,
  debug: true,
  devtool: 'cheap-source-map',
  entry: {
    app: [
      //'webpack/hot/dev-server',
      //'webpack-dev-server/client?http://localhost:' + port,
      './app.js'
    ],
    agregado: './agregado/agregado.js',
    area: './area/area.js',
    cargo: './cargo/cargo.js',
    dashboard: './dashboard',
    login: './login',
    'api-consama-offline':  './offline/layout-api-consama-offline.module.js',
    asistencia: './asistencia/asistencia.js',
    caja: './caja/caja.js',
    compra: './compra/compra.js',
    cliente: './cliente/cliente.js',
    empresa: './empresa/empresa.js',
    flota: './flota/flota.js',
    inventario: './inventario/inventario.js',
    remuneracion: './remuneracion/remuneracion.js',
    role: './role/role.js',
    settings: './settings/settings.js',
    sucursal: './sucursal/sucursal.js',
    trabajador: './trabajador/trabajador.js',
    translate: './translate/translate.js',
    'unidad-medida': './unidad-medida/unidad-medida.js',
    user: './user/user.js',
    log: './log/log.js',
    venta: './venta/venta.js',

    vendor: [
      'angular',
      './api/api.js'
    ]
  },
  output: {
    filename: '[name].bundle.js',
    path: buildPath,
    publicPath: '/build/',
    chunkFilename: '[id]-chunk.js',
    pathinfo: true
  },
  module: {
    noParse: [],
    preLoaders: [
      {test: /\.js$/, loaders: ['jscs'],
        exclude: /node_modules|bower_components|api-lb/},
      {test: /\.js$/, loaders: ['source-map']}
    ],
    loaders: [
      {test: /\.js$/, loader: 'babel', exclude: /node_modules|bower_components/},
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.styl$/, loader: 'style!css?sourceMap!stylus?sourceMap'},
      {test: /\.scss$/, loader: ExtractTextPlugin.extract('style',
        'css?sourceMap!sass?sourceMap&sourceComments&includePaths[]=' +
        bowerPath)},
      {test: /\.jade$/, loader: 'jade'},
      {test: /\.(png|jpg|jpeg)$/,
          loader: 'url?limit=20000&name=assets/[name].[ext]!img?' +
            'minimize&progressive=true'},
      {test: /\.(woff|woff2)$/, loader: 'url?limit=100000&name=[name].[ext]'}
    ]
  },
  plugins: [
    new Webpack.DefinePlugin({
      __DEVELOPMENT__: DEV,
      __PRODUCTION__: DEPLOY
    }),
    new Webpack.ProvidePlugin({
      //angular: 'exports?window.angular!angular/angular.min'
    }),
    new Webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.bundle.js',
      minSize: 20
    }),
    new ExtractTextPlugin('[name].bundle.css')
  ],
  resolve: {
    root: srcPath,
    alias: [],
    extensions: [
      '',
      '.js',
      '.jade',
      '.scss'
    ],
    modulesDirectories: [
      'node_modules'
    ]
  }
};

if (DEV) {
  config.plugins.unshift(new Webpack.HotModuleReplacementPlugin());
}

if (DEPLOY) {

  config.entry.app = './app';
  config.debug = false;
  config.devtool = 'source-map';
  config.output.pathinfo = false;

  //config.plugins.push(new Webpack.optimize.UglifyJsPlugin());
  config.plugins.push(new Webpack.optimize
    .MinChunkSizePlugin({minChunkSize: 40}));

}

config.addVendor('angular-material',
  bowerPath + '/angular-material/angular-material.min');

config.addVendor('ui.router',
  bowerPath + '/angular-ui-router/release/angular-ui-router.min');

config.addVendor('angular-dynamic-locale',
  bowerPath + '/angular-dynamic-locale/tmhDynamicLocale.min');

config.addVendor('angular-translate',
  bowerPath + '/angular-translate/angular-translate.min');

config.addVendor('ngFx',
  bowerPath + '/ngFx/dist/ngFx.min.js');

config.addVendor('messageformat',
  bowerPath + '/messageformat/messageformat');

config.addVendor('messageformat-locale-en',
  bowerPath + '/messageformat/locale/en');

config.addVendor('messageformat-locale-es',
  bowerPath + '/messageformat/locale/es');

config.addVendor('angular-translate-loader-partial',
  bowerPath + '/angular-translate-loader-partial/' +
  'angular-translate-loader-partial.min');

config.addVendor('angular-translate-loader-static-files',
  bowerPath + '/angular-translate-loader-static-files/' +
  'angular-translate-loader-static-files.min');

config.addVendor('angular-translate-interpolation-messageformat',
  bowerPath + '/angular-translate-interpolation-messageformat/' +
  'angular-translate-interpolation-messageformat.min');

config.addVendor('oclazyload',
  bowerPath + '/oclazyload/dist/ocLazyLoad.min');

config.addVendor('verge',
  bowerPath + '/verge');

config.addVendor('picturefill',
  bowerPath + '/picturefill');

config.addVendor('angular-file-upload',
  bowerPath + '/angular-file-upload/dist/angular-file-upload.min');

module.exports = config;
